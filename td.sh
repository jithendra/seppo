#!/bin/sh
cd "$(dirname "$0")"

make && rsync -avPz _build/seppo-Linux-x86_64-0.2.cgi c2:~/Downloads/
# rsync -avPz --exclude _build/ . c2:~/Documents/seppo/
# ssh c2 make -C Documents/seppo/

ssh c2 ls -l \
  ~/Downloads/seppo-Linux-x86_64-0.2.cgi \
  /var/www/vhosts/dev.seppo.social/pages/2023-05-17/seppo.cgi
ssh c2 /var/www/vhosts/dev.seppo.social/pages/2023-05-17/seppo.cgi -V
curl --head https://dev.seppo.social/2023-05-17/seppo.cgi

espeak done

# rsync -avPz --exclude _build --exclude tmp --delete . c2:~/Documents/seppo/
# ssh c2 make -C Documents/seppo
