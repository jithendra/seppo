(* https://www.w3.org/TR/activitypub/ *)
module ContentType = struct
  let app_act_json = "application/activity+json"
  let app_atom_xml = "application/atom+xml"
  let app_json     = "application/json"
  let app_ld_json  = "application/ld+json"
  let ld_json_act_str = {|application/ld+json; profile="https://www.w3.org/ns/activitystreams"|}
  let text_html    = "text/html"
  let text_xml     = "text/xml"
  let any = "*/*"

  let content_types = [
    app_act_json, `JSON;
    app_atom_xml, `ATOM;
    app_json, `JSON;
    app_ld_json, `JSON;
    ld_json_act_str, `JSON;
    text_html, `HTML;
    text_xml, `XML;
    any, `HTML;
  ]

(*
  let of_string content_type =
    List.find_opt
      (fun (str, _) ->
         String.prefix ~pre:str content_type) content_types
    |> Option.map snd
*)

end

(* RFC7033 *)
module Webfinger = struct
  let json_rd      = "application/jrd+json"

  let self_rel     = "self"
  let ostatus_rel  = "http://ostatus.org/schema/1.0/subscribe"
  let profile_page = "http://webfinger.net/rel/profile-page"
  let alternate    = "alternate"
end

(* https://www.w3.org/TR/activitystreams-core/ *)
module ActivityStreams = struct
  let ns_as  = "https://www.w3.org/ns/activitystreams"
  let ns_sec = "https://w3id.org/security/v1"
  let public = Uri.of_string "https://www.w3.org/ns/activitystreams#Public"

  let context =
    "@context", `A [
      `String ns_as;
      `String ns_sec;
      `O [
        (* https://docs.joinmastodon.org/spec/activitypub/#schema *)
        ("schema", `String "http://schema.org#");
        ("PropertyValue", `String "schema:PropertyValue");
        ("value", `String "schema:value");
        ("@language", `String "und"); (* undefined. BCP47 https://www.w3.org/TR/activitystreams-core/#naturalLanguageValues *)
      ]
    ]
end
