
(* inspired by https://github.com/Gopiandcode/ocamlot/src/master/lib/activitypub/decode.ml *)

(* keep this module agnostic of the json library.
 * So we discard the 'raw' json for now and keep only the data
 * we expect.
 *
 * Jsonm.lexeme has no equal, so raw could not be equaled.
*)
type jsonm = Jsonm.lexeme
let pp_jsonm = Jsonm.pp_lexeme
(* let equal_jsonm l r = Jsonm.equal l r *)

type uri = Uri.t
let pp_uri = Uri.pp
let equal_uri = Uri.equal

(** https://www.w3.org/TR/activitystreams-core/#collections *)
type 'a collection_page = {
  id         : uri;
  current    : uri option;
  first      : uri option;
  is_ordered : bool;
  items      : 'a list;
  last       : uri option;
  next       : uri option;
  part_of    : uri option;
  prev       : uri option;
  total_items: int option;
} [@@deriving show, eq]

(** https://www.w3.org/TR/activitystreams-core/#collections *)
type 'a collection = {
  id         : uri;
  current    : uri option;
  first      : uri option;
  is_ordered : bool;
  items      : 'a list;
  last       : uri option;
  total_items: int option;
} [@@deriving show, eq]

(** https://www.w3.org/TR/activitystreams-vocabulary/#activity-types *)
(** https://www.w3.org/TR/activitystreams-vocabulary/#dfn-create *)
type 'a create = {
  id            : uri;
  actor         : uri;
  cc            : uri list;
  direct_message: bool;
  obj           : 'a;
  published     : Ptime.t option;
  to_           : uri list;
  (* raw: jsonm; *)
} [@@deriving show, eq]

(** https://www.w3.org/TR/activitystreams-vocabulary/#dfn-announce *)
type 'a announce = {
  id       : uri;
  actor    : uri;
  cc       : uri list;
  obj      : 'a;
  published: Ptime.t option;
  to_      : uri list;
  (*  raw: jsonm; *)
} [@@deriving show, eq]


type 'a accept = {
  id       : uri;
  actor    : uri;
  obj      : 'a;
  published: Ptime.t option;
  (*  raw: jsonm; *)
} [@@deriving show, eq]

type 'a undo = {
  id       : uri;
  actor    : uri;
  obj      : 'a;
  published: Ptime.t option;
  (*  raw: jsonm; *)
} [@@deriving show, eq]

type 'a delete = {
  id       : uri;
  actor    : uri;
  obj      : 'a;
  published: Ptime.t option;
  (*  raw: jsonm; *)
}
[@@deriving show, eq]

type 'a event = [
    `Create of 'a create
  | `Announce of 'a announce
  | `Accept of 'a accept
  | `Undo of 'a undo
  | `Delete of 'a delete
] [@@deriving show, eq]


type public_key = {
  id   : uri;
  owner: uri;
  pem  : string;
  signatureAlgorithm: string option;
} [@@deriving show, eq]

(* Attachment as seen on typical actor/person profiles, e.g.
 * $ curl -L -H 'Accept: application/activity+json' 'https://digitalcourage.social/users/mro'
 *
 * https://www.w3.org/TR/activitystreams-vocabulary/#dfn-attachment
 * https://docs.joinmastodon.org/spec/activitypub/#schema
 *
    {
      "name": "Support",
      "value": "<a href=\"https://seppo.social/support\">Seppo.Social/support</a>",
      "type": "PropertyValue"
    },
*)
type property_value = {
  name : string;
  value: string;
} [@@deriving show, eq]

(* https://www.w3.org/TR/activitystreams-vocabulary/#dfn-person
 * https://www.w3.org/TR/activitystreams-core/#actors
*)
type person = {
  id                         : uri;
  inbox                      : uri;
  outbox                     : uri;
  followers                  : uri option;
  following                  : uri option;

  attachment                 : property_value list;
  discoverable               : bool;
  icon                       : uri option;
  image                      : uri option;
  manually_approves_followers: bool;
  name                       : string option;
  preferred_username         : string option;
  public_key                 : public_key;
  summary                    : string option;
  url                        : uri option;
  (*  raw: jsonm; *)
}  [@@deriving show, eq]

(* https://www.w3.org/TR/activitystreams-vocabulary/#dfn-follow *)
type follow = {
  id     : uri;
  actor  : uri;
  cc     : uri list;
  object_: uri;
  state  : [`Pending | `Cancelled ] option;
  to_    : uri list;
  (*  raw: jsonm; *)
} [@@deriving show, eq]

(* https://www.w3.org/TR/activitystreams-vocabulary/#dfn-tag *)
type tag = {
  ty  : [`Mention | `Hashtag ];
  href: uri;
  name: string;
} [@@deriving show, eq]

(* https://www.w3.org/TR/activitystreams-vocabulary/#dfn-attachment *)
type attachment = {
  type_     : string option;
  (* https://www.w3.org/TR/activitystreams-vocabulary/#dfn-mediatype *)
  media_type: string option;
  name      : string option;
  url       : uri;
} [@@deriving show, eq]

(* https://www.w3.org/TR/activitystreams-vocabulary/#dfn-note*)
type note = {
  id         : uri;
  actor      : uri;
  attachment : attachment list;
  cc         : uri list;
  content    : string;
  in_reply_to: uri option;
  media_type : string option; (* https://www.w3.org/TR/activitystreams-vocabulary/#dfn-mediatype *)
  published  : Ptime.t option;
  sensitive  : bool;
  source     : uri option;
  summary    : string option;
  tags       : tag list;
  to_        : uri list;
  (*raw: jsonm;*)
} [@@deriving show, eq]

type block = {
  id       : uri;
  actor    : uri;
  obj      : uri;
  published: Ptime.t option;
  (*raw: jsonm;*)
} [@@deriving show, eq]

type like = {
  id       : uri;
  actor    : uri;
  obj      : uri;
  published: Ptime.t option;
  (*raw: jsonm;*)
}
[@@deriving show, eq]


type core_obj = [
  | `Block  of block
  | `Follow of follow
  | `Like   of like
  | `Link   of string
  | `Note   of note
  | `Person of person
] [@@deriving show, eq]

type core_event = core_obj event
[@@deriving show, eq]

type obj = [ core_obj | core_event ]
[@@deriving show, eq]

module Webfinger = struct

  type ty = [
    | `ActivityJson
    | `ActivityJsonLd
    | `Atom
    | `Html
    | `Json
  ]
  [@@deriving show, eq]

  type link =
    | Self             of ty * uri
    | ProfilePage      of ty * uri
    | Alternate        of ty * uri
    | OStatusSubscribe of string (* should contain unescaped {} *)
  [@@deriving show, eq]

  type query_result = {
    subject: string;
    aliases: string list;
    links  : link list;
  }
  [@@deriving show, eq]

  let self_link links =
    links
    |> List.find_map (function
        | Self ((`ActivityJson | `ActivityJsonLd | `Json), url) -> Some url
        | _ -> None)

  let profile_page links =
    links
    |> List.find_map (function
        | ProfilePage ((`Html | `Atom), url) -> Some url
        | _ -> None)
end
