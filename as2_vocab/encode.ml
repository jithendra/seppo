module E = Decoders_ezjsonm.Encode

let (<:) = function
  | (_, None) -> fun _ -> []
  | (field, Some vl) -> fun ty -> [field, ty vl]
let (@)  field vl = (field, Some vl)
let (@?) field vl = (field, vl)
let ptime time = time |> Ptime.to_rfc3339 ~tz_offset_s:0 |> E.string
let uri ~base u = u |> Uri.resolve "https" base |> Uri.to_string |> E.string
let obj ls = E.obj @@ List.flatten ls
let ap_obj ?(context = false) ty ls =
  let l = ("type", E.string ty) :: List.flatten ls in
  let l = if context
    then Constants.ActivityStreams.context :: l
    else l in
  E.obj l

let or_raw conv = function
  | `Raw v -> v
  | `Value v -> conv v

(** https://www.w3.org/TR/activitystreams-core/#collections *)
let collection_page ~base enc
    ({ id; 
       current;
       first;
       is_ordered;
       items;
       last;
       next;
       part_of;
       prev;
       total_items 
     }: _ Types.collection_page) =
  ap_obj ~context:true "OrderedCollectionPage" [
    "id"         @ id <: uri ~base;
    "current"    @? current <: uri ~base;
    "first"      @? first <: uri ~base;
    "last"       @? last <: uri ~base;
    "next"       @? next <: uri ~base;
    "partOf"     @? part_of <: uri ~base;
    "prev"       @? prev <: uri ~base;
    "totalItems" @? total_items <: E.int;
    (match is_ordered with
     | true -> "orderedItems"
     | false -> "items")  @ items <: E.list enc
  ]

let collection ~base enc
    ({ id; 
       current;
       first;
       is_ordered;
       items;
       last;
       total_items;
     }: _ Types.collection) =
  ap_obj ~context:true "OrderedCollection" [
    "id"         @ id <: uri ~base;
    "current"    @? current <: uri ~base;
    "first"      @? first <: uri ~base;
    "last"       @? last <: uri ~base;
    "totalItems" @? total_items <: E.int;
    match is_ordered with
    | true  -> "orderedItems" @ items <: E.list enc
    | false -> "items" @ items <: E.list enc
  ]

let create ?(context = false) ~base enc ({ id; actor; published; to_; cc; direct_message; obj(*(*; raw=_*)*) }:
                                           _ Types.create) =
  ap_obj ~context "Create" [
    "id"            @ id <: uri ~base;
    "actor"         @ actor <: uri ~base;
    "published"     @? published <: ptime;
    "to"            @ to_ <: E.(list (uri ~base));
    "cc"            @ cc <: E.(list (uri ~base));
    "directMessage" @ direct_message <: E.bool;
    "object"        @ obj <: enc;
  ]

let announce ~base enc ({ id; actor; published; to_; cc; obj(*(*; raw=_*)*) } : _ Types.announce) =
  ap_obj ~context:true "Announce" [
    "id"        @ id <: uri ~base;
    "actor"     @ actor <: uri ~base;
    "published" @? published <: ptime;
    "to"        @ to_ <: E.(list (uri ~base));
    "cc"        @ cc <: E.(list (uri ~base));
    "object"    @ obj <: enc;
  ]

let accept ~base enc ({ id; actor; published; obj(*; raw=_*) } : _ Types.accept) =
  ap_obj ~context:true "Accept" [
    "id"        @ id <: uri ~base;
    "actor"     @ actor <: uri ~base;
    "published" @? published <: ptime;
    "object"    @ obj <: enc;
  ]

let undo ~base enc ({ id; actor; published; obj(*; raw=_*) } : _ Types.undo) =
  ap_obj ~context:true "Undo" [
    "id"        @ id <: uri ~base;
    "actor"     @ actor <: uri ~base;
    "published" @? published <: ptime;
    "object"    @ obj <: enc;
  ]

let delete ~base enc ({ id; actor; published; obj(*; raw=_*) } : _ Types.delete) =
  ap_obj ~context:true "Delete" [
    "id"        @ id <: uri ~base;
    "actor"     @ actor <: uri ~base;
    "published" @? published <: ptime;
    "object"    @ obj <: enc;
  ]

(** * Objects *)

let public_key ~base (key: Types.public_key) =
  obj [
    "id"           @ key.id <: uri ~base;
    "owner"        @ key.owner <: uri ~base;
    "publicKeyPem" @ key.pem <: E.string;
    "signatureAlgorithm" @? key.signatureAlgorithm <: E.string;
  ]

let property_value (v : Types.property_value) =
  obj [
    "type"  @ "PropertyValue" <: E.string;
    "name"  @ v.name <: E.string;
    "value" @ v.value <: E.string;
  ]

let image ~base url =
  obj [
    "type" @ "Image" <: E.string;
    (* mediatype? *)
    "url"  @ url <: uri ~base;
  ]

let person ~base ({ id; name; url; inbox; outbox;
                    preferred_username; summary;
                    manually_approves_followers;
                    discoverable; followers; following;
                    public_key=key; attachment; icon=ic; image=im(*; raw=_*) }: Types.person) =
  ap_obj ~context:true "Person" [
    "id"                        @ id <: uri ~base;
    "inbox"                     @ inbox <: uri ~base;
    "outbox"                    @ outbox <: uri ~base;
    "followers"                 @? followers <: uri ~base;
    "following"                 @? following <: uri ~base;

    "name"                      @? name <: E.string;
    "url"                       @? url <: uri ~base;

    "preferredUsername"         @? preferred_username <: E.string;

    "summary"                   @? summary <: E.string;
    "publicKey"                 @ key <: public_key ~base;
    "manuallyApprovesFollowers" @ manually_approves_followers <: E.bool;
    "discoverable"              @ discoverable <: E.bool;
    "attachment"                @ attachment <: E.list property_value;
    "icon"                      @? ic <: image ~base;
    "image"                     @? im <: image ~base;
  ]

let state = function
    `Pending -> E.string "pending"
  | `Cancelled -> E.string "cancelled"


let follow ~base ({ id; actor; cc; object_; to_; state=st(*; raw=_*) }: Types.follow) =
  ap_obj ~context:true "Follow" [
    "id"     @ id <: uri ~base;
    "actor"  @ actor <: uri ~base;
    "to"     @ to_ <: E.list (uri ~base);
    "cc"     @ cc <: E.list (uri ~base);
    "object" @ object_ <: uri ~base;
    "state"  @? st <: state;
  ]

(* https://www.w3.org/TR/activitystreams-vocabulary/#microsyntaxes *)
let tag ~base ({ ty; href; name }: Types.tag) =
  let ty,pre = match ty with
    | `Mention -> "Mention","@"
    | `Hashtag -> "Hashtag","#" in
  ap_obj ty [
    "href" @ href <: uri ~base;
    "name" @ (pre ^ name) <: E.string;
  ]

let attachment ~base ({media_type; name; url; type_}: Types.attachment) =
  obj [
    "type"      @? type_ <: E.string;
    "mediaType" @? media_type <: E.string;
    "name"      @? name <: E.string;
    "url"       @ url <: uri ~base;
  ]

let note ?(context = false)
    ~base
    ({ id; actor; to_; in_reply_to; cc; media_type; content; sensitive; source; summary;
       attachment=att;
       published; tags(*; raw=_*) }: Types.note) =
  let att = match att with [] -> None | _ -> Some att in
  ap_obj ~context "Note" [
    "id"         @ id <: uri ~base;
    "actor"      @ actor <: uri ~base;
    "attachment" @? att <: E.list (attachment ~base);
    "to"         @ to_ <: E.list (uri ~base);
    "inReplyTo"  @? in_reply_to <: uri ~base;
    "cc"         @ cc <: E.list (uri ~base);
    "mediaType"  @? media_type <: E.string;
    "content"    @ content <: E.string;
    "sensitive"  @ sensitive <: E.bool;
    "source"     @? source <: uri ~base;
    "summary"    @? summary <: E.string;
    "published"  @? published <: ptime;
    "tags"       @ tags <: (E.list (tag ~base));
  ]

let block ~base ({ id; obj; published; actor(*; raw=_*) }: Types.block) =
  ap_obj ~context:true "Block" [
    "id"        @ id <: uri ~base;
    "object"    @ obj <: uri ~base;
    "actor"     @ actor <: uri ~base;
    "published" @? published <: ptime;
  ]

let like ~base ({ id; actor; published; obj(*; raw=_*) }: Types.like) =
  ap_obj ~context:true "Like" [
    "id"        @ id <: uri ~base;
    "actor"     @ actor <: uri ~base;
    "object"    @ obj <: uri ~base;
    "published" @? published <: ptime;
  ]


let core_obj ~base : Types.core_obj E.encoder = function
  | `Block b -> block ~base b
  | `Follow f -> follow ~base f
  | `Like l -> like ~base l
  | `Link r -> E.string r
  | `Note n -> note ~base n
  | `Person p -> person ~base p

let event ~base enc : _ Types.event E.encoder = function
  | `Accept a -> accept ~base enc a
  | `Announce a -> announce ~base enc a
  | `Create c -> create ~base enc c
  | `Delete d -> delete ~base enc d
  | `Undo u -> undo ~base enc u

let object_ ~base : Types.obj E.encoder = function
  | #Types.core_obj as c -> core_obj ~base c
  | #Types.core_event as e -> event ~base (core_obj ~base) e

module Webfinger = struct

  let ty = function
    | `ActivityJson -> E.string Constants.ContentType.app_act_json
    | `ActivityJsonLd -> E.string Constants.ContentType.ld_json_act_str
    | `Atom -> E.string Constants.ContentType.app_atom_xml
    | `Html -> E.string Constants.ContentType.text_html
    | `Json -> E.string Constants.ContentType.app_json
    | `Xml  -> E.string Constants.ContentType.text_xml

  let link ~base = function
    | Types.Webfinger.Self (t, href) -> obj [
        "href" @ href <: uri ~base;
        "rel"  @ Constants.Webfinger.self_rel <: E.string;
        "type" @ t <: ty;
      ]
    | ProfilePage (t, href) ->
      obj [
        "href" @ href <: uri ~base;
        "rel"  @ Constants.Webfinger.profile_page <: E.string;
        "type" @ t <: ty;
      ]
    | Alternate (t, href) ->
      obj [
        "href" @ href <: uri ~base;
        "rel"  @ Constants.Webfinger.alternate <: E.string;
        "type" @ t <: ty;
      ]
    | OStatusSubscribe template -> obj [
        "rel"      @ Constants.Webfinger.ostatus_rel <: E.string;
        "template" @ template <: E.string;
      ]

  let query_result ~base ({subject;aliases;links}: Types.Webfinger.query_result) =
    obj [
      "subject" @ subject <: E.string;
      "aliases" @ aliases <: E.(list string);
      "links"   @ links <: E.list (link ~base);
    ]

end
