
# v$ver, $date

Download: https://Seppo.Social/Linux-x86_64-$ver
Source:   https://Seppo.Social/v/$git_sha
Install:  https://seppo.social/en/support/#installation

Changes

- being subscribed to
- decline subscriptions
- subscribe others
- block


Corresponds to https://blog.mro.name/2022/12/nlnet-seppo/#7-activitypub-activities-un-follow-block
