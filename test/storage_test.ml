(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Web.
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

open Seppo_lib
open Storage

let t1 (a,_) = a

let test_dir_of_ix () =
  Logr.info (fun m -> m "%s.%s" "storage_test" "test_dir_of_ix");
  "app/var/lib/o/p/23.ix" |> dir_of_ix
  |> t1
  |> Assert2.equals_string "storage_test.url_of_ix 10" "o/p-23/";
  "app/var/lib/o/t/foo/23.ix" |> dir_of_ix
  |> t1
  |> Assert2.equals_string "storage_test.url_of_ix 20" "o/t/foo-23/";
  "app/var/lib/activitypub/tag/foo/23.ix" |> dir_of_ix
  |> t1
  |> Assert2.equals_string "storage_test.url_of_ix 50" "/dev/null"

let test_unnumbered () =
  Logr.info (fun m -> m "%s.%s" "storage_test" "test_unnumbered");
  "o/p-23/" |> unnumbered |> t1 |> Assert2.equals_string "unnum 1" "o/p/";
  "o/p-23/index.xml" |> unnumbered |> t1 |> Assert2.equals_string "unnum 10" "o/p/";
  "o/t/foo-12/index.xml" |> unnumbered |> t1 |> Assert2.equals_string "unnum 20" "o/t/foo/";
  "o/d/2023-03-05-0/index.xml" |> unnumbered |> t1 |> Assert2.equals_string "unnum 30" "o/d/2023-03-05/";
  assert true

let test_tuple () =
  Logr.info (fun m -> m "storage_test.test_tuple");
  Logr.info (fun m -> m "%s.%s" "storage_test" "test_tuple");
  (23,42) |> TwoPad10.encode |> Assert2.equals_string "storage_test.test_tuple 10" "0x00000017-0x0000002a";
  (0x3fff_ffff,42) |> TwoPad10.encode |> Assert2.equals_string "storage_test.test_tuple 20" "0x3fffffff-0x0000002a";
  let (a,b) = "0000000023-0000000042" |> TwoPad10.decode in
  a |> Assert2.equals_int "storage_test.test_tuple 30" 23;
  b |> Assert2.equals_int "storage_test.test_tuple 40" 42;
  assert true

(*
let test_json () =
  let minify = false in
  let base = Uri.of_string "https://example.com/su/" in
  let item = Rfc4287_test.mk_sample () in
  item |> As2.Note.mk_note_json ~base
  |> As2.Note.mk_create_json ~base item
  |> Ezjsonm.to_string ~minify
  |> Assert2.equals_string "storage_test.test_json 10" {|{
  "type": "Create",
  "id": "https://example.com/su/o/p-12/#23/Create",
  "actor": "https://example.com/su/activitypub/",
  "published": "2023-02-11T11:07:23+01:00",
  "to": [
    "https://www.w3.org/ns/activitystreams#Public"
  ],
  "cc": [
    "https://example.com/su/activitypub/followers/"
  ],
  "object": {
    "type": "Note",
    "id": "o/p-12/#23",
    "actor": "activitypub/",
    "to": [
      "https://www.w3.org/ns/activitystreams#Public"
    ],
    "cc": [
      "activitypub/followers/"
    ],
    "mediaType": "text/plain; charset=utf8",
    "content": "I am happy to announce the premiere release of #Seppo!, Personal #Social #Media under funding of NLnet.nl.\n\nFind it at https://Seppo.Social/downloads/\n\nIt has no notable user facing #ActivityPub features so far, but\n\n- easy setup of instance & account,\n- #webfinger discoverability (from e.g. mastodon search),\n- a welcoming, long-term reliable website.\n\nI made this embarrassingly limited release to build awareness for low-barrier-entry internet services in general and especially in the field of personal communication as well as letting the #fediverse and #permacomputing communities know.\n\nYour comments are very much appreciated.",
    "sensitive": false,
    "summary": "#Announce Seppo.Social v0.1 and Request for Comments.",
    "published": "2023-02-11T10:07:23Z",
    "tags": [
      {
        "type": "Hashtag",
        "href": "o/t/webfinger/",
        "name": "#webfinger"
      },
      {
        "type": "Hashtag",
        "href": "o/t/Social/",
        "name": "#Social"
      },
      {
        "type": "Hashtag",
        "href": "o/t/Seppo/",
        "name": "#Seppo"
      },
      {
        "type": "Hashtag",
        "href": "o/t/permacomputing/",
        "name": "#permacomputing"
      },
      {
        "type": "Hashtag",
        "href": "o/t/Media/",
        "name": "#Media"
      },
      {
        "type": "Hashtag",
        "href": "o/t/Fediverse/",
        "name": "#Fediverse"
      },
      {
        "type": "Hashtag",
        "href": "o/t/Announce/",
        "name": "#Announce"
      },
      {
        "type": "Hashtag",
        "href": "o/t/ActivityPub/",
        "name": "#ActivityPub"
      }
    ]
  }
}|}
*)

let () =
  (* Unix.chdir "../../../test/"; *)
  test_dir_of_ix ();
  test_unnumbered ();
  test_tuple ();
  (*  test_json (); *)
  assert true
