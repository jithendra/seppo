open Seppo_lib

let test_regexp () =
  let rx = Str.regexp {|^[^\n\t ]\([^\n\t]+[^\n\t ]\)?$|} in
  assert (Str.string_match rx "a" 0);
  assert true

let test_markup_xml () =
  [
    `Start_element (("", "foo"), []);
    `Start_element (("", "bar"), []);
    `Start_element (("", "baz"), []);
    `End_element;
    `End_element;
    `End_element;
  ]
  |> Markup.of_list |> Markup.pretty_print |> Markup.write_xml
  |> Markup.to_string
  |> Assert2.equals_string "foo" "<foo>\n <bar>\n  <baz/>\n </bar>\n</foo>\n"

let test_login () =
  let tit = "> U \" h & ' u <"
  and tok = "ff13e7eaf9541ca2ba30fd44e864c3ff014d2bc9"
  and ret = "retu"
  and att n v = (("", n), v)
  and elm name atts = `Start_element (("", name), atts) in
  [
    `Xml
      {
        Markup.version = "1.0";
        encoding = Some "utf-8";
        standalone = Some false;
      };
    `PI
      ("xml-stylesheet", "type='text/xsl' href='./themes/current/do=login.xslt'");
    `Comment
      "\n\
      \  must be compatible with \
       https://code.mro.name/mro/Shaarli-API-test/src/master/tests/test-post.sh\n\
      \  \
       https://code.mro.name/mro/ShaarliOS/src/1d124e012933d1209d64071a90237dc5ec6372fc/ios/ShaarliOS/API/ShaarliCmd.m#L386\n";
    elm "html" [ att "xmlns" "http://www.w3.org/1999/xhtml" ];
    elm "head" [];
    elm "title" [];
    `Text [ tit ];
    `End_element;
    `End_element;
    elm "body" [];
    elm "form" [ att "method" "post" ];
    elm "input" [ att "name" "login"; att "type" "text" ];
    `End_element;
    elm "input" [ att "name" "password"; att "type" "password" ];
    `End_element;
    elm "input" [ att "name" "longlastingsession"; att "type" "checkbox" ];
    `End_element;
    elm "input" [ att "name" "token"; att "type" "hidden"; att "value" tok ];
    `End_element;
    elm "input" [ att "name" "returnurl"; att "type" "hidden"; att "value" ret ];
    `End_element;
    elm "input" [ att "value" "Login"; att "type" "submit" ];
    `End_element;
    `End_element;
    `End_element;
  ]
  |> Markup.of_list |> Markup.pretty_print |> Markup.write_xml
  |> Markup.to_string |> String.length
  |> Assert2.equals_int "foo" 842

let test_cookie () =
  Web.cookie_name |> Assert2.equals_string "c0" "#Seppo!";
  (match "(5:seppi25:2022-10-03T01:02:03+01:00)"
         |> Web.cookie_decode with
  | Ok (Auth.Uid uid, _t) ->
    uid |> Assert2.equals_string "c1" "seppi"
  | Error e -> e |> Assert2.equals_string "c2" "");
  (Auth.Uid "seppa", Ptime.epoch)
  |> Web.cookie_encode
  |> Assert2.equals_string "c3" "(5:seppa25:1970-01-01T00:00:00-00:00)";
  (match (Auth.Uid "seppu", Ptime.epoch)
         |> Web.cookie_encode
         |> Web.cookie_decode with
  | Ok (Auth.Uid uid, _t) ->
    uid |> Assert2.equals_string "c4" "seppu"
  | Error e -> e |> Assert2.equals_string "c2" "");
  assert true

let test_csrf_token () =
  let v = "f19a65cecdfa2971afb827bc9413eb7244e469a8" in
  let f = ([("token", [ v ])], ()) in
  match Web.check_token v f with
  | Ok (t, _) -> Assert2.equals_string "shaarli_test:test_csrf_token 10" v t
  | Error (s,r,_) -> 
    Assert2.equals_int "shaarli_test:test_csrf_token 20" 200 s;
    Assert2.equals_string "shaarli_test:test_csrf_token 30" "" r
(*   *)

let () =
  Unix.chdir "../../../test/";
  test_regexp ();
  test_markup_xml ();
  test_login ();
  test_cookie ();
  test_csrf_token ();
  assert true
