(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Web.
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

open Seppo_lib

let o i = i |> Optint.of_int

let oo i = i |> Optint.of_int64

let test_hash () =
  Logr.info (fun m -> m "mapcdb_test.test_hash");
  let open Mapcdb in
  "s" |> Bytes.of_string |> Ds_cdb.hash
  |> Assert2.equals_int32 "u" 177622l;

  "s" |> Bytes.of_string |> hash_by |> Assert2.equals_optint "u" (o 177622);

  Assert2.equals_optint "u" (o 0x2b5c4) ("a" |> Bytes.of_string |> hash_by);
  let k0 =
    "http://www.traunsteiner-tagblatt.de/region+lokal/landkreis-traunstein/traunstein/pressemitteilungen-der-stadt-traunstein_artikel,-Traunstein-20-%E2%80%93-Neue-Medien-im-Mittelpunkt-_arid,198374.html"
  in
  Assert2.equals_optint "u 43" (oo 0xc7410a37L) (k0 |> Bytes.of_string |> hash_by);

  assert true

let test_find_string_opt () =
  Logr.info (fun m -> m "mapcdb_test.test_find_string_opt");
  let open Mapcdb in
  find_string_opt "s" (Cdb "mini.cdb")
  |> Option.get
  |> Assert2.equals_string "b" "ß";
  assert (None = find_string_opt "zzz" (Cdb "mini.cdb"));

  find_string_opt
    "http://www.traunsteiner-tagblatt.de/region+lokal/landkreis-traunstein/traunstein/pressemitteilungen-der-stadt-traunstein_artikel,-Traunstein-20-%E2%80%93-Neue-Medien-im-Mittelpunkt-_arid,198374.html"
    (Cdb "big.cdb")
  |> Option.get |> String.length
  |> Assert2.equals_int "b" 1000;

  assert true

let mk_db fn =
  (try
     Unix.mkdir "tmp" File.pDir;
   with Unix.Unix_error (Unix.EEXIST, _, _) -> ());
  (try
     Unix.unlink fn;
     Unix.unlink (fn ^ "~")
   with Unix.Unix_error (Unix.ENOENT, _, _) -> ());
  File.touch fn;
  Mapcdb.Cdb fn

let test_update () =
  Logr.info (fun m -> m "mapcdb_test.test_update");
  let fn = mk_db "tmp/add.cdb" in
  let _ = Mapcdb.update_string "a" "1" fn in
  Mapcdb.find_string_opt "a" fn |> Option.get |> Assert2.equals_string "add 1" "1";
  let _ = Mapcdb.update_string "a" "2" fn in
  Mapcdb.find_string_opt "a" fn |> Option.get |> Assert2.equals_string "add 2" "2";
  let _ = Mapcdb.remove_string "a" fn in
  assert (Mapcdb.find_string_opt "a" fn |> Option.is_none);
  assert true

let test_fold_left () =
  Logr.info (fun m -> m "%s.%s" "mapcdb_test" "fold_left");
  let cdb = Mapcdb.Cdb "data/mini.cdb" in
  Mapcdb.fold_left (fun init (k,v) -> 
      let k = k |> Bytes.to_string
      and v = v |> Bytes.to_string in
      (* Logr.debug (fun m -> m "  %s->%s" k v); *)
      (k,v) :: init) [] cdb
  |> List.length |> Assert2.equals_int "test_fold_left 11" 3

let bench_update_1 n =
  Logr.info (fun m -> m "mapcdb_test.bench_update_1");
  let fn = mk_db "tmp/add_1.cdb" in
  let i = ref 1 in
  while !i < n do
    Printf.printf ".";
    let k = Printf.sprintf "%d %f" !i (Sys.time()) in
    let _ = Mapcdb.update_string k k fn in
    incr i
  done;
  assert true

let bench_update_2 n =
  Logr.info (fun m -> m "mapcdb_test.bench_update_2");
  let fn = mk_db "tmp/add_2.cdb" in
  let l = List.init n
      (fun i ->
         let k :bytes = Printf.sprintf "%d" i |> Bytes.of_string in
         (k,k)) in
  let s = List.to_seq l in
  let all _ = true in
  let _ = Mapcdb.add_seq all s fn in
  assert true

let bench_update_3 n =
  Logr.info (fun m -> m "mapcdb_test.bench_update_3");
  let fn = mk_db "tmp/add_3.cdb"
  and all _ = true
  and fkt add =
    let i = ref 0 in
    while !i < n do
      let k = Printf.sprintf "%d" !i |> Bytes.of_string in
      let v = Printf.sprintf "%f" (Sys.time()) |> Bytes.of_string in
      let _ = add (k,v) in
      incr i
    done;
  in
  let _ = Mapcdb.add_many all fkt fn in
  assert true

let () =
  Unix.chdir "../../../test/";
  test_hash ();
  test_find_string_opt ();
  test_update ();
  test_fold_left ();
  bench_update_1 1000;
  bench_update_2 100_000;
  bench_update_3 100_000;
  assert true
