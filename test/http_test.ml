(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Web.
 *
 * http_test.ml
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

open Seppo_lib

module Request = struct
  let test_uri () =
    let abs' (r : Http.Request.t) : Uri.t =
      Uri.make
        ~scheme:r.scheme
        ~host:r.host
        ~port:(int_of_string r.server_port)
        ~path:(r.script_name ^ r.path_info)
        ()
    and r : Http.Request.t = {
      content_type   = "text/plain";
      content_length = None;
      host           = "example.com";
      http_cookie    = "";
      path_info      = "/shaarli";
      query_string   = "post=uhu";
      request_method = "GET";
      remote_addr    = "127.0.0.1";
      scheme         = "https";
      script_name    = "/sub/seppo.cgi";
      server_port    = "443";
    } in
    r |> abs' |> Uri.to_string |> Assert2.equals_string "test_uri 0" "https://example.com:443/sub/seppo.cgi/shaarli";
    r |> Http.Request.abs |> Uri.of_string |> Uri.to_string |> Assert2.equals_string "test_uri 10" "https://example.com/sub/seppo.cgi/shaarli?post=uhu";
    r |> Http.Request.request_uri |> Assert2.equals_string "test_uri 20" "/sub/seppo.cgi/shaarli?post=uhu";
    r |> Http.Request.request_uri |> Uri.of_string |> Uri.to_string |> Assert2.equals_string "test_uri 21" "/sub/seppo.cgi/shaarli?post=uhu";
    "a" |> Assert2.equals_string "test_uri 30" "a";
    assert true
end

module Cookie = struct
  let test_rfc1123 () =
    let s = "Thu, 01 Jan 1970 00:00:00 GMT" in
    Ptime.epoch |> Http.to_rfc1123 |> Assert2.equals_string "rfc1123" s;
    assert true

  let test_to_string () =
    let http_only = Some true
    and path = Some "seppo.cgi"
    and same_site = Some `Strict
    and max_age = Some (30. *. 60.)
    and secure = Some true in
    Cookie.to_string ?path ?secure ?http_only ?same_site "auth_until"
      "2022-04-08T22:30:07Z"
    |> Assert2.equals_string "cookie 1"
      "auth_until=2022-04-08T22:30:07Z; Path=seppo.cgi; Secure; HttpOnly; \
       SameSite=Strict";
    Cookie.to_string ?max_age ?path ?secure ?http_only ?same_site "auth"
      "yes"
    |> Assert2.equals_string "cookie 2"
      "auth=yes; Max-Age=1800; Path=seppo.cgi; Secure; HttpOnly; \
       SameSite=Strict";
    assert true

  let test_of_string () =
    let c = Cookie.to_string "#Seppo!" "foo" in
    c |> Assert2.equals_string "test_of_string 0" "#Seppo!=foo";
    let v = match c |> Cookie.of_string with
      | ("#Seppo!", v) :: [] -> v
      | _ -> assert false
    in
    v |> Assert2.equals_string "test_of_string 10 v" "foo";
    assert true
end

module Form = struct
  let test_of_channel () =
    let ic = "data/cgi_" ^ "2022-04-05T125146.post" |> open_in in
    let fv = ic |> Http.Form.of_channel in
    ic |> close_in;
    (match fv with
     | [ (k0, [ v0 ]); (k1, [ v1 ]); (k2, [ v2 ]); (k3, [ v3 ]) ] ->
       k0 |> Assert2.equals_string "key 0" "login";
       v0 |> Assert2.equals_string "val 0" "demo";
       k1 |> Assert2.equals_string "key 1" "password";
       v1 |> Assert2.equals_string "val 1" "demodemodemo";
       k2 |> Assert2.equals_string "key 2" "token";
       v2
       |> Assert2.equals_string "val 2"
         "ff13e7eaf9541ca2ba30fd44e864c3ff014d2bc9";
       k3 |> Assert2.equals_string "key 3" "returnurl";
       v3
       |> Assert2.equals_string "val 3" "https://demo.mro.name/shaarligo/o/p/";
       assert true
     | _ -> assert false);
    (* match
       fv
       |> Http.Form.filter_sort_keys
         [ "login"; "password"; "token"; "returnurl" ]
       with
       | [ (k0, [ v0 ]); (k1, [ v1 ]); (k2, [ v2 ]); (k3, [ v3 ]) ] ->
       k0 |> Assert2.equals_string "key 0" "login";
       v0 |> Assert2.equals_string "val 0" "demo";
       k1 |> Assert2.equals_string "key 1" "password";
       v1 |> Assert2.equals_string "val 1" "demodemodemo";
       k2 |> Assert2.equals_string "key 3" "returnurl";
       v2
       |> Assert2.equals_string "val 3" "https://demo.mro.name/shaarligo/o/p/";
       k3 |> Assert2.equals_string "key 2" "token";
       v3
       |> Assert2.equals_string "val 2"
         "ff13e7eaf9541ca2ba30fd44e864c3ff014d2bc9";
       assert true
       | _ -> assert false *);
    assert true

  let test_to_html () =
    let defs = [
      ("ka", (Ok "va", "text", [("autofocus",""); ("pattern", {|^\S+$|})]));
    ] in
    (match List.assoc_opt "ka" defs with
     | Some (Ok v,_,_) -> v
     | _ -> "foo")
    |> Assert2.equals_string "http_test.test_to_htmOption.val 10" "va";
    assert true

  let test_from_html () =
    let pred ty valu (na,va) =
      Result.bind
        valu
        (fun v ->
           match v with
           | None -> Ok None
           | Some v as vv ->
             match ty,na with
             | _,"pattern" ->
               Logr.debug (fun m -> m "    '%s' ~ /%s/" v va);
               Ok vv
             | _ ->
               Logr.debug (fun m -> m "    ignored %s='%s'" na va);
               Ok vv)
    in
    let string (name,(ty,preds)) vals =
      let v = Option.bind
          (List.assoc_opt name vals)
          (fun v -> Some (v |> String.concat "")) in
      List.fold_left (pred ty) (Ok v) preds in
    let _validate defs vals =
      Logr.debug (fun m -> m "Form.validate");
      let field init (name,(ty,preds)) =
        match string (name,(ty,preds)) vals with
        | Error _ as inp ->
          (match init with
           | Error a -> Error (inp :: a)
           | Ok a    -> Error (inp :: a)
          )
        | Ok _ as inp ->
          (match init with
           | Error a -> Error (inp :: a)
           | Ok a    -> Ok    (inp :: a)
          )
      in
      List.fold_left field (Ok []) defs
    in
    let def0 = ("ka", ("text", [("autofocus",""); ("pattern", {|^\S+$|})])) in
    let _defs = [ def0; ] in
    let vals = [
      ("ka", ["vb"]);
    ] in
    (* match _validate defs vals with
       | Ok res -> List.assoc_opt "ka" res
                 |> Option.value ~default:(Ok None)
                 |> Result.get_ok
                 |> Option.get
                 |> Assert2.equals_string "http_test.test_from_html 10" "vb"
       | _ -> assert false); *)
    let ( let* ) = Result.bind in
    let run () =
      let* k = string def0 vals in
      Ok k in
    (match run() with
     | Ok (Some v)   -> v |> Assert2.equals_string "http_test.test_from_html 20" "vb"
     | _ -> assert true);
    assert true

  let test_from_html1 () =
    let i0 : Http.Form.input = ("k0", ("text", [
        ("autofocus", "autofocus");
        ("required",  "required");
        ("pattern",   {|^[a-z][0-9]+$|});
      ])) in
    let i1 = ("k1", ("text", [
        ("required",  "required");
        ("minlength", "1");
        ("maxlength", "50");
        ("pattern",   {|^v.$|});
      ])) in
    let vals : Http.Form.t = [
      ("k0", ["v0"]);
      ("k1", ["v1"]);
    ] in
    let ( let* ) = Result.bind in
    let run () =
      let* v0 = vals |> Http.Form.string i0 in
      let* v1 = Http.Form.string i1 vals in
      v0 |> Assert2.equals_string "http_test.test_from_html 25" "v0";
      v1 |> Assert2.equals_string "http_test.test_from_html 35" "v1";
      Ok () in
    (match run() with
     | Error (_,e) -> e |> Assert2.equals_string "http_test.test_from_html 95" ""
     | _    -> ())
end

let test_headers () =
  [ ("A", "a"); ("B", "b") ] @ [ ("C", "c") ]
  |> Cohttp.Header.of_list
  |> Cohttp.Header.to_string
  |> Assert2.equals_string "test_headers 1" "A: a\r\nB: b\r\nC: c\r\n\r\n";
  [ ("A", "a"); ("B", "b") ] @ [ ("C", "c") ]
  |> Cohttp.Header.of_list
  |> Cohttp.Header.to_frames
  |> String.concat "\n"
  |> Assert2.equals_string "test_headers 2" "A: a\nB: b\nC: c";
  assert true

(*
 * https://datatracker.ietf.org/doc/html/draft-cavage-http-signatures-12#appendix-C.2
 *)
let test_sign_basic () =
  let open Cohttp in
  let h = [
    ("(request-target)", "post /foo?param=value&pet=dog");
    ("host", "example.com");
    ("date", "Sun, 05 Jan 2014 21:31:40 GMT");
  ] |> Header.of_list in
  h
  |> Header.to_frames
  |> String.concat "\n"
  |> Assert2.equals_string "test_sign_basic frames"
    "(request-target): post /foo?param=value&pet=dog\n\
     host: example.com\n\
     date: Sun, 05 Jan 2014 21:31:40 GMT"
  ;
  Lwt.bind
    (As2.PubKeyPem.pk_from_pem ("data/cavage.priv.pem", "data/cavage.pub.pem"))
    (fun (_pub, pk) ->
       h
       |> Header.to_frames
       |> String.concat "\n"
       |> As2.PubKeyPem.sign pk
       |> Lwt.return
    )
  |> Lwt_main.run
  |> Assert2.equals_string "test_sign_basic sig"
    "qdx+H7PHHDZgy4y/Ahn9Tny9V3GP6YgBPyUXMmoxWtLbHpUnXS2mg2+SbrQDMCJypxBLSPQR2aAjn7ndmw2iicw3HMbe8VfEdKFYRqzic+efkb3nndiv/x1xSHDJWeSWkx3ButlYSuBskLu6kd9Fswtemr3lgdDEmn04swr2Os0="

(*
 * https://datatracker.ietf.org/doc/html/draft-cavage-http-signatures-12#appendix-C.3
 *)
let test_sign_all_headers () =
  let open Cohttp in
  let h = [
    ("(request-target)", "post /foo?param=value&pet=dog");
    ("(created)", "1402170695");
    ("(expires)", "1402170699");
    ("host", "example.com");
    ("date", "Sun, 05 Jan 2014 21:31:40 GMT");
    ("content-type", "application/json");
    ("digest", "SHA-256=X48E9qOokqqrvdts8nOJRJN3OWDUoyWxBf7kbu9DBPE=");
    ("content-length", "18");
  ] |> Header.of_list in
  h
  |> Header.to_frames
  |> String.concat "\n"
  |> Assert2.equals_string "test_sign_all_headers frames"
    "(request-target): post /foo?param=value&pet=dog\n\
     (created): 1402170695\n\
     (expires): 1402170699\n\
     host: example.com\n\
     date: Sun, 05 Jan 2014 21:31:40 GMT\n\
     content-type: application/json\n\
     digest: SHA-256=X48E9qOokqqrvdts8nOJRJN3OWDUoyWxBf7kbu9DBPE=\n\
     content-length: 18"
  ;
  Lwt.bind
    (As2.PubKeyPem.pk_from_pem ("data/cavage.priv.pem", "data/cavage.pub.pem"))
    (fun (_pub, pk) ->
       h
       |> Header.to_frames
       |> String.concat "\n"
       |> As2.PubKeyPem.sign pk
       |> Lwt.return
    )
  |> Lwt_main.run
  (*  |> Assert2.equals_string "test_sign sig"
        "vSdrb+dS3EceC9bcwHSo4MlyKS59iFIrhgYkz8+oVLEEzmYZZvRs8rgOp+63LEM3v+MFHB32NfpB2bEKBIvB1q52LaEUHFv120V01IL+TAD48XaERZFukWgHoBTLMhYS2Gb51gWxpeIq8knRmPnYePbF5MOkR0Zkly4zKH7s1dE="
  *)
  |> Assert2.equals_string "test_sign_all_headers sig"
    "nAkCW0wg9AbbStQRLi8fsS1mPPnA6S5+/0alANcoDFG9hG0bJ8NnMRcB1Sz1eccNMzzLEke7nGXqoiJYZFfT81oaRqh/MNFwQVX4OZvTLZ5xVZQuchRkOSO7b2QX0aFWFOUq6dnwAyliHrp6w3FOxwkGGJPaerw2lOYLdC/Bejk="

let test_signed_headers () =
  let open Cohttp in
  (* values from
     https://datatracker.ietf.org/doc/html/draft-cavage-http-signatures-12#appendix-C.3
  *)
  let id = Uri.of_string "https://example.com/actor/"
  and dgst = "SHA-256=X48E9qOokqqrvdts8nOJRJN3OWDUoyWxBf7kbu9DBPE="
  and date,_,_ = Ptime.of_rfc3339 "2014-01-05T22:31:40+01:00" |> Result.get_ok
  and uri = Uri.of_string "https://example.com/foo?param=value&pet=dog" in
  let pk = Lwt.bind
      (As2.PubKeyPem.pk_from_pem ("data/cavage.priv.pem", "data/cavage.pub.pem"))
      (fun (_pub, pk) -> pk |> Lwt.return)
           |> Lwt_main.run in
  Http.signed_headers (As2.PubKeyPem.sign pk) id date dgst uri
  |> Header.to_frames
  |> String.concat "\n"
  |> Assert2.equals_string "test_signed_headers 1"
    "host: example.com\n\
     date: Sun, 05 Jan 2014 21:31:40 GMT\n\
     digest: SHA-256=X48E9qOokqqrvdts8nOJRJN3OWDUoyWxBf7kbu9DBPE=\n\
     signature: \
     keyId=\"https://example.com/actor/#main-key\",\
     algorithm=\"rsa-sha256\",\
     headers=\"(request-target) host date digest\",\
     signature=\"WC34OEWXgO0viIZAu5qnBcKj5nOMlgjs0ASxgJPYX9x1VtKrYRRhAosH7ixFnkJneSHGn8yY9lowNvbdBg+ZsINx6P0e1WyB0YJbwsREYKYpG1sjwS3R3iCXmXf3m+txiCNhFcbbvb0Grq3wbAWGB0VW7ymI6AHixDXFLD5IYl4=\""

let () =
  Unix.chdir "../../../test/";
  Request.test_uri ();
  Cookie.test_rfc1123 ();
  Cookie.test_to_string ();
  Cookie.test_of_string ();
  Form.test_of_channel ();
  Form.test_to_html ();
  Form.test_from_html ();
  Form.test_from_html1 ();
  test_headers ();
  test_sign_basic ();
  test_sign_all_headers ();
  test_signed_headers ();
  assert true
