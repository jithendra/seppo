(* *)
open Seppo_lib

let test_ubase () =
  let nfc = "V\197\169 Ng\225\187\141c Phan"
  and nfd = "Vu\204\131 Ngo\204\163c Phan" in
  Assert2.equals_string "uhu0" "Vũ Ngọc Phan" nfc;
  Assert2.equals_string "uhu1" "Vũ Ngọc Phan" nfd;
  Assert2.equals_string "uhu2" "Vu Ngoc Phan" (Ubase.from_utf8 nfc);
  Assert2.equals_string "uhu3" "Vu Ngoc Phan" (Ubase.from_utf8 nfd)

let test_regexp () =
  let t0 = Str.regexp_case_fold ".*yst.*" in
  assert (Str.string_match t0 "haystack" 0);
  let t2 = Str.regexp_string_case_fold "ySt" in
  assert (2 = Str.search_forward t2 "haystack" 0);
  assert (2 = try Str.search_forward t2 "haystack" 0 with Not_found -> -1);
  assert (-1 = try Str.search_forward t2 "hay_stack" 0 with Not_found -> -1);
  assert (0 <= try Str.search_forward t2 "haystack" 0 with Not_found -> -1)

(*
  Assert2.equals_int "url_cleaner" 5 (List.length c0.url_cleaner);
  Assert2.equals_int "posse" 2 (List.length c0.posse)
*)

let test_needle () =
  let t = Str.regexp_case_fold ".*yst.*" in
  Assert2.equals_int "foo0" 1 (Search.string_rank t "haystack")

let test_needles () =
  let tp = [ "föo"; "bär"; "báz" ] |> Search.needles_prepare in
  ("my fÓo", "", "") |> Search.entry_rank tp |> Assert2.equals_int "uhu" 2

let test_emoji () =
  "my 😷 ö" |> Ubase.from_utf8 |> Assert2.equals_string "wtf" "my 😷 o"

let () =
  Unix.chdir "../../../test/";
  test_ubase ();
  test_regexp ();
  test_needle ();
  test_needles ();
  test_emoji ()
