(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Web.
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

open Seppo_lib

let test_plain2html () =
  "a\nb\nc https://Seppo.Social/d/?e#f "
  |> Lexing.from_string
  |> Plain2html.url (Buffer.create 50)
  |> Buffer.contents
  |> Assert2.equals_string "test_plain2html 1"
    "a<br/>\n\
     b<br/>\n\
     c <a href=\"https://seppo.social/d/?e#f\">Seppo.Social/d/?e#f</a> "

let () =
  Unix.chdir "../../../test/";
  test_plain2html ();
  assert true

