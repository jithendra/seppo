(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Web.
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

open Seppo_lib

let test_www_base () =
  (match
     "/a/b/c"
     |> String.split_on_char '/'
     |> List.rev
     |> List.cons "."
   with
   | [ d; c; b; a; v ] ->
     v |> Assert2.equals_string "file_test.test_www_base 1.v" "";
     a |> Assert2.equals_string "file_test.test_www_base 1.a" "a";
     b |> Assert2.equals_string "file_test.test_www_base 1.b" "b";
     c |> Assert2.equals_string "file_test.test_www_base 1.c" "c";
     d |> Assert2.equals_string "file_test.test_www_base 1.d" ".";
   | _ ->
     "woring" |> Assert2.equals_string "file_test.test_www_base 1.wrong" "");
  (match
     "/a/b/c"
     |> String.split_on_char '/'
     |> List.rev
     |> File.find_path_tail (fun s -> Ok (s |> String.equal "/b/c"))
   with
   | Ok s -> s |> Assert2.equals_string "file_test.test_www_base 1" "/b/c"
   | Error e -> e |> Assert2.equals_string "file_test.test_www_base 2" "");
  let wefi u =
    let wkwf =  ".well-known/webfinger/" in
    let i = u |> Uri.path |> String.split_on_char '/' |> List.length in
    ((List.init (i-2) (fun _ -> "../") |> String.concat "") ^ wkwf)
  in
  "https://ursi@demo.mro.name/" |> Uri.of_string 
  |> wefi |> Assert2.equals_string "file_test.test_www_base 6" ".well-known/webfinger/";
  "https://ursi@demo.mro.name/seppo/" |> Uri.of_string 
  |> wefi |> Assert2.equals_string "file_test.test_www_base 6" "../.well-known/webfinger/"


let test_path_climb () =
  let u = Uri.make ~scheme:"https" ~userinfo:"foo" ~host:"example.com" ~path:"/a/b/c/" () in
  u |> Uri.path |> Assert2.equals_string "file_test.test_path_climb 1" "/a/b/c/";
  let i = u |> Uri.path |> String.split_on_char '/' |> List.length in
  List.init (i-2) (fun _ -> "..") |> String.concat "/"
  |> Assert2.equals_string "file_test.test_path_climb 1" "../../..";
  "z" |> Assert2.equals_string "file_test.test_path_climb 1" "z"

let test_path_hd_tl () =
  "/foo/bar/baz"
  |> File.Path.hd '/'
  |> Option.get |> Assert2.equals_string "test_request hd 0" "";
  let t0 = "/foo/bar/baz" |> File.Path.tl '/' |> Option.get in
  t0 |> Assert2.equals_string "test_request tl 0" "foo/bar/baz";
  t0 |> File.Path.hd '/'
  |> Option.get |> Assert2.equals_string "test_request hd 1" "foo";
  let t1 = t0 |> File.Path.tl '/' |> Option.get in
  t1 |> Assert2.equals_string "test_request tl 0" "bar/baz";
  let t2 = t1 |> File.Path.tl '/' |> Option.get in
  t2 |> Assert2.equals_string "test_request t2 0" "baz";
  assert (t2 |> File.Path.tl '/' |> Option.is_none);
  assert true

let () =
  Unix.chdir "../../../test/";
  test_www_base ();
  test_path_climb ();
  test_path_hd_tl ();
  assert true

