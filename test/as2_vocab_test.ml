
open Seppo_lib

(* diaspora profile json e.g.

   https://pod.diaspora.software/u/hq
   https://pod.diaspora.software/people/7bca7c80311b01332d046c626dd55703
*)

let test_person () =
  let empty : As2_vocab.Types.person = {
    id                          = Uri.empty;
    inbox                       = Uri.empty;
    outbox                      = Uri.empty;
    followers                   = None;
    following                   = None;
    attachment                  = [];
    discoverable                = false;
    icon                        = None;
    image                       = None;
    manually_approves_followers = false;
    name                        = None;
    preferred_username          = None;
    public_key                  = {
      id                        = Uri.empty;
      owner                     = Uri.empty;
      pem                       = "";
      signatureAlgorithm        = None;
    };
    summary                     = None;
    url                         = None;
  } in
  let base = (Uri.of_string "https://example.com/su/") in
  {empty with id=Uri.make ~path:"id/" ()}
  |> As2_vocab.Encode.person ~base
  |> Ezjsonm.value_to_string ~minify:false
  |> Assert2.equals_string "as2_vocab_test test_person 10" {|{
  "@context": [
    "https://www.w3.org/ns/activitystreams",
    "https://w3id.org/security/v1",
    {
      "schema": "http://schema.org#",
      "PropertyValue": "schema:PropertyValue",
      "value": "schema:value",
      "@language": "und"
    }
  ],
  "type": "Person",
  "id": "https://example.com/su/id/",
  "inbox": "https://example.com/su/",
  "outbox": "https://example.com/su/",
  "publicKey": {
    "id": "https://example.com/su/",
    "owner": "https://example.com/su/",
    "publicKeyPem": ""
  },
  "manuallyApprovesFollowers": false,
  "discoverable": false,
  "attachment": []
}|};
  let p = {|{
  "@context": [
    "https://www.w3.org/ns/activitystreams",
    "https://w3id.org/security/v1",
    {
      "schema": "http://schema.org#",
      "PropertyValue": "schema:PropertyValue",
      "value": "schema:value",
      "@language": "und"
    }
  ],
  "type": "Person",
  "id": "https://example.com/su/id/",
  "inbox": "https://example.com/su/",
  "outbox": "https://example.com/su/",
  "publicKey": {
    "id": "https://example.com/su/",
    "owner": "https://example.com/su/",
    "publicKeyPem": ""
  },
  "manuallyApprovesFollowers": false,
  "discoverable": false,
  "attachment": []
}|}
          |> Ezjsonm.value_from_string
          |> As2_vocab.Decode.person
          |> Result.get_ok in
  p.id
  |> Uri.to_string
  |> Assert2.equals_string "as2_vocab_test test_person 20" "https://example.com/su/id/"


let test_webfinger_sunshine () =
  let q = File.in_channel "data/webfinger.mini.json" Ezjsonm.from_channel
          |> As2_vocab.Activitypub.Decode.Webfinger.query_result
          |> Result.get_ok in
  q.subject |> Assert2.equals_string "ap_vocab_test test_a 10" "acct:ursi@example.com";
  q.links |> List.length |> Assert2.equals_int "ap_vocab_test test_a 20" 3;
  let q = File.in_channel "data/webfinger.zap.json" Ezjsonm.from_channel
          |> As2_vocab.Activitypub.Decode.Webfinger.query_result
          |> Result.get_ok in
  q.subject |> Assert2.equals_string "as2_vocab_test test_sunshine_atom 10" "acct:mike@macgirvin.com";
  q.links |> List.length |> Assert2.equals_int "as2_vocab_test test_sunshine_atom 20" 3;
  let q = File.in_channel "data/webfinger.atom.json" Ezjsonm.from_channel
          |> As2_vocab.Activitypub.Decode.Webfinger.query_result
          |> Result.get_ok in
  q.subject |> Assert2.equals_string "as2_vocab_test test_sunshine_atom 10" "acct:ursi@example.com";
  q.links |> List.length |> Assert2.equals_int "as2_vocab_test test_sunshine_atom 20" 3

let test_profile_sunshine () =
  let q = File.in_channel "data/profile.mini.json" Ezjsonm.from_channel
          |> As2_vocab.Activitypub.Decode.person
          |> Result.get_ok in
  q.name |> Option.get |> Assert2.equals_string "as2_vocab_test test_profile_sunshine 10" "Yet Another #Seppo! 🌻";
  q.preferred_username |> Option.get |> Assert2.equals_string "as2_vocab_test test_profile_sunshine 10" "ursi";
  q.attachment |> List.length |> Assert2.equals_int "" 0;
  let q = File.in_channel "data/profile.mast.json" Ezjsonm.from_channel
          |> As2_vocab.Activitypub.Decode.person
          |> Result.get_ok in
  (match q.attachment with
   | [a;b;_] ->
     a.name |> Assert2.equals_string "as2_vocab_test test_profile_sunshine 40" "Support";
     b.value |> Assert2.equals_string "as2_vocab_test test_profile_sunshine 50" {|<a href="https://seppo.social">Seppo.Social</a>|};
   | _ -> Assert2.equals_int "" 0 1
  );
  q.image |> Option.get |> Uri.to_string |> Assert2.equals_string "as2_vocab_test test_profile_sunshine 60" "https://example.com/me-banner.jpg";
  "" |> Assert2.equals_string "test_profile_sunshine" "";
  let q = File.in_channel "data/profile.akkoma.json" Ezjsonm.from_channel
          |> As2_vocab.Activitypub.Decode.person
          |> Result.get_ok in
  q.name |> Option.get |> Assert2.equals_string "as2_vocab_test test_profile_sunshine 10" "Kinetix";
  let q = File.in_channel "data/profile.gnusocial.json" Ezjsonm.from_channel
          |> As2_vocab.Activitypub.Decode.person
          |> Result.get_ok in
  q.preferred_username |> Option.get |> Assert2.equals_string "as2_vocab_test test_profile_sunshine 10" "diogo";
  let q = File.in_channel "data/profile.lemmy.json" Ezjsonm.from_channel
          |> As2_vocab.Activitypub.Decode.person
          |> Result.get_ok in
  q.preferred_username |> Option.get |> Assert2.equals_string "as2_vocab_test test_profile_sunshine 10" "nutomic";
  let q = File.in_channel "data/profile.mast.json" Ezjsonm.from_channel
          |> As2_vocab.Activitypub.Decode.person
          |> Result.get_ok in
  q.preferred_username |> Option.get |> Assert2.equals_string "as2_vocab_test test_profile_sunshine 10" "ursi";
  let q = File.in_channel "data/profile.peertube.json" Ezjsonm.from_channel
          |> As2_vocab.Activitypub.Decode.person
          |> Result.get_ok in
  q.preferred_username |> Option.get |> Assert2.equals_string "as2_vocab_test test_profile_sunshine 10" "edps";
  let q = File.in_channel "data/profile.zap.json" Ezjsonm.from_channel
          |> As2_vocab.Activitypub.Decode.person
          |> Result.get_ok in
  q.preferred_username |> Option.get |> Assert2.equals_string "as2_vocab_test test_profile_sunshine 10" "mike";
  assert true

let test_encode () =
  let minify = false in
  let base = Uri.of_string "http://example.com/foo/" in
  let module E = Decoders_ezjsonm.Encode in
  E.encode_string E.obj [("k", `String "v")]
  |> Assert2.equals_string "as2_vocab_test/test_encode 10" {|{"k":"v"}|};
  Ezjsonm.value_to_string (E.obj [("k", `String "v")])
  |> Assert2.equals_string "as2_vocab_test/test_encode 10" {|{"k":"v"}|};

  let e : Rfc4287.Entry.t = {
    id         = Uri.make ~path:"a/b/" ?fragment:(Some "c") ();
    lang       = Rfc4287.Rfc4646 "de";
    author     = Uri.make ~userinfo:"bar" ();
    title      = "uhu";
    published  = (Rfc4287.Rfc3339 "2023-03-07T01:23:45Z") ;
    updated    = (Rfc4287.Rfc3339 "2023-03-07T01:23:46Z");
    links      = [];
    categories = [
      ((Label (Single "lbl")),(Term (Single "term")),Uri.make ~path:"t/" ());
    ];
    content    = "Das war aber einfach";
  } in
  let n = As2.Note.of_rfc4287 e in
  let j = n |> As2_vocab.Encode.note ~base in
  j |> Ezjsonm.value_to_string ~minify
  |> Assert2.equals_string "as2_vocab_test/test_encode 10" {|{
  "type": "Note",
  "id": "http://example.com/foo/a/b/#c",
  "actor": "http://example.com/foo/activitypub/",
  "to": [
    "https://www.w3.org/ns/activitystreams#Public"
  ],
  "cc": [
    "http://example.com/foo/activitypub/followers/"
  ],
  "mediaType": "text/plain; charset=utf8",
  "content": "Das war aber einfach",
  "sensitive": false,
  "summary": "uhu",
  "published": "2023-03-07T01:23:45Z",
  "tags": [
    {
      "type": "Hashtag",
      "href": "http://example.com/foo/t/term/",
      "name": "#lbl"
    }
  ]
}|};

  let co : 'a As2_vocab.Types.collection_page = {
    id = Uri.of_string "http://example.com/foo/";
    current    = None;
    first      = None;
    is_ordered = true;
    items      = [As2.Note.mk_create n];
    last       = None;
    next       = None;
    part_of    = None;
    prev       = None;
    total_items= None;
  } in
  let j = As2_vocab.Encode.collection_page ~base
      (As2_vocab.Encode.create ~base
         (As2_vocab.Encode.note ~base))
      co in
  j |> Ezjsonm.value_to_string ~minify
  |> Assert2.equals_string "as2_vocab_test/test_encode 20" {|{
  "@context": [
    "https://www.w3.org/ns/activitystreams",
    "https://w3id.org/security/v1",
    {
      "schema": "http://schema.org#",
      "PropertyValue": "schema:PropertyValue",
      "value": "schema:value",
      "@language": "und"
    }
  ],
  "type": "OrderedCollectionPage",
  "id": "http://example.com/foo/",
  "orderedItems": [
    {
      "type": "Create",
      "id": "http://example.com/foo/a/b/#c/Create",
      "actor": "http://example.com/foo/activitypub/",
      "published": "2023-03-07T01:23:45Z",
      "to": [
        "https://www.w3.org/ns/activitystreams#Public"
      ],
      "cc": [
        "http://example.com/foo/activitypub/followers/"
      ],
      "directMessage": false,
      "object": {
        "type": "Note",
        "id": "http://example.com/foo/a/b/#c",
        "actor": "http://example.com/foo/activitypub/",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public"
        ],
        "cc": [
          "http://example.com/foo/activitypub/followers/"
        ],
        "mediaType": "text/plain; charset=utf8",
        "content": "Das war aber einfach",
        "sensitive": false,
        "summary": "uhu",
        "published": "2023-03-07T01:23:45Z",
        "tags": [
          {
            "type": "Hashtag",
            "href": "http://example.com/foo/t/term/",
            "name": "#lbl"
          }
        ]
      }
    }
  ]
}|};

  assert true

(* https://www.w3.org/TR/activitystreams-core/#ex17-jsonld *)
let test_ex15_note () =
  let j = File.in_channel "data/note.as2_core.ex15.json" Ezjsonm.value_from_channel in
  (match j with
   | `O [
       "@context", _ ;
       "summary", _ ;
       "type", `String "Create" ;
       "actor",  _ ;
       "object", `O [
         "type", `String "Note" ;
         _]
     ] -> assert true
   | _ -> assert false);
  let p = j
          |> As2_vocab.Decode.(create note)
          |> Result.is_error in
  assert p
  (*
  _p.obj.summary |> Option.get |> Assert2.equals_string "" "";
*)

let () =
  Unix.chdir "../../../test/";
  test_person ();
  test_webfinger_sunshine ();
  test_profile_sunshine ();
  test_encode ();
  test_ex15_note ();
  assert true
