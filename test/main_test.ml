(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Web.
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

let tail x =
  Assert2.equals_string "uu" "ok" (if x |> Result.is_ok then "ok" else "no")

open Seppo_lib

let fn = Mapcdb.Cdb "tmp/main.cdb"
let Mapcdb.Cdb fn' = fn

let test_sift_tags () =
  Logr.debug (fun m -> m "%s.%s" "Main_test" "test_sift_tags");
  let open Rfc4287 in
  Unix.(try unlink fn' with Unix_error (ENOENT, "unlink", _) -> ());
  File.touch fn';
  let published = Rfc3339 "2023-03-09T12:34:56+01:00" in
  let author = Uri.make ~userinfo:"uhu" () in
  let lang = Rfc4646 "en" in
  let uri = Uri.empty in
  let title = {|Just a #note|} in
  let content = {|th some #more #Hashtags.|} in
  (let* e = Entry.from_text_plain ~published ~author ~lang ~uri title content in
   let* e : Entry.t = Main.sift_tags fn e in
   let li = e.categories in
   li |> List.length |> Assert2.equals_int "Main_test.test_sift_tags 10" 3;
   let (Category.Label (Single la),Category.Term (Single te),sc) = li |> List.hd in
   la |> Assert2.equals_string "Main_test.test_sift_tags 10" "note";
   te |> Assert2.equals_string "Main_test.test_sift_tags 20" "note";
   sc |> Uri.to_string |> Assert2.equals_string "Main_test.test_sift_tags 30" "o/t/";
   let li = li |> List.tl in
   let (Category.Label (Single la),Category.Term (Single te),sc) = li |> List.hd in
   la |> Assert2.equals_string "Main_test.test_sift_tags 40" "more";
   te |> Assert2.equals_string "Main_test.test_sift_tags 50" "more";
   sc |> Uri.to_string |> Assert2.equals_string "Main_test.test_sift_tags 60" "o/t/";
   let li = li |> List.tl in
   let (Category.Label (Single la),Category.Term (Single te),sc) = li |> List.hd in
   la |> Assert2.equals_string "Main_test.test_sift_tags 70" "Hashtags";
   te |> Assert2.equals_string "Main_test.test_sift_tags 80" "Hashtags";
   sc |> Uri.to_string |> Assert2.equals_string "Main_test.test_sift_tags 90" "o/t/";
   let li = li |> List.tl in
   li |> List.length |> Assert2.equals_int "Main_test.test_sift_tags 10" 0;
   Ok e) |> tail;
  assert true

let test_tmap_from_cdb () =
  let tm = Tag.Tmap.empty in
  let tm = Tag.Tmap.add "a" "A" tm in
  Tag.Tmap.find "a" tm |> Assert2.equals_string "Main_test.test_tmap_from_cdb 10" "A";
  assert true

let () =
  Unix.chdir "../../../test/";
  test_sift_tags ();
  test_tmap_from_cdb ();
  assert true
