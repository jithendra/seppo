(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Web.
 *
 * as2_test.ml
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

open Seppo_lib

let test_webfinger_handle () =
  Logr.debug (fun m -> m "as2_test.testwebfinger_handle");
  let open As2.Webfinger.Client in
  (Localpart "uh", Host "ah.oh")
  |> to_string
  |> Assert2.equals_string "as2_test.test_webfinger_handle 0" "@uh@ah.oh";

  (Localpart "uh", Host "example.com")
  |> well_known_uri
  |> Uri.to_string
  |> Assert2.equals_string "as2_test.test_webfinger_handle 1" "https://example.com/.well-known/webfinger?resource=acct:uh@example.com";

  (match "@uh@ah.oh" |> As2.Webfinger.Client.from_string with
   | Ok (Localpart "uh", Host "ah.oh") -> assert true
   | Ok _ -> Assert2.equals_string "as2_test.test_webfinger_handle 1.0" "h" "i"
   | Error e -> Assert2.equals_string "as2_test.test_webfinger_handle 1.1" "" e
  );

  "@ok@example.com"
  |> As2.Webfinger.Client.from_string
  |> Result.get_ok
  |> As2.Webfinger.Client.to_uri
  |> Uri.to_string
  |> Assert2.equals_string "as2_test.test_webfinger_handle 1" "webfinger://ok@example.com";
  assert true

let test_webfinger () =
  Logr.debug (fun m -> m "as2_test.test_webfinger");
  As2.Webfinger.jsonm (Auth.Uid "usr", Uri.of_string "scheme://example.com/p/a/t/h/")
  |> Result.get_ok
  |> Ezjsonm.value_to_string ~minify:false
  |> Assert2.equals_string "test_webfinger"
    {|{
  "subject": "acct:usr@example.com",
  "aliases": [],
  "links": [
    {
      "href": "scheme://example.com/p/a/t/h/activitypub/profile.json",
      "rel": "self",
      "type": "application/activity+json"
    }
  ]
}|};
  assert true

let test_webfinger_decode () =
  Logr.debug (fun m -> m "as2_test.test_webfinger_decode");
  let j = File.in_channel "data/webfinger.gnusocial.json" Ezjsonm.value_from_channel in
  let q = As2_vocab.Decode.Webfinger.query_result j |> Result.get_ok in
  q.subject |> Assert2.equals_string "test_webfinger_decode 10" "acct:administrator@gnusocial.net";
  As2_vocab.Encode.Webfinger.query_result ~base:Uri.empty q
  |> Ezjsonm.value_to_string ~minify:false
  |> Assert2.equals_string "test_webfinger_decode 20" {|{
  "subject": "acct:administrator@gnusocial.net",
  "aliases": [
    "https://gnusocial.net/index.php/user/1",
    "https://gnusocial.net/administrator",
    "https://gnusocial.net/user/1",
    "https://gnusocial.net/index.php/administrator"
  ],
  "links": [
    {
      "href": "https://gnusocial.net/administrator",
      "rel": "http://webfinger.net/rel/profile-page",
      "type": "text/html"
    },
    {
      "rel": "http://ostatus.org/schema/1.0/subscribe",
      "template": "https://gnusocial.net/main/remotefollowsub?profile={uri}"
    },
    {
      "href": "https://gnusocial.net/index.php/user/1",
      "rel": "self",
      "type": "application/activity+json"
    }
  ]
}|};
  let j = File.in_channel "data/webfinger.pleroma.json" Ezjsonm.value_from_channel in
  let q = j |> As2_vocab.Decode.Webfinger.query_result |> Result.get_ok in
  q.subject |> Assert2.equals_string "test_webfinger_decode 30" "acct:gabek@social.gabekangas.com";
  assert true

let test_webfinger_sift () =
  Logr.debug (fun m -> m "as2_test.test_webfinger_sift");
  let p = As2.Webfinger.make (Auth.Uid "usr", Uri.of_string "https://example.com/sub/") in
  assert (p.links
          |> As2_vocab.Types.Webfinger.profile_page
          |> Option.is_none);
  p.links
  |> As2_vocab.Types.Webfinger.self_link
  |> Option.get
  |> Uri.to_string
  |> Assert2.equals_string "as2_test.test_webfinger_sift" "https://example.com/sub/activitypub/profile.json"

let test_digest_sha256 () =
  Logr.debug (fun m -> m "as2_test.test_digest_sha256");
  let (`Hex h) =
    "" |> Cstruct.of_string |> Mirage_crypto.Hash.SHA256.digest
    |> Hex.of_cstruct
  in
  (* https://de.wikipedia.org/wiki/SHA-2 *)
  h
  |> Assert2.equals_string "digest hex"
    "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855";
  "" |> Cstruct.of_string |> Mirage_crypto.Hash.SHA256.digest
  |> Cstruct.to_string |> Base64.encode_exn
  (* printf "%s" "" | openssl dgst -sha256 -binary | base64 *)
  |> Assert2.equals_string "digest base64"
    "47DEQpj8HBSa+/TImW+5JCeuQeRkm5NMpJWZG3hSuFU="

open As2


let test_digst () =
  Logr.debug (fun m -> m "as2_test.test_digst");
  "" |> Activity.digest
  |> Assert2.equals_string "test_digest"
    "SHA-256=47DEQpj8HBSa+/TImW+5JCeuQeRkm5NMpJWZG3hSuFU=";
  assert true

let test_person () =
  Logr.debug (fun m -> m "as2_test.test_actor");
  let base = "https://example.com/subb/activitypub/" |> Uri.of_string in
  let p : As2_vocab.Types.person = {
    id                         = Uri.empty;
    inbox                      = Uri.make ~path:"inbox/" ();
    outbox                     = Uri.make ~path:"outbox/" ();
    followers                  = Some (Uri.make ~path:"followers/" ());
    following                  = Some (Uri.make ~path:"following/" ());
    attachment                 = [
      { name  = "Support";
        value = "https://Seppo.Social/support"; };
      { name  = "Generator";
        value = "https://Seppo.Social"; };
      { name  = "Funding";
        value = "https://nlnet.nl/project/Seppo/"; };
    ];
    discoverable               = false;
    icon                       = Some (Uri.make ~path:"../my-icon.jpg"());
    image                      = Some (Uri.make ~path:"../my-banner.jpg"());
    manually_approves_followers= false;
    name                       = Some "Sepp";
    preferred_username         = Some "sepp";
    public_key                 = { id    = Uri.make ~fragment:"main-key" ();
                                   owner = Uri.empty;
                                   pem   = "foo";
                                   signatureAlgorithm = None; };
    summary                    = Some "sum";
    url                        = Some Uri.empty;
  } in 
  p |> As2_vocab.Encode.person ~base
  |> Ezjsonm.value_to_string ~minify:false
  |> Assert2.equals_string "as2_test.test_person 10" {|{
  "@context": [
    "https://www.w3.org/ns/activitystreams",
    "https://w3id.org/security/v1",
    {
      "schema": "http://schema.org#",
      "PropertyValue": "schema:PropertyValue",
      "value": "schema:value",
      "@language": "und"
    }
  ],
  "type": "Person",
  "id": "https://example.com/subb/activitypub/",
  "inbox": "https://example.com/subb/activitypub/inbox/",
  "outbox": "https://example.com/subb/activitypub/outbox/",
  "followers": "https://example.com/subb/activitypub/followers/",
  "following": "https://example.com/subb/activitypub/following/",
  "name": "Sepp",
  "url": "https://example.com/subb/activitypub/",
  "preferredUsername": "sepp",
  "summary": "sum",
  "publicKey": {
    "id": "https://example.com/subb/activitypub/#main-key",
    "owner": "https://example.com/subb/activitypub/",
    "publicKeyPem": "foo"
  },
  "manuallyApprovesFollowers": false,
  "discoverable": false,
  "attachment": [
    {
      "type": "PropertyValue",
      "name": "Support",
      "value": "https://Seppo.Social/support"
    },
    {
      "type": "PropertyValue",
      "name": "Generator",
      "value": "https://Seppo.Social"
    },
    {
      "type": "PropertyValue",
      "name": "Funding",
      "value": "https://nlnet.nl/project/Seppo/"
    }
  ],
  "icon": {
    "type": "Image",
    "url": "https://example.com/subb/my-icon.jpg"
  },
  "image": {
    "type": "Image",
    "url": "https://example.com/subb/my-banner.jpg"
  }
}|};
  p
  |> PersonX.xml ~base
  |> Ezxmlm.to_string ~decl:true ~dtd:""
  |> Assert2.equals_string "as2_test.test_person 20" {|<?xml version="1.0" encoding="UTF-8"?>

<rdf:RDF xmlns:as="https://www.w3.org/ns/activitystreams#" xmlns:ldp="http://www.w3.org/ns/ldp#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:schema="http://schema.org#" xmlns:sec="https://w3id.org/security#"><as:Person xml:lang="nl" rdf:about="https://example.com/subb/activitypub/"><as:attachment><schema:PropertyValue><as:name>Support</as:name><schema:value>https://Seppo.Social/support</schema:value></schema:PropertyValue></as:attachment><as:attachment><schema:PropertyValue><as:name>Funding</as:name><schema:value>https://nlnet.nl/project/Seppo/</schema:value></schema:PropertyValue></as:attachment><as:attachment><schema:PropertyValue><as:name>Generator</as:name><schema:value>https://Seppo.Social</schema:value></schema:PropertyValue></as:attachment><as:icon><as:Image><as:url rdf:resource="../my-icon.jpg"/></as:Image></as:icon><as:image><as:Image><as:url rdf:resource="../my-banner.jpg"/></as:Image></as:image><as:name>Sepp</as:name><as:summary>sum</as:summary></as:Person></rdf:RDF>|}

let test_actor () =
  Logr.debug (fun m -> m "as2_test.test_actor");
  Logr.info (fun m -> m "test_actor");
  ("data/2022-07-22-095632-heluecht_--_actor_as.json"
   |> Person.from_file |> Result.get_ok).inbox
  |> Uri.to_string
  |> Assert2.equals_string "test_actor friendica" "https://pirati.ca/inbox/heluecht";
  ("data/2022-11-18-173642-mro_--_actor_as_mas.json"
   |> Person.from_file |> Result.get_ok).inbox
  |> Uri.to_string
  |> Assert2.equals_string "test_actor mastodon" "https://digitalcourage.social/users/mro/inbox";
  ("data/profile.gnusocial.json"
   |> Person.from_file |> Result.get_ok).inbox
  |> Uri.to_string
  |> Assert2.equals_string "test_actor gnusocial" "https://social.hackersatporto.com/user/1/inbox.json";
  ("data/2022-07-22-103548-mro_--_actor_as_ple.json"
   |> Person.from_file |> Result.get_ok).inbox
  |> Uri.to_string
  |> Assert2.equals_string "test_actor pleroma" "https://pleroma.tilde.zone/users/mro/inbox";
  assert true

let test_examine_response () =
  {|{"error":"Unable to fetch key JSON at https://example.com/activitypub/#main-key"}|}
  |> As2.examine_response
  |> Result.get_error
  |> Assert2.equals_string "as2_test.test_examine_response" "Unable to fetch key JSON at https://example.com/activitypub/#main-key";
  assert true

let () =
  Unix.chdir "../../../test/";
  test_webfinger_handle ();
  test_webfinger ();
  test_webfinger_sift ();
  test_webfinger_decode ();
  test_digest_sha256 ();
  test_digst ();
  test_person ();
  test_actor ();
  test_examine_response ();
  assert true
