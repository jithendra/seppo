(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Web.
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

open Seppo_lib
open Rfc4287

let mk_sample () =
  let tag path = Category.((Label (Single path)), (Term (Single path)), tagu) in
  let e : Entry.t = {
    id         = "o/p-12/#23" |> Uri.of_string;
    lang       = Rfc4646 "en";
    author     = Uri.make ~userinfo:"fediverse" ~host:"mro.name" ();
    title      = "#Announce Seppo.Social v0.1 and Request for Comments.";
    published  = Rfc3339 "2023-02-11T11:07:23+01:00";
    updated    = Rfc3339 "2023-02-11T11:07:23+01:00";
    links      = [ "https://seppo.social/en/downloads/seppo-Linux-x86_64-0.1/" |> Uri.of_string  |> Link.make ];
    categories = [
      tag "ActivityPub";
      tag "Announce";
      tag "Fediverse";
      tag "Media";
      tag "permacomputing";
      tag "Seppo";
      tag "Social";
      tag "webfinger";
    ];
    content    = {|I am happy to announce the premiere release of #Seppo!, Personal #Social #Media under funding of NLnet.nl.

Find it at https://Seppo.Social/downloads/

It has no notable user facing #ActivityPub features so far, but

- easy setup of instance & account,
- #webfinger discoverability (from e.g. mastodon search),
- a welcoming, long-term reliable website.

I made this embarrassingly limited release to build awareness for low-barrier-entry internet services in general and especially in the field of personal communication as well as letting the #fediverse and #permacomputing communities know.

Your comments are very much appreciated.|};
  } in
  e

let tail x =
  Assert2.equals_string "uu" "ok" (if x |> Result.is_ok then "ok" else "no")

let test_encode () =
  Logr.info (fun m -> m "rfc4287_test.test_encode");
  let e = mk_sample () in
  e
  |> Entry.encode
  |> Csexp.to_string
  |> Assert2.equals_string "t 1" {|(2:id10:o/p-12/#234:lang2:en5:title53:#Announce Seppo.Social v0.1 and Request for Comments.6:author20://fediverse@mro.name9:published25:2023-02-11T11:07:23+01:007:updated25:2023-02-11T11:07:23+01:005:links((4:href57:https://seppo.social/en/downloads/seppo-Linux-x86_64-0.1/))10:categories((5:label11:ActivityPub4:term11:ActivityPub6:scheme4:o/t/)(5:label8:Announce4:term8:Announce6:scheme4:o/t/)(5:label9:Fediverse4:term9:Fediverse6:scheme4:o/t/)(5:label5:Media4:term5:Media6:scheme4:o/t/)(5:label14:permacomputing4:term14:permacomputing6:scheme4:o/t/)(5:label5:Seppo4:term5:Seppo6:scheme4:o/t/)(5:label6:Social4:term6:Social6:scheme4:o/t/)(5:label9:webfinger4:term9:webfinger6:scheme4:o/t/))7:content635:I am happy to announce the premiere release of #Seppo!, Personal #Social #Media under funding of NLnet.nl.

Find it at https://Seppo.Social/downloads/

It has no notable user facing #ActivityPub features so far, but

- easy setup of instance & account,
- #webfinger discoverability (from e.g. mastodon search),
- a welcoming, long-term reliable website.

I made this embarrassingly limited release to build awareness for low-barrier-entry internet services in general and especially in the field of personal communication as well as letting the #fediverse and #permacomputing communities know.

Your comments are very much appreciated.)|};
  e |> Entry.encode |> Csexp.to_string |> String.length |> Assert2.equals_int "t 0" 1341;
  match e
        |> Entry.encode
        |> Entry.decode with
  | Error e -> e |> Assert2.equals_string "t 2" ""
  | Ok e ->
    let Rfc4646 lang = e.lang
    and titl = e.title
    and Rfc3339 publ = e.published
    and Rfc3339 upda = e.updated
    and cont = e.content
    and li_a, li_b = match e.links with
      | [ {href; rel=None; title=None; } ] -> (href,"")
      | _ -> (Uri.make (), "ouch 301")
    and ca_a, ca_b, ca_c = match e.categories  with
      | (Label (Single a), Term (Single b), c) :: _ -> (a,b,c)
      | _ -> ("ouch 302", "", Uri.make ())
    in
    lang |> Assert2.equals_string "t 30" "en";
    titl |> Assert2.equals_string "t 40" "#Announce Seppo.Social v0.1 and Request for Comments.";
    publ |> Assert2.equals_string "t 50" "2023-02-11T11:07:23+01:00";
    upda |> Assert2.equals_string "t 60" "2023-02-11T11:07:23+01:00";
    e.links |> List.length |> Assert2.equals_int "t 61" 1;
    li_a |> Uri.to_string |> Assert2.equals_string "t 70" "https://seppo.social/en/downloads/seppo-Linux-x86_64-0.1/";
    li_b |> Assert2.equals_string "t 80" "";
    e.categories |> List.length |> Assert2.equals_int "t 81" 8;
    ca_a |> Assert2.equals_string "t 90" "webfinger";
    ca_b |> Assert2.equals_string "t 100" "webfinger";
    ca_c |> Uri.to_string |> Assert2.equals_string "t 101" "o/t/";
    cont |> Assert2.equals_string "t 110" {|I am happy to announce the premiere release of #Seppo!, Personal #Social #Media under funding of NLnet.nl.

Find it at https://Seppo.Social/downloads/

It has no notable user facing #ActivityPub features so far, but

- easy setup of instance & account,
- #webfinger discoverability (from e.g. mastodon search),
- a welcoming, long-term reliable website.

I made this embarrassingly limited release to build awareness for low-barrier-entry internet services in general and especially in the field of personal communication as well as letting the #fediverse and #permacomputing communities know.

Your comments are very much appreciated.|} ;
    e |> Storage.feed_urls
    |> List.map Uri.to_string |> String.concat " ; "
    |> Assert2.equals_string "t 110" "o/p/ ; activitypub/outbox/ ; o/d/2023-02-11/ ; o/t/webfinger/ ; o/t/Social/ ; o/t/Seppo/ ; o/t/permacomputing/ ; o/t/Media/ ; o/t/Fediverse/ ; o/t/Announce/ ; o/t/ActivityPub/";
    assert true


let test_from_plain_text () =
  Logr.info (fun m -> m "rfc4287_test.test_from_plain_text");
  let published = Rfc3339 "2023-02-14T01:23:45+01:00" in
  let author = Uri.make ~userinfo:"uhu" () in
  let lang = Rfc4646 "nl" in
  let uri = Uri.of_string "https://nlnet.nl/projects/Seppo/" in
  (let* n = Entry.from_text_plain ~published ~author ~lang ~uri "Hello, world!" "a new Note." in
   let ti = n.title in
   let co = n.content in
   ti |> Assert2.equals_string "rfc4287 1" "Hello, world!";
   n.links |> List.length |> Assert2.equals_int "rfc4287 2" 1;
   n.categories |> List.length |> Assert2.equals_int "rfc4287 3" 0;
   co |> Assert2.equals_string "rfc4287 4" "a new Note.";
   n |> Entry.encode
   |> Csexp.to_string
   |> Assert2.equals_string "rfc4287 5" {|(2:id7:aseggdb4:lang2:nl5:title13:Hello, world!6:author6://uhu@9:published25:2023-02-14T01:23:45+01:007:updated25:2023-02-14T01:23:45+01:005:links((4:href32:https://nlnet.nl/projects/Seppo/))10:categories()7:content11:a new Note.)|};
   Ok n)
  |> tail

(**
 * inspired by https://code.mro.name/mro/ShaarliGo/src/cb798ebfae17431732e37a94ee80b29bd3b78911/atom.go#L302
 * https://opam.ocaml.org/packages/base32/
 * https://opam.ocaml.org/packages/base64/
*)
let test_id_make () =
  Logr.info (fun m -> m "rfc4287_test.test_id_make");
  let assrt l id iso =
    match iso |> Ptime.of_rfc3339 with
    | Ok (t,_,_) ->
      let f = Entry.id_make t |> Uri.to_string in
      Assert2.equals_string l id f
    | _ -> "" |> Assert2.equals_string "" "-"
  in
  "1970-01-01T00:00:00+00:00" |> assrt "rfc4287_test.test_id_make 0" "2222222";
  "2023-01-01T00:00:00+00:00" |> assrt "rfc4287_test.test_id_make 1" "as35e22";
  "2081-01-01T00:00:00+00:00" |> assrt "rfc4287_test.test_id_make 2" "s9y3s22";
  "2120-01-01T00:00:00+00:00" |> assrt "rfc4287_test.test_id_make 4" "2sd6e22";
  assert true

let test_entry_atom_signals () =
  Logr.info (fun m -> m "rfc4287_test.test_entry_atom_signals");
  let base = Uri.of_string "https://example.com/sub/" in
  let published = Rfc3339 "2023-02-14T01:23:45+01:00" in
  let author = Uri.make ~userinfo:"uhu" () in
  let lang = Rfc4646 "nl" in
  let uri = Uri.of_string "https://nlnet.nl/projects/Seppo/" in
  let e0 = Entry.from_text_plain ~published ~author ~lang ~uri "Hello, world!" "a new Note."
           |> Result.get_ok  in
  let buf = Buffer.create 1024 in
  let o = Xmlm.make_output ~decl:false (`Buffer buf) in
  Xmlm.output o (`Dtd None);
  Entry.to_atom_signals ~base e0
  |> List.iter (Xmlm.output o);
  buf |> Buffer.to_bytes |> Bytes.to_string
  |> Assert2.equals_string "rfc4287_test.test_entry_atom_signals 10" {|
  <entry xmlns="http://www.w3.org/2005/Atom" xml:lang="nl">
    <id>https://example.com/sub/aseggdb</id>
    <title type="text">Hello, world!</title>
    <updated>2023-02-14T01:23:45+01:00</updated>
    <published>2023-02-14T01:23:45+01:00</published>
    <author><name>@uhu@example.com</name></author>
    <link rel="self" href="https://example.com/sub/aseggdb"/>
    <link href="https://nlnet.nl/projects/Seppo/"/>
    <content type="text">a new Note.</content>
  </entry>|}

let test_feed_atom_signals () =
  Logr.info (fun m -> m "rfc4287_test.test_feed_atom_signals");
  let unn,f = "o/p-12/index.xml" |> Storage.unnumbered in
  unn |> Assert2.equals_string "test_feed_atom_signals 15" "o/p/";
  f |> Assert2.equals_string "test_feed_atom_signals 16" "index.xml";
  unn |> Storage.num_ix (Some 11) |> Uri.to_string |>  Assert2.equals_string "test_feed_atom_signals 17" "o/p-11/";
  let unn,f = "o/p-12/" |> Storage.unnumbered in
  unn |> Assert2.equals_string "test_feed_atom_signals 12" "o/p/";
  f |> Assert2.equals_string "test_feed_atom_signals 13" "";
  unn |> Storage.num_ix (Some 11) |> Uri.to_string |>  Assert2.equals_string "test_feed_atom_signals 14" "o/p-11/";

  let published = Rfc3339 "2023-02-14T01:23:45+01:00" in
  let author = Uri.make ~userinfo:"uhu" () in
  let lang = Rfc4646 "nl" in
  let uri = Uri.of_string "https://nlnet.nl/projects/Seppo/" in
  let e0 = Entry.from_text_plain ~published ~author ~lang ~uri "Hello, world!" "a new Note."
           |> Result.get_ok  in
  let buf = Buffer.create 1024 in
  let o = Xmlm.make_output ~decl:false (`Buffer buf) in
  Xmlm.output o (`Dtd None);
  Feed.to_atom_signals
    ~author:(Uri.make ~userinfo:"sepp" ())
    ~base:(Uri.make ~scheme:"https" ~host:"example.com" ~path:"/sub/" ())
    ~lang:(Rfc4646 "nl")
    ~self:(Uri.make ~path:"o/p-11/" ())
    ~prev:(Some (Uri.make ~path:"o/p-10/" ()))
    ~next:None
    ~first:(Uri.make ~path:"o/p/" ())
    ~last:(Uri.make ~path:"o/p-0/" ())
    ~title:"My fancy #Seppo!"
    ~updated:(Rfc3339 "2023-02-27T12:34:56+01:00")
    [e0]
  |> List.iter (Xmlm.output o);
  buf |> Buffer.to_bytes |> Bytes.to_string
  |> Assert2.equals_string "rfc4287_test.test_feed_atom_signals 20" {|<feed xmlns="http://www.w3.org/2005/Atom" xml:lang="nl" xml:base="https://example.com/sub/">
  <title>My fancy #Seppo!</title>
  <id>https://example.com/sub/o/p-11/</id>
  <updated>2023-02-27T12:34:56+01:00</updated>
  <generator uri="Seppo.Social">Seppo - Personal Social Web</generator>
  <author><name>@sepp@example.com</name></author>
  <link rel="self" href="o/p-11/"/>
  <link title="fi" rel="first" href="o/p/"/>
  <link title="1" rel="last" href="o/p-0/"/>
  <link rel="previous" href="o/p-10/"/>
  <entry xmlns="http://www.w3.org/2005/Atom" xml:lang="nl">
    <id>https://example.com/sub/aseggdb</id>
    <title type="text">Hello, world!</title>
    <updated>2023-02-14T01:23:45+01:00</updated>
    <published>2023-02-14T01:23:45+01:00</published>
    <author><name>@uhu@example.com</name></author>
    <link rel="self" href="https://example.com/sub/aseggdb"/>
    <link href="https://nlnet.nl/projects/Seppo/"/>
    <content type="text">a new Note.</content>
  </entry>
</feed>|}

let () =
  Unix.chdir "../../../test/";
  test_encode ();
  test_from_plain_text ();
  test_id_make ();
  test_entry_atom_signals ();
  test_feed_atom_signals ();
  assert true
