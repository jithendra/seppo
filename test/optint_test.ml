
let hex ppf x = x
                |> Optint.to_int
                |> Format.fprintf ppf "%x"

let test_hex () =
  Optint.of_string "1234567"
  |> Format.asprintf "0x%a" hex
  |> Assert2.equals_string "optint test 10" "0x12d687"

let () =
  Unix.chdir "../../../test/";
  test_hex ();
  assert true
