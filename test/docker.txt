
# Install docker

Follow https://heise.de/-4309355

macOS 

https://www.docker.com/products/docker-desktop

Linux

$ curl -fsSL https://get.docker.com -o get-docker.sh
$ less get-docker.sh
$ sudo sh get-docker.sh
$ sudo usermod -aG docker $USER


# OCaml

https://hub.docker.com/r/ocamlpro/ocaml

# create an account at https://hub.docker.com
$ docker pull ocaml/opam
$ docker images
$ docker container create --name seppo --tty --interactive ocaml/opam
