
open Seppo_lib

(* https://erratique.ch/software/xmlm/doc/Xmlm/index.html#outns *)
let test_ezxmlm_out () =
  Logr.info (fun m -> m "%s.%s" "xmlm_test" "text_ezxmlm_out");
  []
  |> Ezxmlm.to_string
  |> Assert2.equals_string "test_write empty" "";
  [ `El ((("", "uhu"), []), []) ]
  |> Ezxmlm.to_string
  |> Assert2.equals_string "test_write 1" "<uhu/>";
  [ `El ((("", "e"), [ (("", "a"), "v") ]), [ `Data "foo" ]) ]
  |> Ezxmlm.to_string
  |> Assert2.equals_string "test_write 2" "<e a=\"v\">foo</e>";
  [ `El ((("", "e"), [ (("", "xmlns"), "A") ]), [ `Data "foo" ]) ]
  |> Ezxmlm.to_string
  |> Assert2.equals_string "test_write 3" "<e xmlns=\"A\">foo</e>";
  [ `El ((("A", "e"), [((Xmlm.ns_xmlns, "xmlns"), "A")]), [`Data "foo"]) ]
  |> Ezxmlm.to_string
  |> Assert2.equals_string "test_write 4" "<e xmlns=\"A\">foo</e>";
  [ `El ((("N", "e"), [((Xmlm.ns_xmlns, "n"), "N")]), [`Data "foo"]) ]
  |> Ezxmlm.to_string
  |> Assert2.equals_string "test_write 5" "<n:e xmlns:n=\"N\">foo</n:e>";
  assert true

let test_ezxmlm_out2() =
  Logr.info (fun m -> m "%s.%s" "xmlm_test" "text_ezxmlm_out2");
  [`El ((("","elm"),[(("","nam"),"val")]),[])]
  |> Ezxmlm.to_string
  |> Assert2.equals_string "xmlm_test:test_xml_out2 10" {|<elm nam="val"/>|};
  [`El ((("uri 2","root"),[
       ((Xmlm.ns_xmlns,"n2"),"uri 2");
     ]),[
          `El ((("uri 2","c1"),[
            ]),[
                 `Data "uh";
                 `Data "u";
               ])]);
  ]
  |> Ezxmlm.to_string ~decl:true
  |> Assert2.equals_string "xmlm_test:test_xml_out2 20" {|<?xml version="1.0" encoding="UTF-8"?>
<n2:root xmlns:n2="uri 2"><n2:c1>uhu</n2:c1></n2:root>|};
  assert true

let test_xmlm_signals () =
  Logr.info (fun m -> m "%s.%s" "xmlm_test" "test_xmlm_signals");
  let dst = Buffer.create 1024 in
  Buffer.add_string dst {|<?xml version="1.0" encoding="UTF-8"?>
|};
  Buffer.add_string dst "<!-- huhu -->\n";
  let o = Xmlm.make_output ~decl:false ~indent:(Some 2) (`Buffer dst) in
  let out = Xmlm.output o in
  out (`Dtd None);
  out (`El_start (("uri","e"),[
      ((Xmlm.ns_xmlns,"n"),"uri");
      ("","k"),"v"]));
  out (`Data "uhu");
  out `El_end;
  dst
  |> Buffer.to_bytes
  |> Bytes.to_string
  |> Assert2.equals_string "test_xmlm_signals 10" {|<?xml version="1.0" encoding="UTF-8"?>
<!-- huhu -->
<n:e xmlns:n="uri" k="v">
  uhu
</n:e>|};
  assert true

let () =
  Unix.chdir "../../../test/";
  test_ezxmlm_out ();
  test_ezxmlm_out2 ();
  test_xmlm_signals ();
  assert true
