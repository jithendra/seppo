(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Web.
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

let ( let* ) = Result.bind
let ( let+ ) = Result.map

let lwt_err e = Lwt.return (Error e)

(* may go to where PubKeyPem is: As2 *)
let post_signed
    ?(date = Ptime_clock.now ())
    ?(headers = [ Http.H.ct_json; Http.H.acc_act_json ])
    ~sndr
    ~pk
    body
    uri =
  let he_sig = As2.PubKeyPem.(Http.signed_headers (sign pk) sndr date (digest body) uri) in
  let headers = Cohttp.Header.add_list he_sig headers in
  Http.post ~headers (`String body) uri

(* https://www.w3.org/TR/activitypub/#delivery *)
let notify_remote_actor ~sndr ~pk (msg_id, ibox, wefi, body) =
  Logr.debug (fun m -> m "%s.%s %s / %s %s" "Job" "notify_remote_actor" ibox wefi msg_id);
  let ibox = ibox |> Uri.of_string in
  let error = lwt_err in

  let ok (_resp, body) =
    let%lwt b = body |> Cohttp_lwt.Body.to_string in
    b
    |> As2.examine_response
    |> Lwt.return
    (*Lwt.return (Ok msg_id *) in
  let attempt2 ibox =
    let%lwt r = post_signed ~pk ~sndr body ibox in
    Result.fold ~ok ~error r
  in
  (* have the webfinger handle as fallback option *)
  let attempt1 ibox wefi =
    let ok v = Lwt.return (Ok v) in
    let error _ =
      (* get the ibox from the handle *)
      let%lwt q = As2.Webfinger.Client.query wefi in
      let ok (q : As2_vocab.Types.Webfinger.query_result) =
        match q.links |> As2_vocab.Types.Webfinger.self_link with
        | None -> Lwt.return (Error "no self found in webfinger query result")
        | Some ap ->
          let%lwt ap = ap |> As2.Profile.http_get_remote_actor in
          let ok (ap : As2_vocab.Types.person) = attempt2 ap.inbox in
          Result.fold ~ok ~error ap in
      Result.fold ~ok ~error q in
    let%lwt r = post_signed ~pk ~sndr body ibox in
    Result.fold ~ok ~error r
  in
  attempt1 ibox wefi

(* must correspond to dispatch_job *)
let job_encode_notify msg_id (ibox, wefi) json =
  Csexp.(List [Atom "notify"; List [Atom msg_id; List [Atom ibox; Atom wefi]; Atom json]])

let dispatch_job ~base ~pk j payload =
  let sndr = As2.(base ^/ apub) in
  let open Csexp in
  match payload with
  (* must correspond to job_encode_notify *)
  | List [Atom "notify"; List [Atom msg_id; List [Atom ibox; Atom wefi]; Atom body]] ->
    notify_remote_actor ~sndr ~pk (msg_id, ibox, wefi, body)
  |  _ ->
    Logr.err (fun m -> m "%s.%s invalid job format %s" "Main" "dispatch_job" j);
    Error "invalid job format" |> Lwt.return

let process_queue
    ?(due = Ptime_clock.now ())
    ?(wait = Job.wait)
    ?(new_ = Job.new_)
    ?(run = Job.run)
    ?(cur = Job.cur)
    ~base
    ~pk
    que =
  let _ = run
  and _ = cur in
  (* move those due from wait into new *)
  let rec move_due_wait_new ~wait ~new_ ~due  =
    match Job.(peek_opt_any_due ~due ~wait que) with
    | None -> ()
    | Some j ->
      Job.(move que j wait new_);
      move_due_wait_new ~wait ~new_ ~due
  in
  let Queue que' = que in
  (* TODO pull out Lwt_main.run *)
  let rec loop () =
    match Job.peek_opt new_ que with
    | None -> ()
    | Some j ->
      (let open Job in
       move que j new_ run;
       let fn = que' ^ run ^ j
       and error s =
         wait_or_err ~wait que run j;
         Logr.info (fun m -> m "job postponed/cancelled: %s reason: %s" j s)
       and ok _p =
         move que j run cur;
         Logr.info (fun m -> m "job done: %s" j)   in
       let ok p =
         dispatch_job ~base ~pk j p
         |> Lwt_main.run
         |> Result.fold ~ok ~error in
       File.in_channel fn Csexp.input
       |> Result.fold ~ok ~error);
      loop ()
  in
  move_due_wait_new ~wait ~new_ ~due;
  loop ();
  Ok que

let sift_urls e =
  Logr.debug (fun m -> m "%s.%s not implemented yet." "Main" "sift_urls");
  Ok e

(* Extract tags from a post into a list.
 *
 * Needs the post and a tag store. Modifies both.
*)
let sift_tags cdb (e : Rfc4287.Entry.t) =
  Logr.debug (fun m -> m "%s.%s" "Main" "sift_tags");
  let open Rfc4287 in
  let c2t init ((Label (Single l),_,_) : Rfc4287.Category.t)  =
    (Tag.Tag ("#" ^ l)) :: init
  in
  let t2c init (Tag.Tag t) =
    let le = t |> String.length in
    assert (1 < le);
    assert ('#' == t.[0]);
    let t = String.sub t 1 (le-1) in
    let t = Single t in
    let l = Category.Label t in
    let te = Category.Term t in
    (l, te, Rfc4287.tagu) :: init
  in
  let ti = e.title in
  let co = e.content in
  let tl = e.categories |> List.fold_left c2t [] in
  let ti,co,tl = Tag.cdb_normalise ti co tl cdb in
  Ok {e with
      title = ti;
      content = co;
      categories = tl |> List.fold_left t2c []}

let sift_handles e =
  Logr.debug (fun m -> m "%s.%s not implemented yet." "Main" "sift_handles");
  Ok e

let publish_note ~base ~(profile : Cfg.Profile.t) ~author (n : Rfc4287.Entry.t) =
  Logr.debug (fun m -> m "%s.%s" "Main" "publish_note");
  (* add to storage and indices (main,date,tags)) *)
  let items_per_page = 50 in
  let n,ix,pos = n |> Storage.save in
  let append_to_ix pos init ix = (ix |> Storage.Atom.append_to_ix pos) :: init in
  let ix_other = n
                 |> Storage.other_ix_files ~items_per_page
                 |> List.fold_left (append_to_ix pos) [] in
  (* refresh feeds, outbox etc. *)
  let lang = profile.language in
  let title = profile.title in
  let updated = n.updated (* more precisely would be: now *) in
  let mater init ix = (ix |> Storage.materialise ~base ~title ~updated ~lang ~author) :: init in
  let _ = ix :: ix_other
          |> List.fold_left mater [] in
  Ok n

(** Enqueue jobs.
 *
 * https://www.w3.org/TR/activitypub/#delivery says "Servers MUST de-duplicate
 * the final recipient list." which implies each actor profile / inbox lookup
 * can lag delivery for all.
 *
 * How long could such a consolidated inbox list be cached? In theory not at
 * all because each inbox target url may change without further notice.
 *
 * In pratice, we will use the inbox as long as it works and redo the
 * webfinger/actor lookup otherwise.
 *
 * 1. get all actor profiles (limit redirects) and extract inbox url
 * 2. de-duplicate
 * 3. deliver to all
 * 4. retry temporary failures
 * 5. handle permanent failures to clean link rot
*)
let notify_followers
    ?(due = Ptime_clock.now ())
    ?(que = Job.qn)
    ~base
    follower_fold
    (n : Rfc4287.Entry.t) =
  Logr.debug (fun m -> m "%s.%s" "Main" "notify_followers");
  let msg_id = n.id |> Uri.to_string in
  let json = n
             |> As2.Note.of_rfc4287
             |> As2.Note.mk_create
             |> As2_vocab.Encode.create ~base ~context:true
               (As2_vocab.Encode.note ~base)
             |> Ezjsonm.value_to_string
  in
  let buf = Buffer.create 0x400 in
  let fkt_enqueue init (wefi,ibox) =
    let wefi = wefi |> Bytes.to_string in
    let ibox = ibox |> Bytes.to_string in
    Logr.debug (fun m -> m "%s.%s %s -> %s" "Main" "notify_followers" wefi ibox);
    Buffer.clear buf;
    job_encode_notify msg_id (ibox, wefi) json |> Csexp.to_buffer buf;
    let _ = buf
            |> Buffer.to_bytes
            |> Job.enqueue ~due que 0 in
    init in
  Logr.debug (fun m -> m "enqueue a job for each follower");
  let _ = follower_fold fkt_enqueue in
  Ok n
