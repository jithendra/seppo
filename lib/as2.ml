(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Web.
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

(*
 * https://www.w3.org/TR/activitystreams-core/
 * https://www.w3.org/TR/activitystreams-core/#media-type
 *)

let cgi' = "seppo.cgi"
let apub = "activitypub/"
let proj = apub ^ "profile.json"
let prox = apub ^ "profile.xml"
let ( ^/ ) a b =
  let p = Uri.path a in
  let p' = p ^ b in
  Uri.with_path a p'
let ( let* ) = Result.bind
let ( >>= ) = Result.bind
let to_result none = Option.to_result ~none
let chain a b =
  let f a = Ok (a, b) in
  Result.bind a f

let writex oc (x : Ezxmlm.nodes) =
  Ezxmlm.to_channel oc ~decl:true None x;
  Ok ""

let write oc j =
  let minify = false in
  Ezjsonm.to_channel ~minify oc j;
  Ok ""

let writev oc j =
  let minify = false in
  Ezjsonm.value_to_channel ~minify oc j;
  Ok ""

let json_from_file fn =
  let ic = open_in_gen  [ Open_rdonly; Open_binary ] 0 fn in
  let j = Ezjsonm.value_from_channel ic in
  close_in ic;
  Ok j

let examine_response j =
  let ok = function
    | `O ["error", `String e] -> Error e
    | _ ->
      Logr.warn (fun m -> m "unknown response: %s" j);
      Error ("unknown response: " ^ j) in
  let error = function
    | `Error _ as e ->
      let e = e |> Ezjsonm.read_error_description in
      Logr.warn (fun m -> m "json parsing error: '%s' in '%s'" e j);
      Error ("json parsing error '" ^ e ^ "' in '" ^ j ^ "'")
    | `Unexpected _ ->
      Logr.warn (fun m -> m "unexpected json: '%s'" j);
      Error ("unexpected json '" ^ j ^ "'")
    | `End_of_input -> Error "end of input"
  in
  j
  |> Ezjsonm.value_from_string_result
  |> Result.fold ~ok ~error

let ns_as  = As2_vocab.Constants.ActivityStreams.ns_as
let ns_sec = As2_vocab.Constants.ActivityStreams.ns_sec
let public = As2_vocab.Constants.ActivityStreams.public

module Note = struct
  let actor_from_author _author =
    Uri.make ~path:apub  ()

  let followers actor =
    Uri.make ~path:"followers/" () |> Http.reso ~base:actor

  let of_rfc4287
      ?(to_ = [As2_vocab.Constants.ActivityStreams.public])
      (e : Rfc4287.Entry.t)
    : As2_vocab.Types.note =
    Logr.debug (fun m -> m "%a" Uri.pp e.id);
    let tag init (lbl,term,base) =
      let ty = `Hashtag in
      let open Rfc4287.Category in
      let (Label (Single name)) = lbl
      and (Term (Single term)) = term in
      let path = term ^ "/" in
      let href = Uri.make ~path () |> Http.reso ~base  in
      ({ty; name; href} : As2_vocab.Types.tag) :: init
    in
    let id = e.id in
    let actor = actor_from_author e.author in
    let summary = Some e.title in
    let Rfc4287.Rfc3339 published = e.published in
    let published = match published |> Ptime.of_rfc3339 with
      | Ok (t,_,_) -> Some t
      | _ -> None  in
    let cc = [followers actor] in
    (* let Rfc4287.Rfc3066 lang = e.lang  in *)
    let tags = e.categories |> List.fold_left tag [] in
    let content = e.content in
    {
      id;
      actor;
      attachment=[];
      cc;
      content;
      in_reply_to=None;
      media_type=(Some Http.Mime.text_plain);
      published;
      sensitive=false;
      source=None;
      summary;
      tags;
      to_;
    }

  let mk_create (obj : As2_vocab.Types.note) : _ As2_vocab.Types.create =
    let frag = match obj.id |> Uri.fragment with
      | None -> Some "Create"
      | Some f -> Some (f ^ "/Create") in
    {
      id             = frag |> Uri.with_fragment obj.id;
      actor          = obj.actor;
      published      = obj.published;
      to_            = obj.to_;
      cc             = obj.cc;
      direct_message = false;
      obj            = obj;
    }
end

module PubKeyPem = struct

  let pub_from_pem s =
    match s
          |> Cstruct.of_string
          |> X509.Public_key.decode_pem with
    | Ok (`RSA _) as k -> k
    | _ -> Error "public key is fishy"

  let check (`RSA k) =
    Logr.warn (fun m -> m "@TODO PubKeyPem.check." );
    Ok (`RSA k)

  let target = apub ^ "id_rsa.pub.pem"
  let pk_pem = "app/etc/id_rsa.priv.pem"
  let key_pems = (pk_pem, target) (* order matters, pk first, pub second *)

  let pk_rule : Make.t = {
    target = pk_pem;
    srcs =  [];
    fresh = Make.Missing;
    build = fun _pre _ oc _ ->
      Logr.debug (fun m -> m "create private key pem." );
      (* https://discuss.ocaml.org/t/tls-signature-with-opam-tls/9399/3?u=mro
       * $ openssl genrsa -out app/etc/id_rsa.priv.pem 2048
      *)
      (* really required? *)
      let module PSS_SHA512 = Mirage_crypto_pk.Rsa.PSS (Mirage_crypto.Hash.SHA512) in
      try
        (* https://github.com/mirage/ocaml-dns/blob/be6c384e9a6fc1de9b8fd3d055371e88a1465a87/app/ocertify.ml#L19 *)
        let key = Mirage_crypto_pk.Rsa.generate ~bits:2048 () in
        (* https://github.com/hannesm/conex/blob/24547cd746915431c72ee7e929d39208f65f4100/mirage-crypto/conex_mirage_crypto.ml#L74 *)
        X509.Private_key.encode_pem (`RSA key)
        |> Cstruct.to_bytes
        |> output_bytes oc;
        Ok "rsa pk created"
      with _ ->
        Logr.err (fun m -> m "couldn't create pk");
        Error "couldn't create pk"
  }

  let rule : Make.t = {
    target;
    srcs = [ pk_pem ];
    fresh = Make.Outdated;
    build = fun _pre _ oc deps ->
      Logr.debug (fun m -> m "create public key pem." );
      match deps with
      | [ fn_priv ] -> (
          assert (fn_priv = pk_pem);
          match
            fn_priv
            |> File.to_string
            |> Cstruct.of_string
            |> X509.Private_key.decode_pem
          with
          | Ok (`RSA key) ->
                    (*
                    https://github.com/hannesm/conex/blob/24547cd746915431c72ee7e929d39208f65f4100/mirage-crypto/conex_mirage_crypto.ml#L77
                    *)
            let pub = Mirage_crypto_pk.Rsa.pub_of_priv key in
            X509.Public_key.encode_pem (`RSA pub)
            |> Cstruct.to_string
            |> output_string oc;
            Ok ""
          | Ok _
          | Error _ ->
            Error "error, proper message not implemented yet,")
      | l ->
        Error
          (Printf.sprintf
             "rule must have exactly one dependency, not %d"
             (List.length l))
  }

  let rulez = [ pk_rule; rule ]

  let make pre =
    Make.make pre rulez target

  let pk_from_pem (fn_priv, fn_pub) =
    (* https://mirleft.github.io/ocaml-tls/doc/tls/X509_lwt/#val-private_of_pems *)
    X509_lwt.private_of_pems ~cert:fn_pub ~priv_key:fn_priv

  let sign pk data =
    Logr.debug (fun m -> m "PubKeyPem.sign");
    (*
     * https://discuss.ocaml.org/t/tls-signature-with-opam-tls/9399/9?u=mro
     * https://mirleft.github.io/ocaml-x509/doc/x509/X509/Private_key/#cryptographic-sign-operation
     *)
    X509.Private_key.sign `SHA256 ~scheme:`RSA_PKCS1 pk (`Message (Cstruct.of_string data))
    |> Result.get_ok
    |> Cstruct.to_string
    |> Base64.encode_exn

  let verify _pk _sig str = Ok str

  (* not key related *)
  let digest s =
    Logr.debug (fun m -> m "PubKeyPem.digest");
    "SHA-256=" ^ (s
                  |> Cstruct.of_string
                  |> Mirage_crypto.Hash.SHA256.digest
                  |> Cstruct.to_string
                  |> Base64.encode_exn)
end

module Followers = struct

  let target = apub ^ "followers/index.json"

  let js_gen kind (actr : Uri.t) : 'a As2_vocab.Types.collection =
    let self = actr ^/ kind in
    {
      id         = self;
      current    = None;
      first      = None;
      is_ordered = true;
      items      = [];
      last       = None;
      total_items= None;
    }

  let jsonm base = js_gen "followers" base
                   |> As2_vocab.Encode.(collection ~base (note ~base))
                   |> Result.ok

  let rule : Make.t = {
    target;
    srcs = [ Cfg.Base.fn ];
    fresh = Make.Outdated;
    build = fun _pre _ oc _newer ->
      Cfg.Base.(from_file fn)
      >>= jsonm
      >>= writev oc
  }

  let rulez = [ rule ]
end

module Following = struct
  let target = apub ^ "following/index.json"

  let jsonm base = Followers.js_gen "following" base
                   |> As2_vocab.Encode.(collection ~base (note ~base))
                   |> Result.ok

  let rule : Make.t = {
    target;
    srcs = [ Cfg.Base.fn ];
    fresh = Make.Outdated;
    build = fun _pre _ oc _newer ->
      Cfg.Base.(from_file fn)
      >>= jsonm
      >>= writev oc
  }

  let rulez = [ rule ]
end

module Liked = struct
  let target = apub ^ "liked/index.json"

  let jsonm base = Followers.js_gen "liked" base
                   |> As2_vocab.Encode.(collection ~base (note ~base))
                   |> Result.ok

  let rule : Make.t = {
    target;
    srcs = [ Cfg.Base.fn ];
    fresh = Make.Outdated;
    build = fun _pre _ oc _newer ->
      Cfg.Base.(from_file fn)
      >>= jsonm
      >>= writev oc
  }

  let rulez = [ rule ]
end

(* A person actor object. https://www.w3.org/TR/activitypub/#actor-objects *)
module Person = struct
  let of_jsonm j' =
    match As2_vocab.Decode.person j' with
    | Error _   -> Error "@TODO aua json"
    | Ok _ as v -> v

  let prsn _pubdate (pem, ((pro : Cfg.Profile.t), (Auth.Uid uid, base))) : As2_vocab.Types.person =
    {
      id                         = base;
      inbox                      = Uri.make ~path:("../" ^ cgi' ^ "/" ^ apub ^ "inbox/") ();
      outbox                     = Uri.make ~path:"outbox/" ();
      followers                  = Some (Uri.make ~path:"followers/" ());
      following                  = Some (Uri.make ~path:"following/" ());
      attachment                 = [
        { name  = "Support";
          value = "https://Seppo.Social/support"; };
        { name  = "Generator";
          value = "https://Seppo.Social"; };
        { name  = "Funding";
          value = "https://nlnet.nl/project/Seppo/"; };
      ];
      discoverable               = false;
      icon                       = Some (Uri.make ~path:"../me-avatar.jpg" ());
      image                      = Some (Uri.make ~path:"../me-banner.jpg" ());
      manually_approves_followers= false;
      name                       = Some pro.title;
      preferred_username         = Some uid;
      public_key                 = {
        id    = Uri.make ~fragment:"main-key" ();
        owner = Uri.empty;
        pem;
        signatureAlgorithm = Some "https://www.w3.org/2001/04/xmldsig-more#rsa-sha256"; (* from hubzilla, e.g. https://im.allmendenetz.de/channel/minetest *)
      };
      summary                    = Some pro.bio;
      url                        = Some Uri.empty;
    }

  let json _pubdate (pem, (pro, (uid, base))) =
    let base = Http.reso ~base (Uri.make ~path:proj ()) in
    prsn _pubdate (pem, (pro, (uid, base)))
    |> As2_vocab.Encode.person ~base
    |> Result.ok

  let target = proj

  let rule : Make.t =
    {
      target;
      srcs = [
        Auth.fn;
        Cfg.Profile.target;
        PubKeyPem.target;
        Followers.target;
        Following.target;
        Liked.target;
      ];
      fresh = Make.Outdated;
      build = fun pre _ oc _newer ->
        let now = Ptime_clock.now () in
        Cfg.Base.(make pre >>= from_file)
        >>= chain Auth.(make pre >>= uid_from_file)
        >>= chain Cfg.Profile.(make pre >>= from_file)
        >>= chain (PubKeyPem.make pre >>= File.cat)
        >>= json now
        >>= writev oc
    }

  let rulez =
    PubKeyPem.rulez
    @ Followers.rulez
    @ Following.rulez
    @ Liked.rulez
    @ [ rule ]

  let make pre = Make.make pre rulez target

  let from_file fn =
    fn
    |> json_from_file
    >>= of_jsonm
end

module PersonX = struct
  let xml ~base (p : As2_vocab.Types.person) =
    let ns_as     = "https://www.w3.org/ns/activitystreams#"
    and ns_ldp    = "http://www.w3.org/ns/ldp#"
    and ns_rdf    = "http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    and ns_schema = "http://schema.org#"
    and ns_sec    = "https://w3id.org/security#"
    and ns_xml    = Xmlm.ns_xml
    and lang      = "nl" in
    [`El (((ns_rdf,"RDF"),[
         ((Xmlm.ns_xmlns,"as"),ns_as);
         ((Xmlm.ns_xmlns,"ldp"),ns_ldp);
         ((Xmlm.ns_xmlns,"rdf"),ns_rdf);
         ((Xmlm.ns_xmlns,"schema"),ns_schema);
         ((Xmlm.ns_xmlns,"sec"),ns_sec);
       ]),
          [`El (((ns_as,"Person"),[ 
               ((ns_xml,"lang"), lang);
               ((ns_rdf,"about"),base |> Uri.to_string); ]),[
                  (* `Data "<!-- this is just a subset of profile.json -->"; *)
                  `El (((ns_as,"attachment"),[]),[
                      `El (((ns_schema,"PropertyValue"),[]),[
                          `El (((ns_as,"name"),[]),[ `Data "Support" ]);
                          `El (((ns_schema,"value"),[]),[ `Data "https://Seppo.Social/support" ]);
                        ])];
                    );
                  `El (((ns_as,"attachment"),[]),[
                      `El (((ns_schema,"PropertyValue"),[]),[
                          `El (((ns_as,"name"),[]),[ `Data "Funding" ]);
                          `El (((ns_schema,"value"),[]),[ `Data "https://nlnet.nl/project/Seppo/" ]);
                        ])];
                    );
                  `El (((ns_as,"attachment"),[]),[
                      `El (((ns_schema,"PropertyValue"),[]),[
                          `El (((ns_as,"name"),[]),[ `Data "Generator" ]);
                          `El (((ns_schema,"value"),[]),[ `Data "https://Seppo.Social" ]);
                        ])];
                    );
                  `El (((ns_as,"icon"),[]),[
                      `El (((ns_as,"Image"),[]),[
                          `El (((ns_as,"url"),[ ((ns_rdf,"resource"),p.icon |> Option.value ~default:Uri.empty |> Uri.to_string) ]),[]);
                        ])];
                    );
                  `El (((ns_as,"image"),[]),[
                      `El (((ns_as,"Image"),[]),[
                          `El (((ns_as,"url"),[ ((ns_rdf,"resource"),p.image |> Option.value ~default:Uri.empty |> Uri.to_string) ]),[]);
                        ])];
                    );
                  `El (((ns_as,"name"),[]),[ `Data (p.name|> Option.value ~default:"-") ]);
                  `El (((ns_as,"summary"),[]),[ `Data (p.summary |> Option.value ~default:"-") ]);
                ])]
       )]

  let xml' _pubdate (pem, (pro, (uid, base))) =
    let base = Http.reso ~base (Uri.make ~path:prox ()) in
    Person.prsn _pubdate (pem, (pro, (uid, base)))
    |> xml ~base
    |> Result.ok

  let target = prox

  let rule = {Person.rule with target;
                               srcs = [ Person.target ];
                               build = fun pre _ oc _newer ->
                                 let now = Ptime_clock.now () in
                                 Cfg.Base.(make pre >>= from_file)
                                 >>= chain Auth.(make pre >>= uid_from_file)
                                 >>= chain Cfg.Profile.(make pre >>= from_file)
                                 >>= chain (PubKeyPem.make pre >>= File.cat)
                                 >>= xml' now
                                 >>= writex oc }

  let rulez = Person.rulez @ [ rule ]

  let make pre = Make.make pre rulez target
end

module Profile = struct
  let http_get_remote_actor u =
    let mape (_ : Ezjsonm.value Decoders__Error.t) =
      Logr.err (fun m -> m "failed to decipher %a\n\n\
                            Please see https://Seppo.Social/S1002\n\
                            what to do about it.\n"
                   Uri.pp u);
      "actor decode failed" in
    let deco j = j |> As2_vocab.Decode.person |> Result.map_error mape in
    let%lwt p = u |> Http.get_jsonv Result.ok in
    p >>= deco |> Lwt.return

  let http_get_remote_actor' u =
    http_get_remote_actor u
    |> Lwt_main.run

  let pubkey_pem (p : As2_vocab.Types.person) = Ok p.public_key.pem
end

module Activity = struct

  type t = T of string

  let ty s = match s |> String.lowercase_ascii with
    | "like"    -> Ok (T "Like")
    | "dislike" -> Ok (T "Dislike")
    | _         -> Error ("Activity '" ^ s ^ "' not supported.")

  let make_like me _act _pubdate objec _remote_actor: As2_vocab.Types.like =
    {
      id       = Uri.empty;
      actor    = me;
      obj      = objec;
      published= None;
      (* to cc *)
    }

  let digest = PubKeyPem.digest

  (** e.g. https://tube.network.europa.eu/w/aTx3DYwH1km2gTEn9gKpah
   *
   * $ curl -H 'accept: application/activity+json' 'https://tube.network.europa.eu/w/aTx3DYwH1km2gTEn9gKpah'
   * $ curl -H 'accept: application/activity+json' 'https://tube.network.europa.eu/accounts/edps'
  *)
  let like' (act_type : t) post_uri (me : As2_vocab.Types.person) =
    let base = Uri.empty in
    let open Cohttp in
    let open Cohttp_lwt in
    (* we need the sender and recipient actor profiles *)
    (* https://github.com/roburio/http-lwt-client/blob/main/src/http_lwt_client.ml *)
    let post_attributed_to json =
      let extract3tries k0 k1 j =
        match Ezjsonm.find j [ k0 ] with
        | `String s -> Some s
        | `A (`String s :: _) -> Some s
        | `A ((`O _ as hd) :: _) -> (
            (* ignore 'type' *)
            match Ezjsonm.find hd [ k1 ] with
            | `String s -> Some s
            | _ -> None)
        | _ -> None
      in
      json
      |> extract3tries "attributedTo" "id"
      |> to_result (* TODO examine the http response code? *) "attribution not found"
      >>= fun v -> Ok (Uri.of_string v)
    in
    let%lwt p = Http.get_jsonv post_attributed_to post_uri in
    match p with
    | Error _ as e -> Lwt.return e
    | Ok (act_uri : Uri.t) ->
      let%lwt j = Http.get_jsonv Result.ok act_uri in
      match j >>= Person.of_jsonm with
      | Error _ as e -> Lwt.return e
      | Ok pro ->
        let _ = Person.make "" in
        let%lwt _,pk = PubKeyPem.(pk_from_pem key_pems) in
        let date = Ptime_clock.now ()
        and sndr = me.id
        and rcpt = pro.id
        and inbx = pro.inbox in
        let body = make_like sndr act_type date post_uri rcpt
                   |> As2_vocab.Encode.like ~base
                   |> Ezjsonm.value_to_string in
        let headers = Http.signed_headers (PubKeyPem.sign pk) sndr date (digest body) inbx in
        let headers = Http.H.add' headers Http.H.ct_json in
        let headers = Header.add_list headers [ Http.H.acc_act_json ] in
        Logr.info (fun m -> m "-> http POST %a" Uri.pp inbx);
        let%lwt p = Http.post ~headers (`String body) inbx in
        match p with
        | Error _ as e -> Lwt.return e
        | Ok (_resp, body) ->
          let%lwt b = body |> Body.to_string in
          Logr.debug (fun m -> m "%s" b);
          Lwt.return (Ok post_uri)

  let like aty uri act =
    aty |> ty
    >>= fun v -> Ok (like' v uri act)
end

(* https://tools.ietf.org/html/rfc7033
*)
module Webfinger = struct
  module Client = struct
    type l = Localpart of string
    type h = Host of string
    type t = (l * h)

    (* this uri is not the .well-known form *)
    let from_uri u : (t, string) result =
      match (u |> Uri.user), (u |> Uri.host) with
      | Some usr, Some hos ->
        Ok (Localpart usr, Host hos)
      | _ ->
        Error "uri must have user and host, e.g. foo@example.com."

    let from_string s : (t, string) result =
      let l = s |> String.length in
      if l < 4 || '@' != s.[0]
      then Error "not a webfinger handle"
      else
        let u = "webfinger://" ^ String.sub s 1 (l-1) |> Uri.of_string in
        match u |> Uri.user, u |> Uri.host with
        | Some l, Some h -> Ok (Localpart l, Host h)
        | _ -> Error "not a webfinger handle"

    (* this uri is not the .well-known form *)
    let to_uri (Localpart us, Host ho) =
      Uri.make ~scheme:"webfinger" ~userinfo:us ~host:ho ()

    let to_string h =
      let s = h |> to_uri |> Uri.to_string in
      let l = s |> String.length in
      "@" ^ String.sub s 12 (l-12)

    let well_known_uri (Localpart local, Host host) =
      Uri.make
        ~scheme:"https"
        ~host
        ~path:"/.well-known/webfinger"
        ~query:[("resource", ["acct:" ^ local ^ "@" ^ host])]
        ()

    let self_json_uri (x : As2_vocab.Types.Webfinger.query_result) : (Uri.t, string) result =
      x.links
      |> As2_vocab.Types.Webfinger.self_link
      |> to_result "self link not found"

    let http_get_remote_finger (w : Uri.t) =
      let mape (_ : Ezjsonm.value Decoders__Error.t) =
        Logr.err (fun m -> m "failed to decipher %a\n\n\
                              Please see https://Seppo.Social/S1001\n\
                              what to do about it.\n"
                     Uri.pp w);
        "webfinger decode failed" in
      let deco j = j |> As2_vocab.Decode.Webfinger.query_result |> Result.map_error mape in
      let headers = [Http.H.acc_app_json] |> Cohttp.Header.of_list in
      let%lwt p = w |> Http.get_jsonv ~headers Result.ok in
      p >>= deco |> Lwt.return

    let http_get_remote_finger' (w : Uri.t) =
      http_get_remote_finger w
      |> Lwt_main.run

    let query handle =
      match handle |> from_string with
      | Error _ as e -> Lwt.return e
      | Ok handle -> handle |> well_known_uri |> http_get_remote_finger
  end

  module Server = struct
    (* Create a local .well-known/webfinger and link here from the global one (in webroot). *)
    let make u =
      let wkwf =  ".well-known/webfinger/" in
      let wefi u =
        let i = u |> Uri.path |> String.split_on_char '/' |> List.length in
        ((List.init (i-2) (fun _ -> "../") |> String.concat "") ^ wkwf)
      in
      u |> wefi |> File.mkdir_p File.pDir
      >>= fun p ->
      let uid = u |> Uri.user |> Option.value ~default:"-"
      and hos = u |> Uri.host |> Option.value ~default:"-"
      and pat = u |> Uri.path in
      let hta' = wkwf ^ ".htaccess" in
      Logr.debug (fun m -> m "write %s" hta');
      File.out_channel true hta'
        (fun oc ->
           Printf.fprintf oc "# https://Seppo.Social/S1002\n\
                              # automatically linked or manually appended to <webroot>/.well-known/webfinger/.htaccess\n\
                              # created by ../../seppo.cgi\n\
                              RewriteEngine On\n\
                              RewriteCond %%{QUERY_STRING} ^resource=acct:%s@%s$ [nocase]\n\
                              RewriteRule ^$ https://%s%s%sindex.json [qsdiscard,last,redirect=seeother]\n"
             (uid |> Str.quote) (hos |> Str.quote) hos pat wkwf;
        );
      (* evtl. put this into a separate function *)
      let hta = p ^ ".htaccess" in
      Unix.((try
               (* remove symlink (and only symlink!) in case *)
               if S_LNK == (lstat hta).st_kind
               then unlink hta;
             with | Unix_error(ENOENT, "lstat", _) -> ());
            let hta' = Printf.sprintf "../..%s%s" pat hta' in
            try symlink hta' hta;
            with | Unix_error(EEXIST, "symlink", "./.well-known/webfinger/.htaccess") -> ());
      Logr.debug (fun m -> m "$ ln -s %s %s" hta' hta);
      Ok u
  end

  let make (Auth.Uid uid, base) : As2_vocab.Types.Webfinger.query_result =
    let host    = base |> Uri.host |> Option.value ~default:"-" in
    let subject = Printf.sprintf "acct:%s@%s" uid host in
    let self    = Uri.make ~path:proj() |> Http.reso ~base in
    (* let subs    = Uri.make ~path:(cgi' ^ "/ostatus/authorize") ()
                  |> Http.reso ~base in
       let subs    = Uri.with_query' subs ["uri", ""] in
       let subs    = (subs |> Uri.to_string) ^ "{uri}" in *)
    let open As2_vocab.Types.Webfinger in
    let links   = [
      Self             (`ActivityJson, self);
      (* ProfilePage      (`Html, base); *)
      (* Alternate        (`Atom, base); *)
      (* OStatusSubscribe subs; *)
    ] in
    {subject;aliases=[];links}

  let jsonm (uid, base) =
    (uid, base)
    |> make
    |> As2_vocab.Encode.Webfinger.query_result ~base
    |> Result.ok

  (* @TODO for now the according .htaccess is only written by bin/shell.ml do_create *)
  let target = ".well-known/webfinger" ^ "/index.json"

  let srcs = [ Person.target ]
  let rule : Make.t =
    { target;
      srcs;
      fresh = Make.Outdated;
      build = fun pre _ oc _newer ->
        Cfg.Base.(make pre >>= from_file)
        >>= chain Auth.(make pre >>= uid_from_file)
        >>= jsonm
        >>= writev oc;
    }
  let rulez = rule :: Person.rulez
end
