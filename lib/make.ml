(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Web.
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

type remake =
  | Outdated (* remake target if dependencies are more recent *)
  | Missing  (* remake target ONLY if dependencies are all missing *)

(** a single rule *)
type t = {
  target : string; (* sould the target be separate to build List.assoc tuples? *)
  srcs   : string list;
  fresh  : remake;
  build  : (string -> t list -> out_channel -> string list -> (string, string) result);
}

let dot oc (rs : t list) =
  let rec rulz l =
    match l with
    | [] -> Ok ""
    | r :: tl (* (target, (srcs, fresh, build)) *) ->
      let target = r.target
      and srcs = r.srcs
      and fresh = r.fresh
      and _build = r.build in
      Printf.fprintf oc "\n";
      let rec sr s =
        match s with
        | [] -> Ok ""
        | src :: tl' ->
          Printf.fprintf oc "  \"%s\" -> \"%s\"" target src;
          if not (fresh = Outdated) then
            Printf.fprintf oc " [style=\"dashed\"]";
          Printf.fprintf oc ";\n";
          sr tl'
      in
      let _ = sr srcs in
      rulz tl
  in
  Printf.fprintf oc
    "digraph \"#Seppo\" {\n\
     label = \"#Seppo internal created files' dependencies\n\
     https://Seppo.Social\";\n\
     rankdir=LR;\n";
  let r = rulz rs in
  Printf.fprintf oc "}\n";
  r

let rec make pre (rs : t list) (fn : string) : (string, 'a) result =
  let ( >>= ) = Result.bind in
  let find_rule rs fn =
    rs
    |> List.find_opt (fun r -> String.equal fn r.target) in
  (*  let _ = if fn |> String.length > 0
      then
        let c = fn.[0] in
        if c = '/' || c = '.'
        then
          (* TODO should we block anything starting with / or containing .. ? *)
          Logr.warn (fun m -> m "dangerous first character: '%s'" fn)
      in *)
  match fn |> find_rule rs with
  | None -> (
      match File.mtime_0 fn <= 0. with
      | true -> Error ("no rule to make target `" ^ fn ^ "'")
      | false -> Ok fn)
  | Some r (* (target, (srcs, fresh, build)) *)  ->
    Logr.debug (fun m -> m "%s%s.%s %s: %s" pre "Make" "make" r.target (r.srcs |> String.concat "; "));
    assert (r.target = fn);
    let rec visit_siblings num sibs _ =
      match sibs with
      | [] -> Ok ""
      | hd :: tl ->
        Logr.debug (fun m -> m "%s%s.%s  visit_sibling %d %s" pre "Make" "make" num hd);
        make (pre ^ "  ") rs hd (* depth first *)
        >>= visit_siblings (num + 1) tl
    in
    if match r.fresh with
      | Missing -> fn |> File.exists
      | Outdated ->
        (* @TODO handle missing target rules Error *)
        let _ = visit_siblings 0 r.srcs "" in
        let tta = File.mtime_0 r.target in
        let rec is_fresh = function
          | [] -> true
          | hd :: tl ->
            let b = (hd |> File.mtime_0 <= tta) in
            Logr.debug (fun m -> m "%s  %s is %s than %s" pre r.target (if b then "FRESHER" else "OLDER") hd);
            b && (is_fresh tl)
        in
        is_fresh r.srcs
    then
      Ok r.target
    else (
      let _ = r.target |> Filename.dirname |> File.mkdir_p File.pDir in
      File.out_channel true r.target (fun oc ->
          Logr.debug (fun m -> m "%s  %s building" pre r.target);
          match r.build pre rs oc r.srcs with
          | Error e' as e ->
            Logr.err (fun m -> m "%s.%s %s: %s" "Make" "make" fn e');
            e
          | Ok _ ->
            Logr.info (fun m -> m "%s.%s %s" "Make" "make" fn);
            Ok r.target
        )
    )

