(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Web.
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

let ( >>= ) = Result.bind
let ( let* ) = Result.bind

let defa = Uri.make ~path:"o/p/" ()
let tagu = Uri.make ~path:"o/t/" ()

(** map until the first Error *)
let list f xs =
  let it xs x =
    let* xs = xs in
    let* x = f x in
    Ok (List.cons x xs)
  in
  xs |> List.fold_left it (Ok [])

type single  = Single of string
type multi   = Multi  of string
type rfc4646 = Rfc4646 of string (* bcp47 *)
type rfc3339 = Rfc3339 of string

let lf i = `Data ( Printf.sprintf "\n%s" (String.make (2*i) ' ') )
let ns_a = "http://www.w3.org/2005/Atom"
let att ?(ns="") n v = ((ns,n),v)
let elm ?(ns=ns_a) ?(attr = []) n = `El_start ( (ns, n), attr)

module Link = struct
  type rel  = Rel of single
  type t = {
    href         : Uri.t;
    rel          : rel option;
    title        : string option;
  }

  let self = Rel (Single "self")
  let last = Rel (Single "last")
  let first = Rel (Single "first")
  let next = Rel (Single "next")
  let prev = Rel (Single "previous")
  let make ?(title = None) ?(rel = None) href = { href; rel; title; }

  let encode r =
    let open Csexp in
    let l = [] in
    let l = (match r.rel with | None -> l
                              | Some (Rel (Single v)) -> Atom "rel" :: Atom v :: l) in
    let l = (match r.title with | None -> l
                                | Some v -> Atom "title" :: Atom v :: l) in
    let l = Atom "href" :: Atom (r.href |> Uri.to_string) :: l in
    List l

  let decode s =
    let open Csexp in
    let rec pairs xs r =
      match xs with
      | Atom "rel"   :: Atom x :: tl -> pairs tl {r with rel=Some (Rel (Single x))}
      | Atom "title" :: Atom x :: tl -> pairs tl {r with title=Some x}
      | [] -> Ok r
      | _ -> Error "unexpected field"
    in
    match s with
    | List ( Atom "href" :: Atom href :: tl ) ->
      href
      |> Uri.of_string
      |> make
      |> pairs tl
    | _ -> Error "unexpected field"

  let to_atom_signals li =
    let href = li.href |> Uri.to_string in
    let attr = [att "href" href] in
    let attr = match li.rel with
      | None                -> attr
      | Some Rel Single rel -> att "rel" rel :: attr in
    let attr = match li.title with
      | None       -> attr
      | Some title -> att "title" title :: attr in
    elm ~attr "link"

  (* deprecated shorthand *)
  let link ~title ~rel ~href =
    let rel = Some rel in
    make ~title ~rel href
    |> to_atom_signals
end

module Category = struct
  type label    = Label of single
  type term     = Term  of single
  type t        = label * term * Uri.t

  let encode (Label (Single l), Term (Single t), u) =
    Csexp.(List [
        Atom "label";  Atom l;
        Atom "term";   Atom t;
        Atom "scheme"; Atom (u |> Uri.to_string)
      ])

  let decode s =
    match s with
    | Csexp.(List [
        Atom "label";  Atom l;
        Atom "term";   Atom t;
        Atom "scheme"; Atom u
      ]) -> Ok (Label (Single l), Term (Single t), u |> Uri.of_string)
    | _ -> Error "expected category but found none"
end

type id      = Id of string

(* Being "super-careful" https://code.mro.name/mro/ProgrammableWebSwartz2013/src/master/content/pages/2-building-for-users.md
 *
 * geohash uses a base 32 https://codeberg.org/mro/geohash/src/commit/ed8e71a03e377b472054a3468979a1cd77fc090d/lib/geohash.ml#L73
 *
 * See also https://opam.ocaml.org/packages/base32/ for int (we need more bits)
*)
module Base24 = struct
  open Optint.Int63

  let alphabet = Bytes.of_string "23456789abcdefghkrstuxyz"
  let base     = 24 |> of_int

  (* encode the n right chars of x *)
  let encode chars x =
    let int_to_char i = i |> Bytes.get alphabet in
    let rec f i x' b =
      match i with
      | -1 -> b
      | _ ->
        rem x' base |> to_int |> int_to_char |> Bytes.set b i;
        f (i - 1) (div x' base) b
    in
    chars |> Bytes.create |> f (chars - 1) x |> Bytes.to_string

  let decode hash =
    let int_of_char c =
      (* if we want it fast, either do binary search or construct a sparse LUT from chars 0-z -> int *)
      match c |> Bytes.index_opt alphabet with
      | None   -> Error c
      | Some i -> Ok i
    and len = hash |> String.length in
    match len <= 7 with
    | false -> Error '_'
    | true  ->
      let rec f idx x =
        match len - idx with
        | 0 -> Ok x
        | _ ->
          let* v = hash.[idx] |> int_of_char in
          v |> of_int
          |> add (mul x base)
          |> f (idx + 1)
      in
      f 0 zero
end

let of_rfc3339 (Rfc3339 t) =
  match t |> Ptime.of_rfc3339 with
  | Error _    -> Error "expected rfc3339"
  | Ok (t,_,_) -> Ok t

let mk_auth ~base a =
  let host = Uri.host base |> Option.value ~default:"-" in
  let userinfo = Uri.user a |> Option.value ~default:"-" in
  let s = Uri.make ~host ~userinfo () |> Uri.to_string in
  let le = s |> String.length in
  "@" ^ String.sub s 2 (le-2)

module Entry = struct
  type t = {
    id         : Uri.t;
    (* assumes an antry has one language for title, tags, content. *)
    lang       : rfc4646;
    author     : Uri.t;
    title      : string;
    published  : rfc3339;
    updated    : rfc3339;
    links      : Link.t list;
    categories : Category.t list;
    content    : string;
  }

  (** inspired by https://code.mro.name/mro/ShaarliGo/src/cb798ebfae17431732e37a94ee80b29bd3b78911/atom.go#L302 *)
  let id_make t =
    let secs_since_epoch t : Optint.Int63.t =
      let (d',ps') = Ptime.epoch |> Ptime.diff t |> Ptime.Span.to_d_ps in
      let open Optint.Int63 in
      let ( +. ) = add
      and ( *. ) = mul
      and s = Int64.div ps' 1_000_000_000_000L |> of_int64
      and day_s = 24 * 60 * 60 |> of_int
      and d' = d' |> of_int in
      d' *. day_s +. s
    in
    let path = t |> secs_since_epoch |> Base24.encode 7 in
    Logr.debug (fun m -> m "id_make %s" path);
    Uri.make ~path ()

  let encode e =
    let Rfc4646 lang  = e.lang
    and Rfc3339 published = e.published
    and Rfc3339 updated   = e.updated
    in
    Csexp.(
      List [
        Atom "id";         Atom (e.id |> Uri.to_string);
        Atom "lang";       Atom lang;
        Atom "title";      Atom e.title;
        Atom "author";     Atom (e.author |> Uri.to_string);
        Atom "published";  Atom published;
        Atom "updated";    Atom updated;
        Atom "links";      List (e.links      |> List.map Link.encode);
        Atom "categories"; List (e.categories |> List.map Category.encode);
        Atom "content";    Atom e.content;
      ] )

  (* I am unsure if similar to https://opam.ocaml.org/packages/decoders-sexplib/
   * could help.
  *)
  let decode s =
    match s with
    | Csexp.(List [
        Atom "id";         Atom id;
        Atom "lang";       Atom lang;
        Atom "title";      Atom title;
        Atom "author";     Atom author;
        Atom "published";  Atom published;
        Atom "updated";    Atom updated;
        Atom "links";      List links;
        Atom "categories"; List categories;
        Atom "content";    Atom content;
      ]) ->
      let id           = id |> Uri.of_string
      and lang         = Rfc4646 lang
      and author       = author |> Uri.of_string
      and published    = Rfc3339 published
      and updated      = Rfc3339 updated in
      let* links       = links      |> list Link.decode in
      let* categories  = categories |> list Category.decode in
      Ok { id; lang; author; title; published; updated; links; categories; content }
    | _ -> Error "not implemented yet"

  let decode_channel ic =
    let* lst = ic |> Csexp.input_many in
    let* lst = lst |> list decode in
    Ok lst

  let one_from_channel ic =
    let* r = ic |> Csexp.input in
    r |> decode

  let from_text_plain ~published ~author ~lang ~uri title content =
    Logr.debug (fun m -> m "new note %s\n%s" title content);
    let links      = [] in
    let categories = []
    and links = (if uri |> Uri.host |> Option.is_none
                 then links
                 else (uri |> Link.make) :: links)
    and updated    = published in
    let* t         = published |> of_rfc3339 in
    let id         = t |> id_make in
    (*
     * - add attributedTo, id
     * - extract microformats (tags, mentions)
     * - via and thanks -> link via
     * - emojis -> tags
     *)
    Ok { id; lang; author; published; updated; links; title; categories; content }

  let from_channel ?(published = Ptime_clock.now ()) ?(author = Uri.make ()) ~lang ic =
    Logr.debug (fun m -> m "Rfc4287.from_channel");
    let l1  = input_line ic
    and buf = Buffer.create 512
    and published = Rfc3339 (published |> Ptime.to_rfc3339) in
    let uri = l1 |> Uri.of_string in
    let l1,uri = (if uri |> Uri.host |> Option.is_none
                  then (l1, Uri.empty)
                  else
                    let l1 = try
                        input_line ic
                      with End_of_file -> "" in
                    (l1,uri) ) in
    (try
       while true do
         ic
         |> input_line
         |> Buffer.add_string buf;
         Buffer.add_char buf '\n'
       done
     with End_of_file -> ());
    buf
    |> Buffer.contents
    |> from_text_plain ~published ~author ~lang ~uri l1

  let save _ =
  (*
   * - apend to storage csexp (tag feed
   * - update indices (id & url cdbs)
   * - recreate recent pages
   * - queue subscriber notification (aka followers)
   *)
    Error "not implemented yet"

  let to_atom_signals ~base e =
    let r = lf 1 :: `El_end :: [] in
    let r =
      lf 2 :: elm ~attr:[att "type" "text"] "content" :: `Data (e.content) :: `El_end ::
      r in
    let cafo init Category.(Label (Single lbl),Term (Single trm),sch) =
      let sch = sch |> Http.reso ~base |> Uri.to_string in
      lf 2 :: elm ~attr:[att "label" lbl; att "term" trm; att "scheme" sch;] "category" :: `El_end ::
      init in
    let r = e.categories |> List.fold_left cafo r in
    let lifo init item = lf 2 :: Link.to_atom_signals item :: `El_end :: init in
    let r = e.links |> List.fold_left lifo r in
    let Rfc4646 lang = e.lang in
    let self = e.id |> Http.reso ~base in
    let id = self |> Uri.to_string in
    let r =
      lf 2 :: Link.link ~title:None ~href:self ~rel:Link.self :: `El_end ::
      r in
    let author = e.author |> mk_auth ~base in
    let Rfc3339 updated = e.updated in
    let Rfc3339 published = e.published in
    let r =
      lf 2 :: elm "id" :: `Data (id) :: `El_end ::
      lf 2 :: elm ~attr:[att "type" "text"] "title" :: `Data (e.title) :: `El_end ::
      lf 2 :: elm "updated" :: `Data (updated) :: `El_end ::
      lf 2 :: elm "published" :: `Data (published) :: `El_end ::
      lf 2 :: elm "author" :: elm "name" :: `Data ( author ) :: `El_end :: `El_end ::
      r in
    lf 1 :: elm ~attr:[
      ((Xmlm.ns_xmlns,"xmlns"),ns_a);
      ((Xmlm.ns_xml,"lang"),lang);
    ] "entry" ::
    r
end

module Feed = struct

  let head_to_atom_signals
      ~base
      ~(self : Uri.t)
      ~prev
      ~next
      ~first
      ~last
      ~title
      ~updated
      ~lang
      ~author
      init =
    let r = init in
    let r = match next with
      | None      -> r
      | Some href -> lf 1 :: Link.link ~title:None ~href ~rel:Link.next :: `El_end ::
                     r in
    let r = match prev with
      | None      -> r
      | Some href -> lf 1 :: Link.link ~title:None ~href ~rel:Link.prev :: `El_end ::
                     r in
    let r =
      lf 1 :: Link.link ~title:None ~href:self  ~rel:Link.self  :: `El_end ::
      lf 1 :: Link.link ~title:(Some "fi") ~href:first ~rel:Link.first :: `El_end ::
      lf 1 :: Link.link ~title:(Some "1") ~href:last  ~rel:Link.last  :: `El_end ::
      r in
    let id = self |> Http.reso ~base |> Uri.to_string in
    let Rfc3339 updated = updated in
    let Rfc4646 lang = lang in
    let author = author |> mk_auth ~base in
    (* let open Markup in *)
    let r = lf 1 :: elm "title" :: `Data ( title ) :: `El_end ::
            lf 1 :: elm "id" :: `Data ( id ) :: `El_end ::
            lf 1 :: elm "updated" :: `Data ( updated ) :: `El_end ::
            lf 1 :: elm ~attr:[att "uri" "Seppo.Social"] "generator" :: `Data ( "Seppo - Personal Social Web" ) :: `El_end ::
            lf 1 :: elm "author" :: elm "name" :: `Data ( author ) :: `El_end :: `El_end ::
            r in
    elm ~attr:[
      ((Xmlm.ns_xmlns,"xmlns"),ns_a);
      ((Xmlm.ns_xml,"lang"),lang);
      ((Xmlm.ns_xml,"base"),base |> Uri.to_string);
    ] "feed" ::
    r

  let to_atom_signals
      ~base
      ~(self : Uri.t)
      ~prev
      ~next
      ~first
      ~last
      ~title
      ~updated
      ~lang
      ~author
      entries =
    let r = lf 0 :: `El_end :: [] in
    let entry init item = Entry.to_atom_signals ~base item @ init in
    let r  = entries |> List.rev |> List.fold_left entry r in
    head_to_atom_signals
      ~base
      ~self
      ~prev
      ~next
      ~first
      ~last
      ~title
      ~updated
      ~lang
      ~author
      r
end
