(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Web.
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

(* rather leverage fileutils? *)

let rec find_path_tail predicate ?(prfx = "") ?(sep = "/") lst =
  match lst with
  | [] -> Error "not found"
  | hd :: tl ->
    let prfx = sep ^ hd ^ prfx in
    match predicate prfx with
    | Error _ as e -> e
    | Ok true -> Ok prfx
    | Ok false -> find_path_tail predicate ~prfx ~sep tl

let mtime_0 ?(default = 0.) fn =
  (* Logr.debug (fun m -> m "mtime_0 %s" fn); *)
  try (Unix.stat fn).st_mtime
  with
  | _ -> default

let pDir = 0o755
let pFile = 0o644
let pFileRO = 0o444

let rec mkdir_p perm n =
  (* TODO should we block anything starting with / or . ? *)
  match Sys.file_exists n with
  | true -> Ok n
  | false -> (
      match n |> Filename.dirname |> mkdir_p perm with
      | Ok _ -> (
          Unix.(try
                  mkdir n perm;
                  Ok n
                with Unix_error (n, a, b) ->
                  Error ((n |> error_message) ^ ": " ^ a ^ " " ^ b)))
      | e -> e)

let count_dir max pred dn =
  let dh = dn |> Unix.opendir in
  let rec next i =
    if i > max
    then i
    else
      try
        let step = if dh |> Unix.readdir |> pred
          then 1
          else 0
        in next (i + step)
      with End_of_file -> i
  in
  let ret = next 0 in
  dh |> Unix.closedir;
  ret

let exists = Sys.file_exists

(* evtl. https://rosettacode.org/wiki/Read_entire_file#OCaml *)
let to_bytes (fn : string) : bytes =
  try 
    let len = (Unix.stat fn).st_size in
    let ic = open_in_gen [ Open_binary; Open_rdonly ] 0 fn in
    let buf = Bytes.create len in
    really_input ic buf 0 len;
    close_in ic;
    buf
  with _ -> Bytes.empty

let to_string fn = fn
                   |> to_bytes
                   |> Bytes.to_string

let cat fn = try
    fn |> to_string |> Result.ok
  with
  | Sys_error e -> Error e
  | Invalid_argument e -> Error e
(* | End_of_file -> Error ("error reading file " ^ fn) *)

let in_channel fn rdr =
  let ic = open_in_gen [ Open_rdonly; Open_binary ] 0 fn in
  let ret = rdr ic in
  close_in ic;
  ret

let out_channel atomic ?(mode = [ Open_append; Open_binary; Open_creat; Open_wronly ]) ?(perm = pFile) fn wrtr =
  let fn' = fn ^ (if atomic then "~" else "") in
  let oc = open_out_gen mode perm fn' in
  let ret = wrtr oc in
  oc |> close_out;
  if atomic then Unix.rename fn' fn;
  ret

let touch fn =
  fn
  |> open_out_gen [ Open_append; Open_binary; Open_creat; Open_wronly ] pFileRO
  |> close_out

let cp' src oc =
  (* primitive take copy inspired by
     https://sylvain.le-gall.net/ocaml-fileutils.html *)
  let len = 16 * 0x400 in
  let buf = Bytes.create len in
  let ic = open_in_gen [ Open_rdonly; Open_binary ] 0 src in
  let r = ref 0 in
  while (r := input ic buf 0 len; !r <> 0) do
    output oc buf 0 !r
  done;
  close_in ic

module Path = struct
  let hd_tl (ch : char) (str : string) : (string * string) option =
    Option.bind
      (String.index_opt str ch)
      (fun len ->
         let hd = String.sub str 0 len
         and tl = let pp1 = len + 1 in
           String.sub str pp1 ((String.length str) - pp1)
         in Some (hd, tl))

  let hd (ch : char) (str : string) : string option =
    Option.bind
      (hd_tl ch str)
      (fun (s,_) -> Some s)

  let tl (ch : char) (str : string) : string option =
    Option.bind
      (hd_tl ch str)
      (fun (_,s) -> Some s)
end

