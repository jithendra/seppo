(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Web.
 *
 * web.ml
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

let ( >>= ) = Result.bind
let ( let* ) = Result.bind
let chain a b =
  let f a = Ok (a, b) in
  Result.bind a f
let seppo = Uri.make ~userinfo:"seppo" ~host:"seppo.social" ()

let to_channel ~xslt oc l =
  let pi = Some ("xml-stylesheet","type='text/xsl' href='../themes/current/"^xslt^".xslt'")
  and readme = Some {|
The html you see here is for compatibility with https://sebsauvage.net/wiki/doku.php?id=php:shaarli

The main reason is backward compatibility for e.g. http://mro.name/ShaarliOS and
https://github.com/dimtion/Shaarlier
|} in
  Storage.Atom.to_chan ~pi ~readme l oc;
  Ok ()

let cookie_timeout tnow =
  30 * 60
  |> Ptime.Span.of_int_s
  |> Ptime.add_span tnow
  |> Option.get

(* payload *)
let cookie_encode (Auth.Uid uid, t) =
  Logr.debug (fun m -> m "Web.cookie_encode");
  let t = t |> Ptime.to_rfc3339 in
  Csexp.(List [ Atom uid; Atom t ] |> to_string)

(* payload *)
let cookie_decode c =
  let open Csexp in
  match c |> parse_string with
  | Ok List [ Atom uid; Atom t ] -> (
      match t |> Ptime.of_rfc3339 with
      | Error _ -> Error "expected valid rfc3339"
      | Ok (t,  _, _) -> Ok (Auth.Uid uid, t))
  | _ -> Error "expected cookie csexp"

let cookie_name = "#Seppo!"

let cookie_make (req : Http.Request.t) =
  Cookie.to_string
    ~domain:req.host
    ~http_only:true
    ~path:req.script_name
    ~same_site:`Strict
    ~secure:true
    cookie_name

let cookie_new_session
    ?(nonce12 = Cookie.random_nonce ())
    ?(tnow = Ptime_clock.now ())
    sec32
    req
    uid =
  (uid, tnow |> cookie_timeout)
  |> cookie_encode
  |> Cstruct.of_string
  |> Cookie.encrypt sec32 nonce12
  |> cookie_make req
  |> Http.H.set_cookie

(**
 * input type'textarea' => textarea
 * input type'submit'   => button
*)
let form0 tit n ips err (frm : Http.Form.t) =
  let elm name atts = `El_start (("", name), atts)
  and att (n,v) = (("", n), v) in
  let fofi err init ((n,(t,atts)) : Http.Form.input) =
    let atts = atts |> List.fold_left (fun init a -> att a :: init) [] in
    let atts = match List.assoc_opt n frm with
      | None   -> atts
      | Some s -> att ("value", s |> String.concat "") :: atts in
    let txt v l =
      `Data (l |> List.assoc_opt v |> Option.value ~default:"")
    in
    init
    @ (err |> List.fold_left (fun init (f,e) ->
        if String.equal f n
        then elm "div" [att ("class","err"); att ("data-name",n)] :: `Data (e) :: `El_end :: init
        else init) [])
    @ match t with
    (* type is abused to mark textarea. Here we put it right again. *)
    | "textarea" -> [ elm "textarea" ([ att ("name",n); ] @ atts); txt ("","value") atts; `El_end ]
    | "submit"   -> [ elm "button" ([ att ("name",n); att ("type",t); ] @ atts); txt ("","value") atts; `El_end ]
    | _          -> [ elm "input"  ([ att ("name",n); att ("type",t); ] @ atts); `El_end; ]
  in
  [
    elm "html" [ ((Xmlm.ns_xml,"base"),"../"); ((Xmlm.ns_xmlns,"xmlns"), "http://www.w3.org/1999/xhtml"); ];
    elm "head" [];
    elm "meta" [ att ("name","generator"); att ("content","Seppo.social")]; `El_end;
    elm "link" [ att ("rel","icon"); att ("type","image/jpg"); att ("href","../me-avatar.jpg");]; `El_end;
    elm "title" []; `Data ( tit ); `El_end;
    `El_end;
    elm "body" [];
    elm "ul" [att ("id","err")]
  ] @
  (* at first display all errors with key "" *)
  (err |> List.fold_left (fun init (f,e) -> match (f,e) with
       | "",e -> elm "li" [] :: `Data (e) :: `El_end :: init
       | _    -> init) [])
  @ [
    `El_end;
    elm "form" [ att ("method","post"); att ("name",n); att ("id",n) ]
  ]
  @ List.fold_left (fofi err) [] ips
  @ [
    `El_end;
    `El_end;
    `El_end;
  ]

module Login = struct
  let path = "/login"
  module F = Http.Form

  let i_tok : F.input = ("token", ("hidden", []))
  let i_uid : F.input = ("login", ("text", [
      ("autofocus","autofocus");
      ("required","required");
    ]))
  let i_pwd : F.input = ("password", ("password", [ ("required","required"); ]))
  let i_lol : F.input = ("longlastingsession", ("checkbox", [ ]))
  let i_ret : F.input = ("returnurl", ("hidden", []))
  let i_but : F.input = ("Login", ("submit", []))
  let n (i,_) v = (i,[v])

  let get oc (tok, (r : Http.Request.t)) =
    let ur = r |> Http.Request.request_uri |> Uri.of_string in
    Http.s200x |> Http.head oc;
    [
      n i_tok tok;
      n i_ret ("returnurl" |> Uri.get_query_param ur |> Option.value ~default:"");
      n i_but "Login";
    ]
    |> form0 "🌺 Login" "loginform" [i_tok;i_uid;i_pwd;i_lol;i_ret;i_but] []
    |> to_channel ~xslt:"loginform" oc

  (* check uid+pwd, Always take at least 2 seconds, if ok set session cookie and
     redirect to returnurl, call ban_f otherwise. *)
  let post tnow ban_f _oc (_tok, (frm, (req : Http.Request.t))) =
    let sleep = 2 in
    Logr.debug (fun m -> m "Web.Login.post, sleep %d seconds..." sleep);
    Unix.sleep sleep;
    let flt r = function
      | (("login", [_]) as v)
      | (("password", [_]) as v)
      | (("returnurl", [_]) as v)
      | (("token", [_]) as v) -> r |> List.cons v
      | (f, _) -> Logr.info (fun m -> m "unconsumed form field: '%s'" f); r
    and cmp (a, _) (b, _) = String.compare a b in
    match frm |> List.fold_left flt [] |> List.sort cmp with
    | [ ("login", [uid]);
        ("password", [pwd]);
        ("returnurl", [retu]);
        ("token", [_] (* token has to be already checked by the caller. *)); ] ->
      Ok (Auth.Uid uid, pwd)
      >>= Auth.chk_file Auth.fn
      >>= (fun uid ->
          Cfg.CookieSecret.(make "" >>= from_file)
          >>= chain (Ok uid))
      >>= (fun (uid, sec) ->
          cookie_new_session ~tnow sec req uid |> Result.ok)
      |> (function
          | Ok cv ->
            Http.s302 ~header:[ cv ] retu
          | Error "invalid username or password" ->
            Logr.debug (fun m -> m "%s.%s 0" "Login" "post");
            ban_f tnow req.remote_addr;
            Http.s403
          | Error e ->
            Logr.err (fun m -> m "%s.%s: %s" "Login" "post" e);
            Http.s500)
    | _ ->
      Logr.info (fun m -> m "%s.%s 2" "Login" "post");
      Http.s401
end

module Logout = struct
  let path = "/logout"

  (* GET requests should be idempotent, have no side effects.
     TODO: We could use a form button for this and POST: https://stackoverflow.com/a/33880971/349514*)
  let get _oc ((_ : Auth.uid option), req) =
    Http.s302 ~header:[ ("Set-Cookie", cookie_make req "") ] ".."
end

let check_token exp ((v : Http.Form.t), vv) =
  Logr.debug (fun m -> m "Web.check_token");
  match Uri.get_query_param (Uri.make ~query:v ()) "token" with
  | Some tok ->
    if String.equal exp tok
    then Ok (tok, (v,vv))
    else Http.s403
  | None ->
    Logr.warn (fun m -> m "check_token: no token in form: %s" (Uri.encoded_of_query v));
    Http.s400

(** get uid from session if still running *)
let ases tnow (r : Http.Request.t) =
  Logr.debug (fun m -> m "%s.%s" "Web" "ases");
  let uid = function
    (* check if the session cookie carries a date in the future *)
    | ("#Seppo!" as n, pay) :: [] ->
      assert (n = cookie_name);
      let sec = Cfg.CookieSecret.(make "" >>= from_file) |> Result.get_ok in
      Option.bind
        (Cookie.decrypt sec pay)
        (fun c ->
           Logr.debug (fun m -> m "%s.%s cookie value '%s'" "Web" "ases" c);
           match c |> cookie_decode with
             Ok (uid, tend) ->
             if tend > tnow
             then
               (Logr.debug (fun m -> m "%s.%s session valid until %a" "Web" "ases" Ptime.pp tend);
                Some uid)
             else None
           | _ -> None)
    | _ ->
      Logr.debug (fun m -> m "%s.%s #Seppo! cookie not found." "Web" "ases");
      None
  in Ok (r.http_cookie |> Cookie.of_string |> uid, r)

module Credentials = struct
  let path = "/credentials"
  module F = Http.Form

  let i_tok : F.input = ("token", ("hidden", []))
  let i_uid : F.input = ("setlogin", ("text", [
      ("autofocus","autofocus");
      ("required","required");
      ("maxlength","50");
      ("minlength","1");
      ("pattern", {|^[a-zA-Z0-9_.-]+$|});
      ("placeholder","Your short name as 'myname' in @myname@example.com");
    ]))
  let i_pwd : F.input = ("setpassword", ("password", [
      ("required","required");
      ("maxlength","200");
      ("minlength","12");
      ("pattern", {|^\S([^\n\t]*\S)?$|});
      ("placeholder","good passwords: xkcd.com/936");
    ]))
  let i_pw2 : F.input = ("confirmpassword", ("password", [
      ("required","required");
      ("placeholder","the same once more");
    ]))
  let i_but : F.input = ("Save", ("submit", []))
  let n (i,_) v = (i,[v])

  let get oc (token, (Auth.Uid uid, _req)) =
    let _need_uid = Auth.(is_setup fn) in
    Http.s200x |> Http.head oc;
    [
      n i_tok token;
      n i_uid uid;
      n i_but "Save config";
    ]
    |> form0 "🌻 Change Password" "changepasswordform" [i_tok;i_uid;i_pwd;i_pw2;i_but] []
    |> to_channel ~xslt:"changepasswordform" oc

  let post _ oc (_tok, (frm, (Auth.Uid _, (req : Http.Request.t)))) =
    let _boo = File.exists Auth.fn in
    Logr.debug (fun m -> m "Web.Credentials.post form name='%s'" "changepasswordform");
    assert (Http.Mime.app_form_url = req.content_type);
    let run() =
      (* funnel additional err messages into the form *)
      let err msg (name,_) pred = if pred
        then Ok ()
        else Error (name,msg) in
      let* uid = F.string i_uid frm in
      let* pwd = F.string i_pwd frm in
      let* pw2 = F.string i_pw2 frm in
      let* _ = String.equal pwd pw2 |> err "not identical to password" i_pw2 in
      Ok (Auth.Uid uid,pwd)
    in
    match run() with
    | Ok (uid,pwd) ->
      let* _ = Auth.((uid, pwd) |> to_file fn) in
      let* _ = (req |> Http.Request.base) |> Cfg.Base.(to_file fn) in
      let* sec = Result.map_error (Http.err500 "failed to read cookie secret") (Cfg.CookieSecret.(make "" >>= from_file)) in
      let header = [ cookie_new_session sec req (uid) ] in
      let path = match As2.Person.(Make.make "" rulez target) with
        | Error _ -> "/profile"
        | Ok _    -> "/.." in
      Http.s302 ~header (req.script_name ^ path)
    | Error ee ->
      Logr.err (fun m -> m "%s.%s" "Web.Credentials" "post");
      Http.s422x |> Http.head oc;
      frm
      |> form0 "🌻 Change Password" "changepasswordform" [i_tok;i_uid;i_pwd;i_pw2;i_but] [ee]
      |> to_channel ~xslt:"changepasswordform" oc
end

(** if no uid then redirect to login/credentials page *)
let uid_redir = function
  | (Some uid, r) -> Ok (uid, r)
  | (None, (r : Http.Request.t)) ->
    let r302 p =
      let path = r.script_name ^ p in
      let query = [("returnurl",[r |> Http.Request.abs])] in
      Uri.make ~path ~query () |> Uri.to_string |> Http.s302
    in
    if Auth.(is_setup fn)
    then r302 Login.path
    else (
      if Credentials.path = r.path_info
      then (
        Logr.info (fun m -> m "credentials are not set, so go on with an empty uid. %s" r.path_info);
        Ok (Auth.dummy, r))
      else r302 Credentials.path
    )

module Profile = struct
  let path = "/profile"
  module F = Http.Form

  let i_tok : F.input = ("token", ("hidden", []))
  let i_tit : F.input = ("title", ("text",          [ ("required","required"); ("maxlength","100"); ("autofocus","autofocus"); ("placeholder","A one-liner describing this #Seppo!"); ]))
  let i_bio : F.input = ("bio",   ("textarea",      [ ("required","required"); ("maxlength","2000"); ("rows","10"); ("placeholder","more text describing this #Seppo!"); ]))
  let i_tzo : F.input = ("timezone", ("text",       [ ("required","required"); ("maxlength","100"); ("placeholder","Europe/Amsterdam What timezone do you usually write from"); ]))
  let i_lng : F.input = ("language", ("text",       [ ("required","required"); ("maxlength","2"); ("minlength","2"); ("pattern", {|^[a-z]*$|}); ("placeholder","nl What language do you usually write in"); ]))
  let i_ppp : F.input = ("posts_per_page", ("text", [ ("required","required"); ("maxlength","3"); ("minlength","1"); ("pattern", {|^[1-9][0-9]*$|}); ("placeholder","50 How many posts should go on one page"); ]))
  let i_but : F.input = ("save",  ("submit", []))
  let n (i,_) v = (i,[v])

  let get oc (token, (_uid, _req)) =
    let p = Cfg.Profile.load () in
    Http.s200x |> Http.head oc;
    let Rfc4287.Rfc4646 lng = p.language in
    [
      n i_tok token;
      n i_tit p.title;
      n i_bio p.bio;
      n i_lng lng;
      n i_tzo (Timedesc.Time_zone.name p.timezone);
      n i_ppp (string_of_int p.posts_per_page);
      n i_but "Save";
    ]
    |> form0 "🎭 Profile" "configform" [i_tok;i_tit;i_bio;i_lng;i_tzo;i_ppp;i_but] []
    |> to_channel ~xslt:"configform" oc

  let post _tnow oc (_tok, (frm, (Auth.Uid uid, (_req : Http.Request.t)))) =
    let run () =
      Logr.debug (fun m -> m "%s.%s save" "Web.Profile" "post");
      let* title   = F.string i_tit frm in
      let* bio     = frm |> F.string i_bio in
      let* language= F.string i_lng frm in
      let language = Rfc4287.Rfc4646 language in
      let* timezone= F.string i_tzo frm in
      let timezone = Timedesc.Time_zone.(make timezone
                                         |> Option.value ~default:(
                                           local ()
                                           |> Option.value ~default:utc)) in
      let* ppp = F.string i_ppp frm in
      let posts_per_page = ppp
                           |> int_of_string_opt
                           |> Option.value ~default:50 in
      let p : Cfg.Profile.t = {title;bio;language;timezone;posts_per_page} in
      let eee e = ("",e) in
      let* _ = Result.map_error eee Cfg.Profile.(to_file target p) in
      let* ba = Result.map_error eee Cfg.Base.(from_file fn) in
      let u = Uri.with_userinfo ba (Some uid) in
      let* _ = Result.map_error eee (As2.Webfinger.Server.make u) in
      let* _ = Result.map_error eee As2.Webfinger.(Make.make "" rulez target) in
      let* _ = Result.map_error eee As2.Person.(Make.make "" rulez target) in
      let* _ = Result.map_error eee As2.PersonX.(Make.make "" rulez target) in
      Ok (p,ba) in
    match run() with
    | Ok (profile,base) ->
      if File.exists Storage.target
      then
        (Logr.debug (fun m -> m "already exists: %s" Storage.target);
         Http.s302 "../")
      else (
        Logr.debug (fun m -> m "add the first post from welcome.en.txt");
        let author    = Uri.make ~userinfo:uid ~host:(Uri.host base |> Option.value ~default:"example.com") ()
        and lang      = Rfc4287.Rfc4646 "en"
        and msg       = Res.read "/welcome.en.txt" |> Option.value ~default:"Ouch, missing welcome."
        and published = Rfc4287.Rfc3339( Ptime_clock.now() |> Ptime.to_rfc3339 )
        and uri       = Uri.with_userinfo seppo None
        in match
          msg
          |> Rfc4287.Entry.from_text_plain ~published ~author ~lang ~uri "Hello, #Seppo!"
          >>= Main.sift_urls
          >>= Main.sift_tags Tag.cdb
          >>= Main.sift_handles
          >>= Main.publish_note ~base ~profile ~author:author
        with
        | Ok _    -> Http.s302 "../"
        | Error _ -> Http.s500;
      )
    | Error ("",e) ->
      Logr.err (fun m -> m "%s.%s %s" "Web.Profile" "post" e);
      Http.s500;
    | Error (_f,e) ->
      Logr.err (fun m -> m "%s.%s %s" "Web.Profile" "post" e);
      Http.s422x |> Http.head oc;
      frm
      |> form0 "🎭 Profile" "configform" [i_tok;i_tit;i_bio;i_lng;i_tzo;i_ppp;i_but] []
      |> to_channel ~xslt:"configform" oc
end

module Post = struct
  let path = "/post"
  module F = Http.Form

  let i_tok : F.input = ("token", ("hidden", []))
  let i_pos : F.input = ("post", ("text", [ ("required","required"); ]))
  let i_but : F.input = ("Add", ("submit", []))
  let n (i,_) v = (i,[v])

  (* only parameter is 'post'
   * https://code.mro.name/github/Shaarli-Vanilla/src/master/index.php#L427
   * https://code.mro.name/github/Shaarli-Vanilla/src/029f75f180f79cd581786baf1b37e810da1adfc3/index.php#L1548
  *)
  let get oc (token, (_uid, _req)) =
    Logr.debug (fun m -> m "%s.%s" "Web.Post" "get");
    (* - look up url in storage
     * - if not present:
     *   - if title not present
     *     then
     *       try to get from url
     *       use title, description, keywords
     * - show 'linkform'
    *)
    Http.s200x |> Http.head oc;
    [
      n i_tok token;
      n i_pos "foo, bar";
      n i_but "Save";
    ]
    |> form0 "Add" "linkform" [i_tok;i_pos;i_but;] []
    |> to_channel ~xslt:"linkform" oc

  (* https://code.mro.name/github/Shaarli-Vanilla/src/master/index.php#L1479 *)
  let post _ _oc (_tok, (_frm, (_uid, (_req : Http.Request.t)))) =
    Logr.debug (fun m -> m "%s.%s" "Web.Post" "post");
    Http.s501
end

module Tools = struct
  let get _ = Http.s501
end

module Session = struct
  let get oc (uid, _req) =
    match uid with
    | None -> (* no ban penalty but 404 nevertheless. *) Http.s404
    | Some (Auth.Uid v) ->
      Http.s200x |> Http.head oc;
      output_string oc v;
      Ok()
end

