(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Web.
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

let ( let* ) = Result.bind
let fold_bind_lines f init ic =
  let rec row_wise init' =
    try
      let* init' = ic |> input_line |> f init' in
      row_wise init'
    with
    | End_of_file -> Ok init'
  in
  row_wise init

let pre        = "app/var/lib/"
let target     = pre ^ "o/p.s"
let fn_id_cdb  = Mapcdb.Cdb (pre ^ "o/id.cdb")
let fn_url_cdb = Mapcdb.Cdb (pre ^ "o/url.cdb")
let fn_t_cdb   = Mapcdb.Cdb (pre ^ "o/t.cdb")

module Fifo = struct
  type t        = string * int

  let make size fn =
    (fn,size)

  let push byt (fn,size) =
    let sep = '\n' in
    let len = byt |> Bytes.length in
    let keep = size - len - 1 in
    let mode = [ Open_append; Open_binary; Open_creat; Open_wronly ] in
    if keep < try (Unix.stat fn).st_size with _ -> 0
    then (* make space and add *)
      let ret = len |> Bytes.create in
      let buf = keep |> Bytes.create in
      File.in_channel fn (fun ic ->
          really_input ic ret 0 len;
          let _ = input_char ic in
          really_input ic buf 0 keep );
      File.out_channel true ~mode fn (fun oc ->
          output_bytes oc buf;
          output_bytes oc byt;
          output_char oc sep
        );
      Some ret
    else (* just add *)
      (File.out_channel false ~mode fn (fun oc ->
           output_bytes oc byt;
           output_char oc sep
         );
       None)
end

(* a tuple of two (file) positions *)
module TwoPad10 = struct
  let length = 21

  let encode (a,b) =
    Printf.sprintf "0x%08x-0x%08x" a b

  let two a b = (a,b)

  let decode s =
    Scanf.sscanf s "%i-%i" two

  let from_channel ic =
    let r = ref ([]) in
    (try while true do
         let tw = ic |> input_line |> decode in
         r := tw :: !r
       done
     with End_of_file -> ());
    !r
end

(* take an internal index filename and compute the relative page url.
 * e.g. "app/var/lib/o/p/12.ix" -> "o/p-12/"
 *
 * See 00015_--_arch_design.txt
*)
let dir_of_ix ix =
  assert (ix |> St.starts_with ~prefix:pre);
  assert (12 == String.length pre);
  let le = ix |> String.length in
  match String.sub ix 12 (le-15) |> String.split_on_char '/' with
  | ["o" as o;"p" as s;i]   -> Printf.sprintf "%s/%s-%s/" o s i,3
  | ["o" as o;"t" as s;v;i]
  | ["o" as o;"d" as s;v;i] -> Printf.sprintf "%s/%s/%s-%s/" o s v i,4
  | _ -> "/dev/null",0

module Atom = struct
  (* the highest index number and filename *)
  let last_ix dir =
    Logr.debug (fun m -> m "Storage.last_ix %s" dir);
    assert (dir |> St.starts_with ~prefix:pre);
    let open Unix in
    (try mkdir dir File.pDir
     with | Unix_error (EEXIST, "mkdir", _) -> ());
    let d = dir |> opendir in
    let mx = ref (-1) in
    let mf = ref "" in
    (
      try
        let (let*) = Option.bind in
        while true do
          let fi = readdir d in
          let _ =
            let* fi' = fi |> Filename.chop_suffix_opt ~suffix:".ix" in
            let* n = fi' |> int_of_string_opt in
            mx := max !mx n;
            if n == !mx
            then mf := fi;
            None in
          ()
        done
      with End_of_file -> ());
    d |> closedir;
    if !mx < 0
    then None
    else Some ( !mx, dir ^ !mf )

  (* the next id and ix file name *)
  let next_id ~items_per_page dir =
    let bytes_per_item = TwoPad10.length in
    assert (dir |> St.starts_with ~prefix:pre);
    (* get the previously highest index number and name *)
    let mk_ix i = Printf.sprintf "%s%d.ix" dir i in
    let _,i,ix =
      match dir |> last_ix with
      | None     -> 0,0,mk_ix 0
      | Some (mx,_) ->
        let ix = mk_ix mx in
        let i = (try (ix |> Unix.stat).st_size
                 with _ -> items_per_page) / (bytes_per_item + 1) in
        if i < items_per_page
        then mx,i,ix
        else let mx = mx + 1 in
          mx,0,mk_ix mx
    in
    let path,_ = dir_of_ix ix in
    let fragment = i |> Int.to_string in
    let id = Uri.make ~path ~fragment () in
    assert (id |> Uri.to_string |> St.starts_with ~prefix:"o/");
    assert (ix |> St.starts_with ~prefix:(pre ^ "o/"));
    id, ix

  let append_to_ix pos fn_ix =
    Logr.debug (fun m -> m "Storage.append_to_ix ... %s" fn_ix);
    File.out_channel false fn_ix (fun oc ->
        output_bytes oc pos;
        output_char oc '\n' );
    fn_ix

  let to_chan ?(pi = None) ?(readme = None) si oc =
    let piw n v = Printf.fprintf oc "<?%s %s?>\n" n v in
    piw "xml" "version=\"1.0\"";
    (match pi with
     | Some (n,v) -> piw n v
     | None       -> ());
    (match readme with
     | Some v -> Printf.fprintf oc "<!--%s-->\n" v
     | None   -> ());
    let o = Xmlm.make_output ~decl:false (`Channel oc) in
    Xmlm.output o (`Dtd None);
    List.iter (Xmlm.output o) si;
end

module Outbox = struct
  let to_chan j oc =
    Ezjsonm.value_to_channel ~minify:false oc j
end

let id_to_b id =
  id |> Uri.to_string |> Bytes.of_string

let _remake_ix fn ix =
  (* add csexp entry to .s and return (id,position) tuple *)
  let add_1_csx oc sx =
    let ol = pos_out oc in
    sx |> Csexp.to_channel oc;
    let ne = pos_out oc in
    let id = match sx |> Rfc4287.Entry.decode with
      | Error _ -> None
      | Ok r    -> Some r.id in
    (id,(ol,ne)) in
  (* if Some id call fkt with id->(ol,ne) *)
  let add_1_p fkt = function
    | (None,_v)    -> Logr.warn (fun m -> m "add a strut?")
    | (Some id,v)  -> fkt (id_to_b id, v |> TwoPad10.encode |> Bytes.of_string) in
  (* - read all csexps from the source *)
  let ic = open_in_gen [ Open_binary; Open_rdonly ] 0 fn in
  let* sxs = Csexp.input_many ic in
  close_in ic;
  (* copy fn content as csexps to tmp file fn' *)
  let fn' = fn ^ "~" in
  let oc = open_out_gen [ Open_binary; Open_wronly ] File.pFile fn' in
  let cp_csx oc sxs sx = (add_1_csx oc sx) :: sxs in
  let pos = List.fold_left (cp_csx oc) [] sxs in
  close_out oc;
  (* recreate cdb *)
  let none _ = false in
  let add_all fkt = List.iter (add_1_p fkt) pos in
  let _ = Mapcdb.add_many none add_all ix in
  (* swap tmp for real *)
  Unix.rename fn' fn;
  Ok fn

open Rfc4287

let other_ix_base (e : Entry.t) =
  let day (Rfc3339 iso) = "o/d/" ^ String.sub iso 0 10 ^ "/" in
  let open Category in
  let tag init (_,(Term (Single p)),_) = ("o/t/" ^ p ^"/") :: init in
  day e.published
  :: (e.categories |> List.fold_left tag [])

let other_ix_files ~items_per_page (e : Entry.t) =
  let ix init item =
    let _,ix = Atom.next_id ~items_per_page (pre ^ item) in
    ix :: init
  in
  e
  |> other_ix_base
  |> List.fold_left ix []

(* all logical feed urls (including the main feed) outbox etc. *)
let feed_urls (e : Entry.t) =
  let db = Uri.make ~path:"o/d/" () in
  let day (Rfc3339 iso) =
    let p = String.sub iso 0 10 in
    Uri.make ~path:(p ^ "/") () |> Http.reso ~base:db in

  let tb = tagu in
  let open Category in
  let tag (_,(Term (Single p)),_) =
    Uri.make ~path:(p ^ "/") () |> Http.reso ~base:tb in

  let obox = Uri.make ~path:(As2.apub ^ "outbox/") () in
  defa
  :: obox
  :: (e.published |> day)
  :: (e.categories |> List.map tag)

let pat = Str.regexp {|^\(.+\)-[0-9]+/\(\(index\.xml\|as\.json\)?\)$|}

(* evtl. better to storage *)
let unnumbered d =
  let open Str in
  if string_match pat d 0
  then
    (matched_group 1 d) ^ "/", (matched_group 2 d)
  else d, ""

(* evtl. better to storage *)
let num_ix i p =
  let d = p |> Filename.dirname in
  let f = p |> Filename.basename in
  let path = match i with
    | None   -> Printf.sprintf "%s/%s/" d f
    | Some i -> Printf.sprintf "%s/%s-%d/" d f i
  in Uri.make ~path ()

let mk_unnumbered_syml ~depth fn' =
  Logr.debug (fun m -> m "Storage.mk_unnumbered %d %s" depth fn');
  let to_dir = false in
  let ld,ln = fn' |> unnumbered in
  let ln = ld ^ ln in
  let pre = match depth with
    | 3 -> "../../"
    | 4 -> "../../../"
    | _ -> "/dev/null" in
  let fn = pre ^ fn' in
  Logr.debug (fun m -> m "ln -s %s %s # depth:%d" fn ln depth);
  let open Unix in
  (try unlink ln
   with Unix_error(ENOENT, "unlink", _) -> ());
  (try mkdir ld File.pDir
   with Unix_error(EEXIST, "mkdir", _) -> ());
  symlink ~to_dir fn ln;
  (fn, ln)

(* rebuild the single pages (atom/json) plus evtl. the softlink *)
let materialise ~base ~title ~updated ~lang ~author fn_ix =
  Logr.debug (fun m -> m "%s.%s %s" "Storage" "materialise" fn_ix);
  (* fold ix rows into entries. *)
  let hydrate sc init item =
    let p0,_ = TwoPad10.decode item in
    seek_in sc p0;
    let* item = Csexp.input sc in
    let* item = Entry.decode item in
    Ok (item :: init) in
  let* es = File.in_channel target (fun sc ->
      File.in_channel fn_ix (fold_bind_lines (hydrate sc) [])
    ) in
  (* write index.xml and as.json *)
  let path,depth = fn_ix |> dir_of_ix in
  let _ = File.mkdir_p File.pDir path in
  let self = Uri.make ~path () in
  let prev = Some (Uri.with_fragment self (Some "todo")) in
  let next = None in
  let unn,_ = self |> Uri.path |> unnumbered in
  (* evtl. better to storage *)
  let first = num_ix None unn in
  let last  = num_ix (Some 0) unn in
  (* atom index.xml *)
  let si = Feed.to_atom_signals
      ~base
      ~self
      ~prev
      ~next
      ~first
      ~last
      ~title
      ~updated
      ~lang
      ~author
      es in
  let fn = path ^ "index.xml" in
  Logr.debug (fun m -> m "%s.%s %s" "Storage" "materialise" fn);
  File.out_channel true fn (Atom.to_chan ~pi:(Some ("xml-stylesheet","type='text/xsl' href='../../themes/current/posts.xslt'")) si);
  let _,_ = mk_unnumbered_syml ~depth fn in
  let r = [fn] in
  (* write as.json *)
  let act_create_note init item =
    let o =  item
             |> As2.Note.of_rfc4287
             |> As2.Note.mk_create in
    o :: init in
  let p : 'a As2_vocab.Types.collection_page = {
    id         = self;
    current    = None;
    first      = None;
    is_ordered = true;
    items      = es |> List.fold_left act_create_note [];
    last       = None;
    next;
    part_of    = Some (Uri.make ~path:unn ());
    prev;
    total_items= None;
  } in
  let j = As2_vocab.Encode.collection_page ~base
      (As2_vocab.Encode.create ~base
         (As2_vocab.Encode.note ~base))
      p in
  let dn = self |> Uri.path in
  let fn = dn ^ "as.json" in
  let _ = File.mkdir_p File.pDir dn in
  File.out_channel true fn (Outbox.to_chan j);
  (* write unnumbered outbox json *)
  (* TODO *)
  let j : 'a As2_vocab.Types.collection = {
    id         = self;
    current    = None;
    first      = None;
    is_ordered = true;
    items      = es |> List.fold_left act_create_note [];
    last       = None;
    total_items= None;
  } in
  let j = j
          |> As2_vocab.Encode.collection ~base
            (As2_vocab.Encode.create ~base
               (As2_vocab.Encode.note ~base)) in
  let fn = unn ^ "as.json" in
  Logr.debug (fun m -> m "%s.%s %s" "Storage" "materialise" fn);
  let _ = File.mkdir_p File.pDir dn in
  File.out_channel true fn (Outbox.to_chan j);
  let r = fn :: r in
  Ok r

(* return a list of ix the entry is part of *)
let save
    ?(items_per_page = 50)
    ?(fn = target)
    ?(fn_id_cdb = fn_id_cdb)
    ?(_fn_url_cdb = fn_url_cdb)
    ?(_fn_t_cdb = fn_t_cdb)
    (e : Rfc4287.Entry.t) =
  Logr.debug (fun m -> m "Storage.save");
  let id,ix = Atom.next_id ~items_per_page (pre ^ "o/p/") in
  let e = {e with id} in
  Logr.debug (fun m -> m "  id: %a  fn_x: %s" Uri.pp id ix);
  (* append entry to global storage .s and record store position *)
  let p0 = try (Unix.stat fn).st_size with _ -> 0 in
  let mode = [ Open_append; Open_binary; Open_creat; Open_wronly ] in
  File.out_channel false ~mode fn (fun oc ->
      e
      |> Rfc4287.Entry.encode
      |> Csexp.to_channel oc
    );
  let p1 = (Unix.stat fn).st_size in
  let pos = (p0,p1)
            |> TwoPad10.encode
            |> Bytes.of_string in
  let ix = ix |> Atom.append_to_ix pos in
  (* append to the global id->pos index *)
  let _ = Mapcdb.add (id_to_b e.id) pos fn_id_cdb in
  Logr.warn (fun m -> m "@TODO append url->id to urls.cdb");
  e,ix,pos

let purge ?(_fn = target) _id =
  (* find start and stop position *)
  (* create a filler buffer of required size *)
  (* overwrite the data *)
  (* find the feeds containing the id and mark them for refresh *)
  (* purge id from indexes? *)
  Error "not implemented yet"

let select ?(_fn = target) _id =
  (* find start and stop positions *)
  Error "not implemented yet"
