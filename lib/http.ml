(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Web.
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

let camel = "🐫"

let ( let* ) = Result.bind

let reso ~base url =
  Uri.resolve "https" base url

module Request = struct
  (** https://tools.ietf.org/html/rfc3875#section-4.1.13  *)
  type t = {
    content_type   : string;
    content_length : int option;
    host           : string;
    http_cookie    : string;
    path_info      : string;
    query_string   : string;
    remote_addr    : string;
    request_method : string;
    scheme         : string;
    script_name    : string;
    server_port    : string;
  }

  (** set script and path for a query_string. *)
  let request_uri req =
    req.script_name ^ req.path_info
    ^ match req.query_string with
    | "" -> ""
    | qs -> "?" ^ qs

  let srvr r : string =
    let prt = match r.scheme, r.server_port with
      | "http",   "80" -> ""
      | "https", "443" -> ""
      | _              -> ":" ^ r.server_port in
    r.scheme ^ "://" ^ r.host ^ prt

  let base r : string =
    (srvr r) ^ (Filename.dirname r.script_name) ^ "/"

  let abs r : string =
    (srvr r) ^ r.script_name ^ r.path_info ^ "?" ^ r.query_string
end

module Cgi = struct
  module Os = struct
    let getenv = Sys.getenv

    (* https://github.com/rixed/ocaml-cgi/blob/master/cgi.ml#L169 *)
    let getenv_safe ?default s =
      try getenv s
      with Not_found ->
      match default with
      | Some d -> d
      | None -> failwith ("Cgi: the environment variable " ^ s ^ " is not set")
  end

  (** despite https://tools.ietf.org/html/rfc3875#section-4.1.13 1und1.de
   * webhosting returns the script_name instead an empty or nonex path_info in
   * case *)
  let consolidate req' =
    Result.bind req' (fun (req : Request.t) ->
        if String.equal req.path_info req.script_name
        then Ok {req with path_info = ""}
        else req')

  (** Almost trivial. https://tools.ietf.org/html/rfc3875 *)
  let request_from_env () =
    try
      let r : Request.t = {
        content_type   = "CONTENT_TYPE" |> Os.getenv_safe ~default:"";
        content_length = Option.bind
            ("CONTENT_LENGTH" |> Sys.getenv_opt)
            (fun s -> Option.bind
                (s |> int_of_string_opt)
                Option.some);
        host           = "HTTP_HOST"    |> Os.getenv_safe ~default:("SERVER_NAME" |> Os.getenv);
        http_cookie    = "HTTP_COOKIE"  |> Os.getenv_safe ~default:"";
        path_info      = "PATH_INFO"    |> Os.getenv_safe ~default:"";
        query_string   = "QUERY_STRING" |> Os.getenv_safe ~default:"";
        request_method = "REQUEST_METHOD" |> Os.getenv;
        remote_addr    = "REMOTE_ADDR"  |> Os.getenv;
        (* request_uri = "REQUEST_URI"  |> Os.getenv ; *)
        scheme =
          (match "HTTPS" |> Os.getenv_safe ~default:"" with
           | "on" -> "https"
           | _    -> "http");
        script_name    = "SCRIPT_NAME"  |> Os.getenv;
        server_port    = "SERVER_PORT"  |> Os.getenv;
      }
      in Ok r
    with Not_found -> Error "Not Found."
end

let plain2html s =
  s
  |> Lexing.from_string
  |> Plain2html.url (Buffer.create 100)
  |> Buffer.contents

module Form = struct
  type t = (string * string list) list

  (* https://discuss.ocaml.org/t/decoding-x-www-form-urlencoded/4505/3?u=mro *)
  (* application/x-www-form-urlencoded *)
  let of_string s : t = Uri.query_of_encoded s
  let of_channel ic = ic |> input_line |> of_string
(*
  let sort (l : t) : t =
    l |> List.sort (fun (a, _) (b, _) -> String.compare a b)

  let filter_sort f l = l |> List.filter f |> sort

  let filter_sort_keys (ks : string list) l =
    l |> filter_sort (fun (k, _) -> List.exists (String.equal k) ks)
*)
  let values ic r = Ok (of_channel ic, r)

  type input = string * (string * (string * string) list)

  let validate name ty v attr =
    let vali ty (an,av) v =
      match ty,an with
      | _,"maxlength" ->
        Logr.debug (fun m -> m "    validate %s='%s'" an av);
        (* http://www.w3.org/TR/html5/forms.html#the-maxlength-and-minlength-attributes
           https://wiki.selfhtml.org/wiki/HTML/Elemente/input *)
        (match av |> int_of_string_opt with
         | None -> Error (name,"invalid maxlength")
         | Some max -> if String.length v <= max
           then Ok v
           else Error (name,"longer than maxlength"))
      | _,"minlength" ->
        Logr.debug (fun m -> m "    validate %s='%s'" an av);
        (* http://www.w3.org/TR/html5/forms.html#the-maxlength-and-minlength-attributes
           https://wiki.selfhtml.org/wiki/HTML/Elemente/input *)
        (match av |> int_of_string_opt with
         | None -> Error (name,"invalid minlength")
         | Some min -> if String.length v >= min
           then Ok v
           else Error (name,"shorter than minlength"))
      | _,"pattern" ->
        Logr.debug (fun m -> m "    '%s' ~ /%s/" v av);
        (try
           let rx = Re.Pcre.regexp av in
           if Re.execp rx v
           then Ok v
           else Error (name,"pattern mismatch")
         with | _ -> Error (name,"invalid pattern"))
      | _ -> Ok v
    in
    Result.bind v (vali ty attr)

  let string_opt ((name,(ty,constraints)) : input) (vals : t) : (string option, string * string) result =
    Logr.debug (fun m -> m "  <input name='%s' ..." name);
    match List.assoc_opt name vals with
    | None   ->
      (match List.assoc_opt "required" constraints with
       | None   -> Ok None
       | Some _ -> Error (name, "required but missing"))
    | Some v ->
      let* s = List.fold_left
          (validate name ty)
          (v |> String.concat "" |> Result.ok)
          constraints in
      Ok (Some s)

  let string (name,co) va : (string, string * string) result =
    match string_opt (name,co) va with
    | Error _ as e -> e
    | Ok None      -> Logr.err (fun m -> m "Field '%s' must be 'required' to use 'string'" name);
      Error (name, "implicitly required but missing")
    | Ok (Some v)  -> Ok v
end

(* https://tools.ietf.org/html/rfc2616/#section-3.3.1
   https://tools.ietf.org/html/rfc1123#page-55
   https://tools.ietf.org/html/rfc822#section-5.1
*)
let to_rfc1123 (time : Ptime.t) =
  (* MIT License, Copyright 2021 Anton Bachin
     https://github.com/aantron/dream/blob/master/src/pure/formats.ml#L51 *)
  let weekday =
    match Ptime.weekday time with
    | `Sun -> "Sun"
    | `Mon -> "Mon"
    | `Tue -> "Tue"
    | `Wed -> "Wed"
    | `Thu -> "Thu"
    | `Fri -> "Fri"
    | `Sat -> "Sat"
  in
  let (y, m, d), ((hh, mm, ss), _tz_offset_s) = Ptime.to_date_time time in
  let month =
    match m with
    | 1 -> "Jan"
    | 2 -> "Feb"
    | 3 -> "Mar"
    | 4 -> "Apr"
    | 5 -> "May"
    | 6 -> "Jun"
    | 7 -> "Jul"
    | 8 -> "Aug"
    | 9 -> "Sep"
    | 10 -> "Oct"
    | 11 -> "Nov"
    | 12 -> "Dec"
    | _ -> assert false
  in
  (* [Ptime.to_date_time] docs give range 0..60 for [ss], accounting for
     leap seconds. However, RFC 6265 §5.1.1 states:
     5.  Abort these steps and fail to parse the cookie-date if:
       *  the second-value is greater than 59.
       (Note that leap seconds cannot be represented in this syntax.)
     See https://tools.ietf.org/html/rfc6265#section-5.1.1.
     Even though [Ptime.to_date_time] time does not return leap seconds, in
     case I misunderstood the gmtime API, of system differences, or future
     refactoring, make sure no leap seconds creep into the output. *)
  Printf.sprintf "%s, %02i %s %04i %02i:%02i:%02i GMT" weekday d month y hh mm
    (min 59 ss)

module Mime = struct
  module C = As2_vocab.Constants.ContentType
  let app_act_json = C.app_act_json
  let app_atom_xml = C.app_atom_xml
  let app_form_url = "application/x-www-form-urlencoded"
  let app_json     = C.app_json
  let img_jpeg     = "image/jpeg"
  let text_html    = "text/html; charset=utf8"
  let text_plain   = "text/plain; charset=utf8"
  let text_xml     = "text/xml; charset=utf8"

  let is_app_json m =
    app_act_json |> String.equal m
    || app_json |> String.equal m
end
module H = struct
  let add' h (n, v) = Cohttp.Header.add h n v

  let acc_act_json  = ("Accept",       Mime.app_act_json)
  let acc_app_json  = ("Accept",       Mime.app_json)
  let agent         = ("User-Agent",   "Seppo.Social")

  let ct_act_json   = ("Content-Type", Mime.app_act_json)
  let ct_html       = ("Content-Type", Mime.text_html)
  let ct_json       = ("Content-Type", Mime.app_json)
  let ct_plain      = ("Content-Type", Mime.text_plain)
  let ct_xml        = ("Content-Type", Mime.text_xml)

  let set_cookie v  = ("Set-Cookie",   v)
end

(* See also https://github.com/aantron/dream/blob/master/src/pure/status.ml *)
(* RFC1945 demands absolute uris https://www.rfc-editor.org/rfc/rfc1945#section-10.11 *)
let s200m mime = Error (200, "Ok", [ mime ])
let s200x = s200m H.ct_xml
let s302 ?(header = []) (url : string) = Error (302, "Found", [ H.ct_plain; ("Location", url) ] @ header )
let s400 = Error (400, "Bad Request",  [ H.ct_plain ])
let s400x= Error (400, "Bad Request",  [ H.ct_xml ])
let s401 = Error (401, "Unauthorized", [ H.ct_plain ])
let s403' = (403, "Forbidden",    [ H.ct_plain ])
let s403 = Error s403'
let s404 = Error (404, "Not Found",    [ H.ct_plain ])
let s405 = Error (405, "Method Not Allowed", [ H.ct_plain ])
(* https://stackoverflow.com/a/42171674/349514 *)
let s422x= Error (422, "Unprocessable Entity", [ H.ct_xml ])
(* https://tools.ietf.org/html/rfc6585#section-4
   Retry-After https://tools.ietf.org/html/rfc2616#section-14.37
   HTTP-date https://tools.ietf.org/html/rfc1123
   https://github.com/inhabitedtype/ocaml-webmachine/blob/master/lib/rfc1123.ml
*)
let s429_t (t : Ptime.t) = Error ( 429, "Too Many Requests", [ H.ct_plain; ("Retry-After", t |> to_rfc1123) ] )
let s500' = (500, "Internal Server Error", [ H.ct_plain ])
let s500 = Error s500'
let s501 = Error (501, "Not Implemented", [ H.ct_plain ])

let err500 ?(error = s500') ?(level = Logs.Error) msg e =
  Logr.msg level (fun m -> m "%s: %s" msg e);
  error

(* make buf_send a noop *)
let s_ok = Ok (-1, "", Buffer.create 0)

let head oc v =
  let status, reason, hdrs = match v with
    | Ok v -> v
    | Error v -> v in
  Printf.fprintf oc "%s: %d %s\r\n" "Status" status reason;
  let single (k, v) = Printf.fprintf oc "%s: %s\r\n" k v in
  hdrs |> List.iter single;
  Printf.fprintf oc "\r\n"

let redirect' req oc url =
  s302 url |> head oc;
  Printf.fprintf oc "%s %s.\r\n" camel "Found";
  Ok req

let redirect oc url =
  s302 url |> head oc;
  Printf.fprintf oc "%s %s.\r\n" camel "Found";
  s_ok

let clob_send oc mime clob =
  s200m ("Content-Type", mime) |> head oc;
  (match clob with
   | Some v -> Printf.fprintf oc "%s" v
   | None -> ());
  0

let clob_send' oc mime clob =
  let _ = clob_send oc mime (Some clob) in
  Ok ()

let buf_send oc res =
  match res with
  (* sentinel *)
  | Ok (-1, _, _) -> 0
  | Ok (code, reason, (buf : Buffer.t)) ->
    Ok (code, reason, []) |> head oc;
    Buffer.output_buffer oc buf;
    if code < 500 then 0 else 1
  | Error (code, reason, (_ : (string * string) list)) as x ->
    x |> head oc;
    Printf.fprintf oc "%s %s\r\n" camel reason;
    if code < 500 then 0 else 1


(** Create headers including a signature for a POST request.
 *
 * https://blog.joinmastodon.org/2018/06/how-to-implement-a-basic-activitypub-server/#http-signatures
 * https://socialhub.activitypub.rocks/t/help-needed-http-signatures/2458
 * https://tools.ietf.org/id/draft-cavage-http-signatures-12.html
 *
 * HTTP signature according https://tools.ietf.org/id/draft-cavage-http-signatures-12.html#rfc.appendix.C
 * https://www.ietf.org/archive/id/draft-ietf-httpbis-message-signatures-10.html#name-creating-a-signature
 * Digest http://tools.ietf.org/html/rfc3230#section-4.3.2
 *
 * https://docs.joinmastodon.org/spec/security/#http
 * https://w3id.org/security#publicKey
 * https://w3id.org/security/v1
 *
 * NOT: https://datatracker.ietf.org/doc/draft-ietf-httpbis-message-signatures/
*)
let signed_headers (fkt_sign : string -> string) sndr date dige uri =
  Logr.debug (fun m -> m "Http.signed_headers");
  let hdr = [
    ("(request-target)", "post " ^ Uri.path_and_query uri);
    ("host", uri |> Uri.host |> Option.value ~default:"-");
    ("date", date |> to_rfc1123);
    ("digest", dige);
  ] |> Cohttp.Header.of_list in
  let open Cohttp in
  let txt = Printf.sprintf
      "keyId=\"%s#main-key\",\
       algorithm=\"rsa-sha256\",\
       headers=\"(request-target) host date digest\",\
       signature=\"%s\""
      (sndr |> Uri.to_string)
      (hdr |> Header.to_frames |> String.concat "\n" |> fkt_sign)
  and hdr = Header.remove hdr "(request-target)" in
  Header.add hdr "signature" txt

(* https://github.com/mirage/ocaml-cohttp#dealing-with-timeouts *)
let timeout ~seconds ~f =
  try%lwt
    Lwt.pick
      [
        Lwt.map Result.ok (f ()) ;
        Lwt.map (fun () -> Error "Timeout") (Lwt_unix.sleep seconds);
      ]
  with
  | Failure s -> Lwt.return (Error s)

(* don't care about maximum redirects but rather enfore a timeout *)
let get ?(seconds = 5.0) ?(headers = [] |> Cohttp.Header.of_list) uri =
  Logr.debug (fun m -> m "-> GET %a" Uri.pp uri);
  let headers = H.add' headers H.agent in
  (* based on https://github.com/mirage/ocaml-cohttp#dealing-with-redirects *)
  let rec get_follow uri =
    let%lwt r = Cohttp_lwt_unix.Client.get ~headers uri in
    follow_redirect r
  and follow_redirect (response, body) =
    match Cohttp.Response.status response with
    | `Found
    | `Moved_permanently
    | `Permanent_redirect
    | `See_other
    | `Temporary_redirect ->
      (
        match "location" |> Cohttp.Header.get (Cohttp.Response.headers response) with
        | Some loc ->
          Logr.debug (fun m -> m "-> location %s" loc);
          let loc = loc |> Uri.of_string |> reso ~base:uri in
          let fol () = get_follow loc in
          (* The unconsumed body would leak memory *)
          let%lwt p = Cohttp_lwt.Body.drain_body body in
          fol p
        | None -> Lwt.return (response, body)
      )
    | _ -> Lwt.return (response, body)
  and f () = get_follow uri in
  timeout ~seconds ~f

let post ?(seconds = 5.0) ~headers body uri =
  Logr.debug (fun m -> m "-> POST %a" Uri.pp uri);
  let headers = H.add' headers H.agent in
  let f () = Cohttp_lwt_unix.Client.post ~body ~headers uri in
  timeout ~seconds ~f

let get_jsonv
    fkt
    ?(seconds = 5.0)
    ?(headers = [ H.acc_act_json ] |> Cohttp.Header.of_list)
    uri =
  let%lwt p = get ~seconds ~headers uri in
  match p with
  | Error _ as e -> Lwt.return e
  | Ok (_resp, body) ->
    let%lwt body = body |> Cohttp_lwt.Body.to_string in
    body
    |> Ezjsonm.value_from_string
    |> fkt
    |> Lwt.return

let get_json_2 fkt ?(seconds = 5.0) ~headers uri =
  let%lwt p = get ~seconds ~headers uri in
  match p with
  | Error _ as e -> Lwt.return e
  | Ok (resp, body) ->
    let%lwt body = body |> Cohttp_lwt.Body.to_string in
    body
    |> Ezjsonm.value_from_string
    |> fkt resp
    |> Lwt.return
