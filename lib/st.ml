
(* has an alloc, but part of ocaml >= 4.13 anyway *)
let starts_with ~prefix s =
  let lp = prefix |> String.length in
  lp <= (s |> String.length) 
  && (prefix |> String.equal (String.sub s 0 lp))

(* has an alloc, but part of ocaml >= 4.13 anyway *)
let ends_with ~suffix s =
  let ls = suffix |> String.length
  and l = s |> String.length in
  ls <= l 
  && (suffix |> String.equal (String.sub s (l - ls) ls))
