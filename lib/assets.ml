(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Web.
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

let ( let* ) = Result.bind
let ( >>= ) = Result.bind

(** Assets packed into the binary and to be unpacked and used as they are *)
module Const = struct
  (** Unpack the web application assets from the executable. *)

  let delete_unpack_marker = "delete-me-to-unpack-missing"

  let mutabl =
    [
      "app/var/lib/me-avatar.jpg";
      "app/var/lib/me-banner.jpg";
      Cfg.Urlcleaner.fn;
    ]

  let all =
    mutabl @ [
      ".htaccess";
      ".well-known/nodeinfo/.htaccess"; (* dot file missing *)
      ".well-known/nodeinfo/README.txt"; (* dot file missing *)
      ".well-known/nodeinfo/index.json"; (* dot dir missing *)
      ".well-known/webfinger/README.txt"; (* dot dir missing *)
      "README.txt";
      "activitypub/followers/index.json";
      "activitypub/following/index.json";
      "activitypub/index.html";
      "activitypub/liked/index.json";
      "activitypub/outbox/index.json";
      "activitypub/README.txt";
      "app/.htaccess";
      "app/README.txt";
      "app/etc/ban.cfg";
      "app/i-must-be-403.svg";
      "app/var/README.txt";
      Tag.cdb |> (fun (Mapcdb.Cdb v) -> v);
      "app/var/lib/followers.cdb";
      "app/var/lib/o/d/README.txt";
      "app/var/lib/o/m/README.txt";
      "app/var/lib/o/p/0.ix";
      "app/var/lib/o/t/README.txt";
      Storage.fn_id_cdb  |> (fun (Mapcdb.Cdb v) -> v);
      Storage.fn_url_cdb |> (fun (Mapcdb.Cdb v) -> v);
      Ban.fn;
      "app/var/log/seppo.log";
      "app/var/log/seppo.log.0";
      "app/var/run/README.txt";
      "app/var/spool/job/tmp/README.txt";
      "app/var/spool/job/new/README.txt";
      "app/var/spool/job/cur/README.txt";
      "app/var/spool/job/wait/README.txt";
      "app/var/spool/job/run/README.txt";
      "app/var/spool/job/err/README.txt";
      "contrib/README.txt";
      "contrib/etc/lighttpd/conf-available/50-seppo.conf";
      "contrib/harden.sh";
      delete_unpack_marker;
      "favicon.ico";
      "index.html";
      "nodeinfo/2.1.json";
      "o/d/README.txt";
      "o/p/index.xml";
      "o/t/README.txt";
      "robots.txt";
      "seppo.pub.pem";
      "themes/current/404.html";
      "themes/current/awesomplete.css";
      "themes/current/awesomplete.js";
      "themes/current/changepasswordform.xslt";
      "themes/current/configform.xslt";
      "themes/current/do=post.xslt";
      "themes/current/do=tools.xslt";
      "themes/current/feed-icon.svg";
      "themes/current/linkform.xslt";
      "themes/current/loginform.xslt";
      "themes/current/posts.css";
      "themes/current/posts.js";
      "themes/current/posts.xslt";
      "themes/current/README.txt";
      "themes/current/style.css";
      "themes/current/valid-atom.svg";
    ]

  (* how to deal with IO erros? Let them fly! *)
  let restore_if_nonex perm candidates =
    let restore_file fn =
      let _ = fn |> Filename.dirname |> File.mkdir_p File.pDir in
      if not (File.exists fn) then
        match File.out_channel false ~perm fn (fun oc ->
            match Res.read ("static/" ^ fn) with
            | None ->
              Logr.err (fun m -> m "missing %s" fn);
              None
            | Some str ->
              str |> output_string oc;
              Logr.info (fun m -> m "unpacked %s" fn);
              Some fn
          ) with
        | None -> Unix.unlink fn
        | Some _ -> ()
    in
    if not (File.exists delete_unpack_marker)
    then (
      Logr.debug (fun m -> m "%s.%s" "Assets" "restore_if_nonex");
      candidates |> List.iter restore_file;
    )
end

(** Generated assets with local dependencies *)
module Gen = struct
  let make x =
    Result.bind 
      As2.Webfinger.(Make.make "" rulez target)
      (fun _ -> Ok x)

  (* ready to be used in the handler. *)
  let make_cgi x =
    match make x with
    | Error msg ->
      Logr.err (fun m -> m "%s" msg);
      Http.s500
    | Ok _ as o -> o
end

