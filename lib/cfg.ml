(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Web.
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

let random_pwd () =
  (* 12*8 bits of entropy packed into 16 legible characters *)
  Random.random_buffer 12
  |> Cstruct.to_string
  |> Base64.encode_string ~alphabet:Base64.uri_safe_alphabet

module Base = struct
  let fn = "app/etc/base.url"

  let make pre = Make.make pre [] fn

  let to_file fn u =
    File.out_channel true fn (fun oc ->
        output_string oc u;
        close_out oc;
        Ok fn)

  let from_file fn =
    File.in_channel fn (fun ic ->
        ic
        |> input_line
        |> Uri.of_string
        |> Result.ok
      )
end

module CookieSecret = struct
  let fn = "app/var/run/cookie.sec"

  let from_file fn =
    File.in_channel fn (fun ic ->
        Logr.debug (fun m -> m "CookieSecret.from_file: ...");
        ic
        |> input_line
        |> Cstruct.of_string
        |> Result.ok)

  let rule : Make.t = {
    target = fn;
    srcs =  [];
    fresh = Make.Missing;
    build = fun _pre _ oc _ ->
      Logr.debug (fun m -> m "CookieSecret.rule: 32 bytes of entropy, see Mirage_crypto.Chacha20");
      Random.random_buffer 32
      |> Cstruct.to_string
      |> output_string oc;
      Ok ""
  }

  let make pre = Make.make pre [rule] fn
end

module Profile = struct
  type t = {
    title    : string; (* similar atom:subtitle *)
    bio      : string; (* similar atom:description *)
    language : Rfc4287.rfc4646;
    timezone : Timedesc.Time_zone.t;
    posts_per_page : int;
  }

  let validate p : (t, 'a) result =
    Ok p

  let from_file fn =
    File.in_channel fn (fun ic ->
        let open Csexp in
        match input ic with
        | Ok List [
            List [ Atom "title";     Atom title ] ;
            List [ Atom "bio";       Atom bio ] ;
            List [ Atom "language";  Atom language ] ;
            List [ Atom "timezone";  Atom timezone ] ;
            List [ Atom "posts-per-page"; Atom posts_per_page ] ;
          ] ->
          {
            title;
            bio;
            language = Rfc4287.Rfc4646 language;
            timezone = Timedesc.Time_zone.(timezone |> make |> Option.value ~default:utc);
            posts_per_page = posts_per_page |> int_of_string;
          } |> validate
        | Error _ as e -> e
        | _ -> Error "profile field expectation failure"
      )

  let to_file fn (p : t) =
    Logr.debug (fun m -> m "to_file '%s' ('%s')" fn p.title);
    File.out_channel true fn (fun oc ->
        let Rfc4287.Rfc4646 language = p.language in
        let tz  : string = p.timezone |> Timedesc.Time_zone.name in
        let ppp : string = p.posts_per_page |> string_of_int in
        Csexp.(List [
            List [ Atom "title";     Atom p.title ] ;
            List [ Atom "bio";       Atom p.bio ] ;
            List [ Atom "language";  Atom language ] ;
            List [ Atom "timezone";  Atom tz ] ;
            List [ Atom "posts-per-page"; Atom ppp ] ;
          ] |> to_channel oc);
        Ok fn
      )

  let target = "app/etc/profile.cfg"

  let load () : t =
    match (try from_file target with | _ -> Error "aua") with
    | Ok p -> p
    | Error _ ->
      let title = "Yet Another #Seppo! 🌻" 
      and bio = "My introduction that has some text\n\
                 and may span mul-\n\
                 tip-\n\
                 le lines. And don't forget to put in some 🐫 🦥 🌻\n\
                 \n\
                 Also mentioning an url is an idea: https://Seppo.Social"
      and language = Rfc4287.Rfc4646 "en"
      and timezone = Timedesc.Time_zone.(local ()
                                         |> Option.value ~default:utc)
      and posts_per_page = 50 in
      {title;bio;language;timezone;posts_per_page}

  let fresh = Make.Outdated

  let ban : Make.t =
    { target = "me-banner.jpg";
      srcs = [ "app/var/lib/me-banner.jpg" ];
      fresh;
      build = fun _pre _ oc _newer ->
        File.cp' "app/var/lib/me-banner.jpg" oc;
        Ok ""
    }
  let thu : Make.t =
    { target = "me-avatar.jpg";
      srcs = [ "app/var/lib/me-avatar.jpg" ];
      fresh;
      build = fun _pre _ oc _newer ->
        File.cp' "app/var/lib/me-avatar.jpg" oc;
        Ok ""
    }

  let make pre =
    let _ = Make.make pre [ ban ] ban.target in
    let _ = Make.make pre [ thu ] thu.target in
    Make.make pre [ ] target
end

module Urlcleaner = struct

  let fn = "app/etc/url-cleaner.cfg"

  type t = {
    rex : string;
    rep : string;
  }

  let is_valid v : (t, 'a) result = Ok v

  let of_file _fn =
    Error "not implemented yet"

  let apply' _c _s =
    Error "not implemented yet"

  let apply _l _s =
    Error "not implemented yet"
end

