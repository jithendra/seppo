#!/bin/sh
# https://discuss.ocaml.org/t/dune-how-to-link-statically-on-linux-not-on-others/8537/4?u=mro
# https://discuss.ocaml.org/t/ld-error-missing-note-gnu-stack-section/12478/3

case "$(uname -s)" in
  Darwin)
    # do not link statically on macos.
    echo '(:standard -cclib )'
    ;;
  FreeBSD)
    echo '(:standard -cclib "-z noexecstack" -ccopt -static)'
    ;;
  *)
    echo '(:standard -cclib -ccopt -static)'
    ;;
esac

