(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Web.
 *
 * cgi.ml
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

open Seppo_lib

let ( let* ) = Result.bind

let index oc =
  Ok (200, "Ok", [Http.H.ct_html]) |> Http.head oc;
  {|<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta name="generator" content="Seppo.Social" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Check WebFinger RFC7033 etc.</title>
</head>
<body>
  <p>This checker is a single-file <a href="https://www.ietf.org/rfc/rfc3875.html">cgi</a>. Host
  it yourself and have all logs are on your property. <a href="https://seppo.social/downloads/">Download apchk.cgi</a>, drop on a
  webserver and off you go without any further ado. The <a href=
  "https://seppo.social/sourcecode/">sourcecode</a> is free and open.</p>
  <p>It still <a href="https://codeberg.org/seppo/seppo/issues?labels=109423">lacks a lot</a>,
  especially error messages for failure cases.</p>
  <h2 id="rfc7033">WebFinger RFC7033</h2>
  <p>Check <a href="https://www.rfc-editor.org/rfc/rfc7033">WebFinger</a> compliance as assumed by
  <a href="https://seppo.social/">seppo.social</a>.</p>
  <p>Fetch the document, parse, re-encode and return as json. Usually a subset of the input.</p>
  <form name="webfinger" action="apchk.cgi/webfinger" method="get" enctype=
  "application/x-www-form-urlencoded" id="webfinger">
    <input placeholder="@myname@example.com" name="handle" type="text" size="45" minlength="5"
    autofocus="autofocus" required="required" /> <input type="submit" />
  </form>
  <h2 id="ap_profile">AP actor profile</h2>
  <p>Check <a href="https://www.w3.org/TR/activitypub/#actor-objects">Activitypub Actor Profile</a>
  compliance as assumed by <a href="https://seppo.social/">seppo.social</a>.</p>
  <p>Fetch the document, parse, re-encode and return as json. Usually a subset of the input.</p>
  <form name="actor" action="apchk.cgi/actor" method="get" enctype=
  "application/x-www-form-urlencoded" id="actor">
    <input placeholder="https://example.com/actors/myname" name="url" type="text" size="45"
    minlength="5" required="required" /> <input type="submit" />
  </form>
</body>
</html>|} |> Printf.fprintf oc "%s";
  Ok ()

let webfinger oc qs =
  match qs |> List.assoc_opt "handle" with
  | Some [handle] ->
    (match handle
           |> As2.Webfinger.Client.from_string
           |> Shell.webfinger with
    | Error _ -> Http.s400
    | Ok q ->
      Ok (200, "Ok", [Http.H.ct_json]) |> Http.head oc;
      q
      |> As2_vocab.Encode.Webfinger.query_result ~base:Uri.empty
      |> Ezjsonm.value_to_channel oc;
      Ok ())
  | _ -> Http.s400

let actor oc qs =
  match qs |> List.assoc_opt "url" with
  | Some [url] ->
    (match url |> Uri.of_string |> Shell.actor with
     | Error _ -> Http.s400
     | Ok q ->
       Ok (200, "Ok", [Http.H.ct_act_json]) |> Http.head oc;
       q
       |> As2_vocab.Encode.person ~base:Uri.empty
       |> Ezjsonm.value_to_channel oc;
       Ok ())
  | _ -> Http.s400

let handle oc _ic (req : Http.Request.t) =
  let ( >>= ) = Result.bind in
  let module L = Logr in
  let dispatch (r : Http.Request.t) =
    let send_res ct p = match p |> Res.read with
      | None -> Http.s500
      | Some b -> Http.clob_send' oc ct b in
    match r.path_info, r.request_method with
    | ("/doap.rdf" as p,          "GET") -> p |> send_res Http.Mime.text_xml
    | ("/LICENSE" as p,           "GET")
    | ("/version" as p,           "GET") -> p |> send_res Http.Mime.text_plain
    | "",                         "GET"  -> index oc
    | "/",                        "GET"  -> Http.s302 req.script_name
    | "/actor",                   "GET"  -> r.query_string |> Uri.query_of_encoded |> actor oc
    | "/webfinger",               "GET"  -> r.query_string |> Uri.query_of_encoded |> webfinger oc
    | _,                          "GET"  -> Http.s404
    | _                                  -> Http.s405
  and finish = function
    | Ok () -> 0
    | Error (code, reason, (_ : (string * string) list)) as h ->
      h |> Http.head oc;
      Printf.fprintf oc "%s %s.\n" Http.camel reason;
      if code < 500
      then 0
      else 1 in
  L.info (fun m -> m "%s -> %s %s" req.remote_addr req.request_method (req |> Http.Request.request_uri));
  Ok req
  >>= dispatch
  |> finish
