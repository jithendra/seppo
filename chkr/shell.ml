(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Web.
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

(* https://caml.inria.fr/pub/docs/manual-ocaml/libref/Sys.html *)

let ( >>= ) = Result.bind
let ( let* ) = Result.bind

let err i msgs =
  let exe = Filename.basename Sys.executable_name in
  msgs |> List.cons exe |> String.concat ": " |> prerr_endline;
  i

open Seppo_lib

let webfinger handle =
  let module C = As2.Webfinger.Client in
  match handle with
  | Error _ -> Error "input must match @myname@example.com"
  | Ok ((C.Localpart _, C.Host _) as x) ->
    let w = x |> C.well_known_uri in
    let* q =
      try w |> C.http_get_remote_finger'
      with
        Ezjsonm.Parse_error (_, e) ->
        Logr.err (fun m -> m "failed to decipher %a\n\n\
                              Please see https://Seppo.Social/S1001\n\
                              what to do about it.\n" Uri.pp w);
        Error e
    in
    (*
    >>= self_json_uri
    >>= As2.Profile.http_get_remote_actor
    >>= As2.Profile.pubkey_pem
    >>= As2.PubKeyPem.pub_from_pem
    >>= As2.PubKeyPem.check
 *) Ok q

(* TODO add more compliance rules. Check pk. *)
let actor = As2.Profile.http_get_remote_actor'
    (*
    >>= As2.Profile.pubkey_pem
    >>= As2.PubKeyPem.pub_from_pem
    >>= As2.PubKeyPem.check
 *)

let exec (args : string list) =
  let print_version oc =
    let exe = Filename.basename Sys.executable_name in
    Printf.fprintf oc "%s: https://Seppo.Social/v/%s\n" exe Version.dune_project_num;
    0
  and print_help oc =
    let _exe = Filename.basename Sys.executable_name in
    Printf.fprintf oc
      "\n\
       Query AP servers.\n\n\
       If run from commandline:\n\n\
       OPTIONS\n\n\
      \  --help, -h\n\
      \      print this help\n\n\
      \  --version, -V\n\
      \      print version\n\n\
       COMMANDS\n\n\
      \  webfinger @myname@example.com\n\
      \      the webfinger json parsed and re-written.\n\n\
      \  actor https://example.com/actors/myname\n\
      \      the AP profile page parsed and re-written.\n\n\
      \  doap\n\
      \      show 'description of a project'\n\n";
    0
  and oc = stdout in
  let tail s = function
    | Error e ->
      Logr.err (fun m -> m "'%s': %s" s e);
      1
    | Ok _ ->
      Logr.info (fun m -> m "%s." s);
      0
  in
  match args with
  | [ _; "-h" ] | [ _; "--help" ] -> print_help oc
  | [ _; "-V" ] | [ _; "--version" ] -> print_version oc
  | [ _; "doap" ] ->
    (match "doap.rdf" |> Res.read with
     | Some v -> Printf.fprintf oc "%s" v
     | None -> ());
    0
  | [ _; "webfinger"; handle ] ->
    (let* q = handle |> As2.Webfinger.Client.from_string |> webfinger in
     q
     |> As2_vocab.Encode.Webfinger.query_result ~base:Uri.empty
     |> Ezjsonm.value_to_channel stdout;
     Ok q)
    |> tail "webfinger"
  | [ _; "actor"; url] ->
    (let* p = url |> Uri.of_string |> actor in
     p
     |> As2_vocab.Encode.person ~base:Uri.empty
     |> Ezjsonm.value_to_channel stdout;
     Ok p)
    |> tail "actor"
  | _ -> err 2 [ "get help with -h" ]
