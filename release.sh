#!/bin/sh
cd "$(dirname "$0")" || exit 1

readonly ver=0.3-alpha

readonly kf="$(ls /media/*/KNUBBEL/seppo.priv.pem /Volumes/KNUBBEL/seppo.priv.pem 2>/dev/null)"
readonly build="linux-x86_64.dev.seppo.social"
readonly deploy="seppo.social"

say () {
  espeak "$1"
}

# ensure git status is empty
# checkout the hash from commandline

# # https://pagefault.blog/2019/04/22/how-to-sign-and-verify-using-openssl/
# # Generate 4096-bit RSA private key and extract public key
# openssl genrsa -out key.pem 4096
# openssl rsa -in key.pem -pubout > key.pub
#
# openssl dgst -sign key.pem -keyform PEM -sha256 -out data.zip.sign -binary data.zip
#
# openssl dgst -verify key.pub -keyform PEM -sha256 -signature data.zip.sign -binary data.zip

say "let's go."
export LANG=C

readonly git_sha="$(git rev-parse --short HEAD)"
readonly date="$(date)"
readonly iso="$(date +%F)"
readonly dst="$(ssh $build uname -s)-$(ssh $build uname -m)"
readonly d="seppo.social/webroot/en/downloads/seppo-$dst-$ver"
readonly dpl="seppo.social/demo/$iso"
readonly fn="seppo.cgi"
readonly fn1="apchk.cgi"
export date git_sha ver

echo "INFO checking some preliminaries" 1>&2 \
&& ls $kf \
&& ssh $deploy echo "$deploy box reachable" \
&& ssh $build echo "$build box reachable" \
&& ssh $build ssh $deploy echo "$deploy also reachable from $build" \
&& echo "INFO building & running tests locally" 1>&2 \
&& make \
&& make test clean 2>&1 \
&& echo "INFO building on build box" 1>&2 \
&& git gc 2>&1 \
&& rsync -avPz --delete . $build:~/Documents/seppo/ \
&& ssh $build make -C Documents/seppo/ \
&& echo "INFO prepare & sign download" 1>&2 \
&& ssh $deploy mkdir -p "$d/" \
&& ssh $build scp Documents/seppo/_build/seppo-$dst-'*'.cgi $deploy:"$d/$fn" \
&& ssh $build scp Documents/seppo/_build/apchk-$dst-'*'.cgi $deploy:"$d/$fn1" \
&& ssh $build cat Documents/seppo/README.txt.tpl \
| envsubst \
| ssh $deploy tee "$d/README.txt" \
&& ssh $deploy curl -L --output "$d/source.tar.gz" "https://codeberg.org/seppo/seppo/archive/$git_sha.tar.gz" \
&& ssh $deploy openssl dgst -binary -sha256 "$d/$fn" > "/tmp/$fn.sha256" \
&& openssl pkeyutl -sign -in "/tmp/$fn.sha256" -inkey "$kf" -pkeyopt digest:sha256 -keyform PEM \
| ssh $deploy tee "$d/$fn.sign" >/dev/null \
&& ssh $deploy openssl dgst -binary -sha256 "$d/$fn1" > "/tmp/$fn1.sha256" \
&& openssl pkeyutl -sign -in "/tmp/$fn1.sha256" -inkey "$kf" -pkeyopt digest:sha256 -keyform PEM \
| ssh $deploy tee "$d/$fn1.sign" >/dev/null \
&& ssh $deploy chmod a-w "$d"/* \
&& echo "INFO deploying to demo.seppo.social/$iso" 1>&2 \
&& ssh $deploy rm -rf "$dpl/" \
&& ssh $deploy mkdir "$dpl/" \
&& ssh $deploy cp "$d/$fn" "$dpl/" \
&& ssh $deploy "./$dpl/$fn create @$iso@demo.seppo.social" \
&& ssh $deploy cp "$d/$fn1" "$dpl/" \
&& say 'well'

# openssl dgst -sign "$kf" -keyform PEM -sha256 -out "$d/seppo.cgi.sign" -binary "$d/seppo.cgi"
# && ssh $deploy openssl dgst -sign "$kf" -keyform PEM -sha256 -out "$d/seppo.cgi.sign" -binary "$d/seppo.cgi"

say 'done'
