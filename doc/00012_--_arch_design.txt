
Q: how to test webfinger/RFC7033?

A: Take the current username or 'seppo' on the current domain: @seppo@demo.mro.name
and ensure a valid id and inbox lookup. Both must respond proper json.

Caveat: leaks the username. But they are public anyway.


https://github.com/w3c/activitypub/issues/194

$ curl -L --head 'https://demo.mro.name/.well-known/webfinger?resource=acct:seppo@demo.mro.name'
HTTP/2 301
location: /seppo/.well-known/webfinger/?resource=acct:seppo@demo.mro.name
date: Tue, 19 Apr 2022 18:18:10 GMT
server: lighttpd/1.4.59

HTTP/2 200
content-type: application/json
accept-ranges: bytes
etag: "2523288411"
last-modified: Wed, 23 Mar 2022 11:26:02 GMT
content-length: 542
date: Tue, 19 Apr 2022 18:58:07 GMT
server: lighttpd/1.4.59

{
  "subject": "acct:seppo@demo.mro.name",
  "aliases": [
    "https://demo.mro.name/seppo/as/me/"
  ],
  "links": [
    {
      "rel": "http://webfinger.net/rel/profile-page",
      "type": "text/html",
      "href": "https://demo.mro.name/seppo/"
    },
    {
      "rel": "self",
      "type": "application/activity+json",
      "href": "https://demo.mro.name/seppo/as/me/"
    },
    {
      "rel": "http://ostatus.org/schema/1.0/subscribe",
      "template": "https://demo.mro.name/seppo/seppo.cgi/ostatus/authorize?uri={uri}"
    }
  ]
}


$ curl -L 'https://demo.mro.name/seppo/as/me/'
$ curl -L -H 'Accept: application/activity+json' 'https://digitalcourage.social/users/mro'
HTTP/2 200
content-type: application/activity+json; charset=utf-8

{
  "@context": [
    "https://www.w3.org/ns/activitystreams",
    "https://w3id.org/security/v1",
    {
      "manuallyApprovesFollowers": "as:manuallyApprovesFollowers",
      "toot": "http://joinmastodon.org/ns#",
      "featured": {
        "@id": "toot:featured",
        "@type": "@id"
      },
      "featuredTags": {
        "@id": "toot:featuredTags",
        "@type": "@id"
      },
      "alsoKnownAs": {
        "@id": "as:alsoKnownAs",
        "@type": "@id"
      },
      "movedTo": {
        "@id": "as:movedTo",
        "@type": "@id"
      },
      "schema": "http://schema.org#",
      "PropertyValue": "schema:PropertyValue",
      "value": "schema:value",
      "discoverable": "toot:discoverable",
      "Device": "toot:Device",
      "Ed25519Signature": "toot:Ed25519Signature",
      "Ed25519Key": "toot:Ed25519Key",
      "Curve25519Key": "toot:Curve25519Key",
      "EncryptedMessage": "toot:EncryptedMessage",
      "publicKeyBase64": "toot:publicKeyBase64",
      "deviceId": "toot:deviceId",
      "claim": {
        "@type": "@id",
        "@id": "toot:claim"
      },
      "fingerprintKey": {
        "@type": "@id",
        "@id": "toot:fingerprintKey"
      },
      "identityKey": {
        "@type": "@id",
        "@id": "toot:identityKey"
      },
      "devices": {
        "@type": "@id",
        "@id": "toot:devices"
      },
      "messageFranking": "toot:messageFranking",
      "messageType": "toot:messageType",
      "cipherText": "toot:cipherText",
      "suspended": "toot:suspended",
      "focalPoint": {
        "@container": "@list",
        "@id": "toot:focalPoint"
      }
    }
  ],
  "id": "https://digitalcourage.social/users/mro",
  "type": "Person",
  "following": "https://digitalcourage.social/users/mro/following",
  "followers": "https://digitalcourage.social/users/mro/followers",
  "inbox": "https://digitalcourage.social/users/mro/inbox",
  "outbox": "https://digitalcourage.social/users/mro/outbox",
  "featured": "https://digitalcourage.social/users/mro/collections/featured",
  "featuredTags": "https://digitalcourage.social/users/mro/collections/tags",
  "preferredUsername": "mro",
  "name": "Marcus Rohrmoser 🌍",
  "summary": "\\u003cp\\u003eProgrammer 📱 🍏 🐫 λ.\\u003cbr /\\u003eFostering the indieweb.org/POSSE\\u003c/p\\u003e",
  "url": "https://digitalcourage.social/@mro",
  "manuallyApprovesFollowers": false,
  "discoverable": true,
  "published": "2022-03-31T00:00:00Z",
  "devices": "https://digitalcourage.social/users/mro/collections/devices",
  "alsoKnownAs": [
    "https://pleroma.tilde.zone/users/mro"
  ],
  "publicKey": {
    "id": "https://digitalcourage.social/users/mro#main-key",
    "owner": "https://digitalcourage.social/users/mro",
    "publicKeyPem": "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsp6FAA+3suRcIBIoo6O6\nChjPz3eBvKgIEv8HMP0lH7rTJOO6JCMpx8LhFVg3zQ8WeSECJinRe2dFDpdqjved\nrxKIu77p+RLkH5lRJZvXimOJNa+ea2A9YDQn6Km9VQ5EaiDsGCxopvHWFmsBy9da\ng+7ftwl+FwUhyNntwxdNJ3PTtV3qRnynq2UezyD3/jSELZ6uufgXIpt/6qFWM13K\nGfqKe0JgJE5APT5UiufUjtybnhNWqO9kYH5VaA9LaIWreDfk5sU2qa9xvUpcvRl0\nxpDDI3ROlzuApTmqh7ERybAlepNOtxg1oJyDkbxNWTRJWcDFZvScDXV226CxF15S\niQIDAQAB\n-----END PUBLIC KEY-----\n"
  },
  "tag": [],
  "attachment": [
    {
      "type": "PropertyValue",
      "name": "Zettelkasten",
      "value": "\\u003ca href=\"http://mro.name/microblog\" target=\"_blank\" rel=\"nofollow noopener noreferrer me\"\\u003e\\u003cspan class=\"invisible\"\\u003ehttp://\\u003c/span\\u003e\\u003cspan class=\"\"\\u003emro.name/microblog\\u003c/span\\u003e\\u003cspan class=\"invisible\"\\u003e\\u003c/span\\u003e\\u003c/a\\u003e"
    },
    {
      "type": "PropertyValue",
      "name": "📍",
      "value": "\\u003ca href=\"http://mro.name/traunstein\" target=\"_blank\" rel=\"nofollow noopener noreferrer me\"\\u003e\\u003cspan class=\"invisible\"\\u003ehttp://\\u003c/span\\u003e\\u003cspan class=\"\"\\u003emro.name/traunstein\\u003c/span\\u003e\\u003cspan class=\"invisible\"\\u003e\\u003c/span\\u003e\\u003c/a\\u003e"
    }
  ],
  "endpoints": {
    "sharedInbox": "https://digitalcourage.social/inbox"
  },
  "icon": {
    "type": "Image",
    "mediaType": "image/jpeg",
    "url": "https://digitalcourage.social/system/accounts/avatars/108/050/262/287/113/206/original/c152bff1a001819f.jpg"
  }
}
