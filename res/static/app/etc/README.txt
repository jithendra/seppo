     _  _   ____                         _  
   _| || |_/ ___|  ___ _ __  _ __   ___ | | 
  |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
  |_      _|___) |  __/ |_) | |_) | (_) |_| 
    |_||_| |____/ \___| .__/| .__/ \___/(_) 
                      |_|   |_|             
 
Personal Social Web.
 

This directory holds all configuration data only written
by the admin web interface 'seppo.cgi/config/'. 

auth.s          username and password bcrypt hash
ban.s           banned IPs,  timing & retries
base.url        web interface base url
cp_mastodon.s   crossposting to mastodon
cp_pinboard.s   crossposting to pinboard
cp_twitter.s    crossposting to twitter
id_rsa.priv.pem personal private key, KEEP SAFE and backup!
me-banner.avif  wide top image
me-thumb.avif   small icon
payment.s       payment license
profile.s       title, bio, image, languages, etc.
url-cleaner.s   url cleaner

