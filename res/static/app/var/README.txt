    _  _   ____                         _ 
  _| || |_/ ___|  ___ _ __  _ __   ___ | |
 |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| |
 |_      _|___) |  __/ |_) | |_) | (_) |_|
   |_||_| |____/ \___| .__/| .__/ \___/(_)
                     |_|   |_|            

Personal Social Web.

Copyright (C) The #Seppo contributors. All rights reserved.


./lib/           all the actual content – posts, media,
./lock/ban.cdb   currently blocked IP addresses,
./log/seppo.log  error logging (size < 1MB),
./run/cookie.sec random secret to encrypt session cookie.

