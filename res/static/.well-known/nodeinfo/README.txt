    _  _   ____                         _ 
  _| || |_/ ___|  ___ _ __  _ __   ___ | |
 |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| |
 |_      _|___) |  __/ |_) | |_) | (_) |_|
   |_||_| |____/ \___| .__/| .__/ \___/(_)
                     |_|   |_|            

Personal Social Web.

Copyright (C) The #Seppo contributors. All rights reserved.


Here are files to enable http://nodeinfo.diaspora.software/ns/schema/2.1
lookups. Requests to /.well-known/nodeinfo must return a JSON telling about
this installation.

For Apache webservers, #Seppo! generates the /.well-known/nodeinfo/.htaccess
symlink to the same location within the #Seppo! installation. It redirects to
index.json next to it.

The .htaccess symlink is regenerated when necessary, but only if it is really a
symlink. Real files are never deleted nor modified, as /.well-known/ is outside
the #Seppo! installation.

./.htaccess                     linked to by /.well-known/nodeinfo/.htaccess
./index.json
