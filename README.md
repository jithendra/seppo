```
    _  _   ____                         _ 
  _| || |_/ ___|  ___ _ __  _ __   ___ | |
 |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| |
 |_      _|___) |  __/ |_) | |_) | (_) |_|
   |_||_| |____/ \___| .__/| .__/ \___/(_)
                     |_|   |_|            

Personal Social Web.

Copyright (C) The #Seppo contributors. All rights reserved.
```

# \#Seppo!

Your typewriter for the Social Web.

* Demo https://Seppo.Social/demo
* Downloads https://Seppo.Social/downloads
* Support https://Seppo.Social/support
* Community Mailing List https://Seppo.Social/mailinglist
* (tbd: Announcements [@news@Seppo.Social](https://Seppo.Social/news/))
* Blog https://Seppo.Social/blog
* 🆘 [security@Seppo.Social](mailto:security@Seppo.Social)
* Development https://Seppo.Social/sourcecode

## Design Goals

| Quality         | very good | good | normal | irrelevant |
|-----------------|:---------:|:----:|:------:|:----------:|
| Functionality   |           |      |    ×   |            |
| Reliability     |     ×     |      |        |            |
| Usability       |     ×     |      |        |            |
| Efficiency      |           |      |    ×   |            |
| Changeability   |           |  ×   |        |            |
| Portability     |           |  ×   |        |            |

## Mirrors

* canonical <https://Seppo.Social/sourcecode>
* primary <https://codeberg.org/seppo/seppo>
* <https://git.sr.ht/~mro/seppo>
* <https://notabug.org/mro/seppo>
* <https://codeberg.org/mro/seppo>
* <https://code.mro.name/mro/seppo>

---

Generously funded by [NLNet grant 2022-08-141 from the 🇪🇺 NGI0 Entrust
fund](https://nlnet.nl/project/Seppo/)

