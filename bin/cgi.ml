(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Web.
 *
 * cgi.ml
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

open Seppo_lib

let ( let* ) = Result.bind
let ( >>= )  = Result.bind

let handle oc ic (req : Http.Request.t) =
  let tnow = Ptime_clock.now () in
  let module L = Logr in
  (** if unconfigured yet. *)
  let redir_if_new (r : Http.Request.t) =
    let s302 ?(header = []) p = r.script_name ^ p |> Http.s302 ~header in
    if File.exists Auth.fn
    then if File.exists Cfg.Profile.target
      then Ok r
      else if r.path_info = Web.Profile.path
           || r.path_info = Web.Login.path
           || r.path_info = Web.Credentials.path
      then Ok r
      else s302 Web.Profile.path
    else
    if r.path_info = Web.Credentials.path
    then Ok r
    else
      let* sec = Result.map_error
          (Http.err500 "expected cookie secret")
          (Cfg.CookieSecret.(make "" >>= from_file)) in
      let header = [ Web.cookie_new_session ~tnow sec req Auth.dummy ] in
      s302 ~header Web.Credentials.path
  and restore_assets lst r = Assets.Const.restore_if_nonex File.pFile lst;
    Ok r
  and ban_db = Mapcdb.Cdb Ban.fn in
  let dispatch (r : Http.Request.t) =
    Logr.info (fun m -> m "%s.%s path_info '%s'" "Cgi" "dispatch" r.path_info);
    let send_file ct p = p
                         |> File.to_string
                         |> Http.clob_send' oc ct
    and send_res ct p = match p |> Res.read with
      | None -> Http.s500
      | Some b -> Http.clob_send' oc ct b
    and ases = Web.ases tnow
    and auth = Web.uid_redir
    and ban  = Ban.escalate ban_db
    and csrf_mk v =
      L.warn (fun m -> m "%s.%s TODO create and store token" "Cgi" "dispatch");
      Ok ("f19a65cecdfa2971afb827bc9413eb7244e469a8", v)
    and csrf_ck v =
      L.warn (fun m -> m "%s.%s TODO load and purge token" "Cgi" "dispatch");
      Web.check_token "f19a65cecdfa2971afb827bc9413eb7244e469a8" v
    and form = Http.Form.values
    and drain_body ic (r : Http.Request.t) =
      let n = r.content_length |> Option.value ~default:0 in
      let b = n |> really_input_string ic in
      Logr.warn (fun m -> m "dump\n\n%s" b)
    in
    match r.path_info, r.request_method with
    | ("/var/lock/challenge" as p,"GET") -> let f = "app" ^ p in f |> send_file Http.Mime.text_plain
    | ("/doap.rdf" as p,          "GET") -> p |> send_res Http.Mime.text_xml
    | ("/LICENSE" as p,           "GET")
    | ("/version" as p,           "GET") -> p |> send_res Http.Mime.text_plain
    | "/search",                  "GET"  -> Http.s501
    | "/activitypub/inbox",       "POST" -> r |> drain_body ic; Http.s501
    (* | "/activitypub/monitor.txt", "GET"  -> r |> Activitypub.monitor oc *)
    | "/credentials",             "GET"  -> r |> ases >>= auth >>=             csrf_mk >>= Web.Credentials.get oc
    | "/credentials",             "POST" -> r |> ases >>= auth >>= form ic >>= csrf_ck >>= Web.Credentials.post tnow oc
    | "/login",                   "GET"  -> r |>                               csrf_mk >>= Web.Login.get oc
    | "/login",                   "POST" -> r |>                   form ic >>= csrf_ck >>= Web.Login.post tnow ban oc
    | "/logout",                  "GET"  -> r |> ases >>=                                  Web.Logout.get oc
    | "/post",                    "GET"  -> r |> ases >>= auth >>=             csrf_mk >>= Web.Post.get oc
    | "/post",                    "POST" -> r |> ases >>= auth >>= form ic >>= csrf_ck >>= Web.Post.post tnow oc
    | "/profile",                 "GET"  -> r |> ases >>= auth >>=             csrf_mk >>= Web.Profile.get oc
    | "/profile",                 "POST" -> r |> ases >>= auth >>= form ic >>= csrf_ck >>= Web.Profile.post tnow oc
    | "/session",                 "GET"  -> r |> ases >>=                                  Web.Session.get oc
    | "/tools",                   "GET"  -> Http.s501
    | "/tools",                   "POST" -> Http.s501
    | "/",                        "GET"  -> ".." |> Http.s302
    | "",                         "GET"  -> (let q = r.query_string |> Uri.query_of_encoded
                                             and s302 ?(header = []) p = r.script_name ^ p |> Http.s302 ~header in
                                             (* shaarli compatibility *)
                                             (match q |> List.assoc_opt "do" with
                                              | Some ["login"]     -> s302 Web.Login.path
                                              | Some ["logout"]    -> s302 Web.Logout.path
                                              | Some ["configure"] -> s302 Web.Profile.path
                                              | _                  -> (Ban.escalate ban_db tnow r.remote_addr;
                                                                       Http.s404)
                                             ))
    | _                                  -> (Ban.escalate ban_db tnow r.remote_addr;
                                             Http.s404)
  and finish = function
    | Ok () -> 0
    | Error (code, reason, (_ : (string * string) list)) as h ->
      Logr.debug (fun m -> m "%s.%s %d %s" "Cgi" "finish" code reason);
      h |> Http.head oc;
      Printf.fprintf oc "%s %s.\n" Http.camel reason;
      if code < 500
      then 0
      else 1 in
  L.info (fun m -> m "%s.%s %s -> %s %s" "Cgi" "handle" req.remote_addr req.request_method (req |> Http.Request.request_uri));
  Ok req
  >>= restore_assets Assets.Const.all
  >>= Ban.check_req ban_db tnow
  >>= redir_if_new
  >>= dispatch
  |> finish

