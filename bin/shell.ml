(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Web.
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

(* https://caml.inria.fr/pub/docs/manual-ocaml/libref/Sys.html *)

let ( >>= ) = Result.bind
let ( let* ) = Result.bind

let err i msgs =
  let exe = Filename.basename Sys.executable_name in
  msgs |> List.cons exe |> String.concat ": " |> prerr_endline;
  i

open Seppo_lib

let do_create b strict handle' =
  match handle' with
  | Error _ -> Error "input must match @myname@example.com"
  | Ok As2.Webfinger.Client.(Localpart uid, Host host) ->
    let ensure_alone f =
      let pred f' = not (f' = "." || f' = ".." || f' = f) in
      0 = (f |> Filename.dirname |> File.count_dir 0 pred)
    in Ok ()
    >>= (fun _ ->
        b |> Filename.dirname |> Unix.chdir;
        match (not strict) || ensure_alone "seppo.cgi" with
        | false -> Error "I need an empty directory only containing seppo.cgi to begin with."
        | true -> Ok "app/var/log/")
    >>= File.mkdir_p 0o700
    >>= (fun d ->
        Logr.open_out (d ^ "seppo.log");
        Ok ())
    >>= (fun _u ->
        let open Unix in
        (try
           access "seppo.cgi" [ W_OK ];
           Logr.warn (fun m -> m "'%s' is writable. Change that to $ chmod 555 %s" b b)
         with
         | Unix_error(EUNKNOWNERR 26, "access", _) (* FreeBSD 13.2 amd64 *)
         | Unix_error (EACCES, "access", _) -> ());
        try
          access "seppo.cgi" [ X_OK ];
          Ok ()
        with
        | Unix_error(EUNKNOWNERR 26, "access", _) (* FreeBSD 13.2 amd64 *)
        | Unix_error (EACCES, "access", _) ->
          Error (Printf.sprintf "'%s' isn't executable. Change that to $ chmod 555 %s" b b)
      )
    >>= (fun _u ->
        let defer b a = let r = try a()
                          with | x -> raise x in
          b();
          r
        in
        Logr.debug (fun m -> m "find the base url");
        let f = "nonce"
        and nonce = Cfg.random_pwd () in
        defer
          (fun () -> Unix.unlink f)
          (fun () -> File.out_channel false f
              (fun oc -> output_string oc nonce; Ok f)
            >>= (fun _ ->
                try
                  (* find the base uri

                     Assume a cwd /a/b/c
                     so we check
                     1. https://example.com/a/b/c/ ^ f
                     2. https://example.com/b/c/ ^ f
                     3. https://example.com/c/ ^ f
                     4. https://example.com/ ^ f

                  *)
                  let bas = Uri.make ~scheme:"https" ~host ()
                  and headers = Cohttp.Header.init () in
                  Unix.getcwd ()
                  |> String.split_on_char '/'
                  |> List.rev
                  |> List.cons "."
                  |> File.find_path_tail
                    (fun x ->
                       let u' = Uri.with_path bas (x ^ "/" ^ f) in
                       Logr.debug (fun m -> m " testing %a" Uri.pp u');
                       (let%lwt p = Http.get ~headers u' in
                        match p with
                        | Error _ as e -> Lwt.return e
                        | Ok (_, body) ->
                          (let%lwt nonce' = Cohttp_lwt.Body.to_string body in
                           ((not strict) || nonce |> String.equal nonce')
                           |> Result.ok
                           |> Lwt.return)
                       ) |> Lwt_main.run)
                  >>= (fun p ->
                      let u = Uri.with_path bas (p ^ "/") in
                      let u = Uri.with_userinfo u (Some uid) in
                      let u = Uri.canonicalize u in
                      Logr.debug (fun m -> m "found base %a" Uri.pp u);
                      Ok u)
                with
                | Failure e -> (* e.g. using a non-existant host *) Error e
                | Sys_error e -> Error e
                | Unix.Unix_error (e, fkt, par) -> Error ((Unix.error_message e) ^ ": " ^ fkt ^ " _ " ^ par))
          )
      )
    >>= (fun u ->
        Assets.Const.all |> Assets.Const.restore_if_nonex File.pFile;
        Ok u)
    >>= (fun u ->
        Logr.debug (fun m -> m "write %s" Cfg.Base.fn);
        None |> Uri.with_userinfo u |> Uri.to_string
        |> Cfg.Base.to_file Cfg.Base.fn
        >>= (fun _ -> Ok u))
    >>= (fun u ->
        Logr.debug (fun m -> m "write %s" Auth.fn);
        u |> Uri.user |> Option.to_result ~none:"internal error: no uid found."
        >>= (fun uid ->
            let cred = (Auth.Uid uid, Cfg.random_pwd ()) in
            let _ = cred |> Auth.to_file Auth.fn in
            Ok cred)
        >>= (fun (Auth.Uid uid, pwd) ->
            Printf.printf "login name: %s\n" uid;
            Printf.printf "password: %s\n" pwd;
            Ok u))
    >>= (fun u ->
        Logr.debug (fun m -> m "write %s" Cfg.Profile.target);
        let p : Cfg.Profile.t = {
          title    = "Yet Another #Seppo! 🌻";
          bio      = "My introduction that has some text\n\
                      and may span mul-\n\
                      tip-\n\
                      le lines. And don't forget to put in some 🐫 🦥 🌻\n\
                      \n\
                      Also referring to an url may do no harm: https://Seppo.Social";
          language  = Rfc4287.Rfc4646 "en";
          timezone = (* try /etc/timezone first? *) "Europe/Amsterdam" |> Timedesc.Time_zone.make |> Option.value ~default:Timedesc.Time_zone.utc;
          posts_per_page = 50;
        } in
        p |> Cfg.Profile.to_file Cfg.Profile.target
        >>= (fun _ ->
            let _ = Cfg.Profile.make "" in
            Ok u))
    >>= Assets.Gen.make
    >>= As2.Webfinger.Server.make
    >>= (fun u -> (* self-test *)
        let module C = As2.Webfinger.Client in
        u
        |> C.from_uri
        >>= (fun x ->
            let w = x |> C.well_known_uri in
            try w |> C.http_get_remote_finger'
            with
              Ezjsonm.Parse_error (_, e) ->
              Logr.err (fun m -> m "failed to decipher %a\n\n\
                                    Please see https://Seppo.Social/S1001\n\
                                    what to do about it.\n" Uri.pp w);
              Error e)
        >>= C.self_json_uri
        >>= As2.Profile.http_get_remote_actor'
        >>= As2.Profile.pubkey_pem
        >>= As2.PubKeyPem.pub_from_pem
        >>= As2.PubKeyPem.check
        >>= (fun _ ->
            Format.printf "URL: %a\n" Uri.pp (None |> Uri.with_userinfo u);
            Ok u)
      )

let exec (args : string list) =
  let print_version oc =
    let exe = Filename.basename Sys.executable_name in
    Printf.fprintf oc "%s: https://Seppo.Social/v/%s\n" exe Version.dune_project_num;
    0
  and print_help oc =
    let _exe = Filename.basename Sys.executable_name in
    Printf.fprintf oc
      "\n\
       Some basic tasks on Seppo.Social installations.\n\n\
       If run from commandline:\n\n\
       OPTIONS\n\n\
      \  --help, -h\n\
      \      print this help\n\n\
      \  --version, -V\n\
      \      print version\n\n\
       COMMANDS\n\n\
      \  create @myname@example.com\n\
      \      set up a new account and instance\n\
      \  note < msg\n\
      \      post a message\n\n\
      \  dot\n\
      \      print file dependencies\n\n\
      \  doap\n\
      \      show 'description of a project'\n\n";
(*
      "\n\
      \  info\n\
      \      tell more about this instance\n\n\
      \  key-rotate\n\
      \      generate new keys\n\n\
      \  make\n\
      \      'make' file dependencies\n\n\
      \  tag sift\n\
      \      filter stdin to stdout\n\n\
      \  activitypub\n\
      \      make activitypub/index.json\n\
      \  activitypub like | dislike <uri>\n\
      \      like or dislike a post\n\n"; *)
    0
  and oc = stdout in
  let tail s = function
    | Error e ->
      Logr.err (fun m -> m "'%s': %s" s e);
      1
    | Ok _ ->
      Logr.info (fun m -> m "%s." s);
      0
  in
  match args with
  | [ _; "-h" ] | [ _; "--help" ] -> print_help oc
  | [ _; "-V" ] | [ _; "--version" ] -> print_version oc
  | [ _; "doap" ] ->
    (match "doap.rdf" |> Res.read with
     | Some v -> Printf.fprintf oc "%s" v
     | None -> ());
    0
  | [ _b; "note" ] ->
    (let* base = Cfg.Base.(fn |> from_file) in
     let* profile = Cfg.Profile.(target |> from_file) in
     let* (Auth.Uid userinfo),_ = Auth.(fn |> from_file) in
     let author = Uri.make ~userinfo () in
     let lang = profile.language in
     let fofo fkt = Mapcdb.fold_left fkt () (Mapcdb.Cdb "app/var/lib/followers.cdb") in
     let main =
       let%lwt _pub, pk = As2.PubKeyPem.(pk_from_pem key_pems) in
       Lwt.return pk in
     let pk = Lwt_main.run main in
     let* _ =
       stdin
       |> Rfc4287.Entry.from_channel ~author ~lang
       >>= Main.sift_urls
       >>= Main.sift_tags Tag.cdb
       >>= Main.sift_handles
       >>= Main.publish_note ~base ~profile ~author
       >>= Main.notify_followers ~base fofo in
     Main.process_queue ~base ~pk Job.qn
    ) |> tail "note"
  | [ b; "fake"; handle ] ->
    handle
    |> As2.Webfinger.Client.from_string
    |> do_create b false
    |> tail "fake"
  | [ b; "create"; handle ] ->
    handle
    |> As2.Webfinger.Client.from_string
    |> do_create b true
    |> tail "create"
  | [ _; "info" ] ->
    0
(*
      | [ _; "append" ] -> (
      let now = (Ptime_clock.now (), Ptime_clock.current_tz_offset_s ()) in
      match Txt.of_channel now [] stdin with
      | Ok e -> (
          match Sepp0.append e with Ok _ -> 0 | Error s -> err 4 [ s ])
      | Error _ -> err 5 [ "ouch 300" ])
*)
  | [ _; "dot" ] -> (
      match Make.dot oc (As2.Webfinger.rulez) with
      | Error _ -> 1
      | Ok _ -> 0)
  | [ _; "tag"; "sift" ] -> (
      match Tag.sift_channel stdin with
      | Error _ -> 1
      | Ok l ->
        l |> List.iter (fun (Tag.Tag s) -> Printf.printf "%s\n" s);
        0)
  | [ _; "activitypub" ] -> (
      Make.make "" As2.Person.rulez As2.Person.target
      |> tail "activitypub"
    )
  | [ _; "activitypub"; "like" as atyp; uri ]
  | [ _; "activitypub"; "dislike" as atyp; uri ] -> (
      Make.make "" As2.Person.rulez As2.Person.target
      >>= As2.Person.from_file
      >>= As2.Activity.like atyp (uri |> Uri.of_string)
      >>= Lwt_main.run
      |> tail (args |> List.tl |> String.concat " ")
    )
  | _ -> err 2 [ "get help with -h" ]
