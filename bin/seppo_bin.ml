(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Web.
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

open Seppo_lib

let () =
  Mirage_crypto_rng_lwt.initialize (module Mirage_crypto_rng.Fortuna);
  let module Cg = Http.Cgi in
  let module L = Logr in
  (match Cg.request_from_env () |> Cg.consolidate with
   | Ok req ->
     let log_dir = "app/var/log/" in
     let _ = File.mkdir_p 0o770 log_dir in
     L.open_out (log_dir ^ "seppo.log");
     (try
        let r = Cgi.handle stdout stdin req in
        close_in stdin;
        close_out stdout;
        (* now we might run background tasks *)
        (* L.close_out (); *)
        r
      with e ->
        let msg = Printexc.to_string e in
        Logr.err (fun m -> m "FATAL %s" msg);
        Logr.err (fun m -> m "cwd: '%s'" (Unix.getcwd ()));
        1)
   | Error _ -> Sys.argv
                |> Array.to_list
                |> Shell.exec)
  |> exit
